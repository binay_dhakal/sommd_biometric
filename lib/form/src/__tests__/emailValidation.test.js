import validate from '../validations/emailValidations';

test("valid email", () => {
  expect(validate({ email: 'test@gmail.com' })).toEqual({});
});

test("blank email", () => {
  expect(validate({ name: 'test' })).toEqual({ "email": "Email address is required" });
});

test("wrong email", () => {
  expect(validate({ email: 'test' })).toEqual({ "email": "Email address is invalid" });
});
