import { renderHook, act } from '@testing-library/react-hooks';
import useForm from '../useForm';

test('should assign default values', () => {
  const { result } = renderHook(() => useForm({ name: 'test' }));
  expect(result.current.inputs.name).toBe('test');
});

describe('handleInputChange', () => {
  test('it should change values', () => {
    const { result } = renderHook(() => useForm());
    const event = { target: { name: 'name', value: 'test' }, persist: () => { } };
    act(() => {
      result.current.handleInputChange(event)
    })
    expect(result.current.inputs.name).toBe('test');
  });
});

describe('handleManualInputChange', () => {
  test('it should change values', () => {
    const { result } = renderHook(() => useForm());
    act(() => {
      result.current.handleManualInputChange('name', 'random')
    })
    expect(result.current.inputs.name).toBe('random');
  });
});


describe('handleSubmit', () => {
  test('it calls callback', () => {
    const callback = jest.fn().mockReturnValue('default');
    const validate = jest.fn().mockReturnValue({});
    const { result } = renderHook(() => useForm({}, callback, validate ));

    act(() => {
      result.current.handleSubmit();
    });

    expect(callback).toHaveBeenCalled();
  });

  test('it doesnt submit', () => {
    const callback = jest.fn().mockReturnValue('default');
    const validate = jest.fn().mockReturnValue({ test: 'Error'});
    const { result } = renderHook(() => useForm({}, callback, validate ));

    act(() => {
      result.current.handleSubmit();
    });

    expect(callback).not.toHaveBeenCalled();
  });
})
