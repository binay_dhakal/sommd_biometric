import { useState, SyntheticEvent } from 'react';

interface FormValue { [name: string]: string }

const useForm = (initialValues: FormValue, callback: () => void, validate: (arg0: FormValue) => {}) => {
  const [inputs, setInputs] = useState(initialValues);
  const [errors, setErrors] = useState({});

  const handleSubmit = (event: SyntheticEvent<HTMLElement>) => {
    if (event) event.preventDefault();
    const validationErrors = validate(inputs);
    if (Object.keys(validationErrors).length === 0) {
      callback();
    } else {
      setErrors(errors);
    }
  };

  const handleInputChange = (event: SyntheticEvent<EventTarget>) => {
    event.persist();
    setInputs((inputData) => ({
      ...inputData,
      [(event.target as HTMLInputElement).name]: (event.target as HTMLInputElement).value }));
  };

  const handleManualInputChange = (name: string, value: string) => {
    setInputs((inputData) => ({ ...inputData, [name]: value }));
  };

  return {
    handleSubmit,
    handleInputChange,
    inputs,
    errors,
    handleManualInputChange,
  };
};
export default useForm;
