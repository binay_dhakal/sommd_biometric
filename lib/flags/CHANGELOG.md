# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.0.0](https://bitbucket.org/winedevelopmentco/webapp/compare/@lib/flags@0.1.1...@lib/flags@1.0.0) (2021-06-03)

**Note:** Version bump only for package @lib/flags





# [1.0.0-alpha.0](https://bitbucket.org/winedevelopmentco/webapp/compare/@lib/flags@0.1.1...@lib/flags@1.0.0-alpha.0) (2021-05-31)

**Note:** Version bump only for package @lib/flags





## 0.1.1 (2021-02-25)

**Note:** Version bump only for package @lib/flags
