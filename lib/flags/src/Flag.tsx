import React from 'react';

import { getFlagByDollarCode } from './assets';


export default function Flag({ id = 'US', width = 24, height = 24 }: {
  id: string,
  width?: number,
  height?: number,
}) {
  const SvgComponent = getFlagByDollarCode(id);
  if (SvgComponent) return <SvgComponent width={width} height={height} />;
  return null;
}
