import Ad from './Ad';
import Ae from './Ae';
import Af from './Af';
import Ag from './Ag';
import Ai from './Ai';
import Al from './Al';
import Am from './Am';
import Ao from './Ao';
import Ar from './Ar';
import As from './As';
import At from './At';
import Au from './Au';
import Aw from './Aw';
import Ax from './Ax';
import Az from './Az';
import Ba from './Ba';
import Bb from './Bb';
import Bd from './Bd';
import Be from './Be';
import Bf from './Bf';
import Bg from './Bg';
import Bh from './Bh';
import Bi from './Bi';
import Bj from './Bj';
import Bl from './Bl';
import Bm from './Bm';
import Bn from './Bn';
import Bo from './Bo';
import Br from './Br';
import Bs from './Bs';
import Bt from './Bt';
import Bv from './Bv';
import Bw from './Bw';
import By from './By';
import Bz from './Bz';
import Ca from './Ca';
import Cc from './Cc';
import Cd from './Cd';
import Cf from './Cf';
import Cg from './Cg';
import Ch from './Ch';
import Ci from './Ci';
import Ck from './Ck';
import Cl from './Cl';
import Cm from './Cm';
import Cn from './Cn';
import Co from './Co';
import Cr from './Cr';
import Cu from './Cu';
import Cv from './Cv';
import Cw from './Cw';
import Cx from './Cx';
import Cy from './Cy';
import Cz from './Cz';
import De from './De';
import Dj from './Dj';
import Dk from './Dk';
import Dm from './Dm';
import Do from './Do';
import Dz from './Dz';
import Ec from './Ec';
import Ee from './Ee';
import Eg from './Eg';
import Er from './Er';
import Es from './Es';
import Et from './Et';
import Eu from './Eu';
import Fi from './Fi';
import Fj from './Fj';
import Fk from './Fk';
import Fm from './Fm';
import Fo from './Fo';
import Fr from './Fr';
import Ga from './Ga';
import Gb from './Gb';
import GbEng from './GbEng';
import GbNir from './GbNir';
import GbSct from './GbSct';
import GbWls from './GbWls';
import GbZet from './GbZet';
import Gd from './Gd';
import Ge from './Ge';
import Gf from './Gf';
import Gg from './Gg';
import Gh from './Gh';
import Gi from './Gi';
import Gl from './Gl';
import Gm from './Gm';
import Gn from './Gn';
import Gp from './Gp';
import Gq from './Gq';
import Gr from './Gr';
import Gs from './Gs';
import Gt from './Gt';
import Gu from './Gu';
import Gw from './Gw';
import Gy from './Gy';
import Hk from './Hk';
import Hm from './Hm';
import Hn from './Hn';
import Hr from './Hr';
import Ht from './Ht';
import Hu from './Hu';
import Id from './Id';
import Ie from './Ie';
import Il from './Il';
import Im from './Im';
import In from './In';
import Io from './Io';
import Iq from './Iq';
import Ir from './Ir';
import Is from './Is';
import It from './It';
import Je from './Je';
import Jm from './Jm';
import Jo from './Jo';
import Jp from './Jp';
import Ke from './Ke';
import Kg from './Kg';
import Kh from './Kh';
import Ki from './Ki';
import Km from './Km';
import Kn from './Kn';
import Kp from './Kp';
import Kr from './Kr';
import Kw from './Kw';
import Ky from './Ky';
import Kz from './Kz';
import La from './La';
import Lb from './Lb';
import Lc from './Lc';
import Lgbt from './Lgbt';
import Li from './Li';
import Lk from './Lk';
import Lr from './Lr';
import Ls from './Ls';
import Lt from './Lt';
import Lu from './Lu';
import Lv from './Lv';
import Ly from './Ly';
import Ma from './Ma';
import Mc from './Mc';
import Md from './Md';
import Me from './Me';
import Mf from './Mf';
import Mg from './Mg';
import Mh from './Mh';
import Mk from './Mk';
import Ml from './Ml';
import Mm from './Mm';
import Mn from './Mn';
import Mo from './Mo';
import Mp from './Mp';
import Mq from './Mq';
import Mr from './Mr';
import Ms from './Ms';
import Mt from './Mt';
import Mu from './Mu';
import Mv from './Mv';
import Mw from './Mw';
import Mx from './Mx';
import My from './My';
import Mz from './Mz';
import Na from './Na';
import Nc from './Nc';
import Ne from './Ne';
import Nf from './Nf';
import Ng from './Ng';
import Ni from './Ni';
import Nl from './Nl';
import No from './No';
import Np from './Np';
import Nr from './Nr';
import Nu from './Nu';
import Nz from './Nz';
import Om from './Om';
import Pa from './Pa';
import Pe from './Pe';
import Pf from './Pf';
import Pg from './Pg';
import Ph from './Ph';
import Pk from './Pk';
import Pl from './Pl';
import Pm from './Pm';
import Pn from './Pn';
import Pr from './Pr';
import Ps from './Ps';
import Pt from './Pt';
import Pw from './Pw';
import Py from './Py';
import Qa from './Qa';
import Re from './Re';
import Ro from './Ro';
import Rs from './Rs';
import Ru from './Ru';
import Rw from './Rw';
import Sa from './Sa';
import Sb from './Sb';
import Sc from './Sc';
import Sd from './Sd';
import Se from './Se';
import Sg from './Sg';
import Sh from './Sh';
import Si from './Si';
import Sj from './Sj';
import Sk from './Sk';
import Sl from './Sl';
import Sm from './Sm';
import Sn from './Sn';
import So from './So';
import Sr from './Sr';
import Ss from './Ss';
import St from './St';
import Sv from './Sv';
import Sx from './Sx';
import Sy from './Sy';
import Sz from './Sz';
import Tc from './Tc';
import Td from './Td';
import Tf from './Tf';
import Tg from './Tg';
import Th from './Th';
import Tj from './Tj';
import Tk from './Tk';
import Tl from './Tl';
import Tm from './Tm';
import Tn from './Tn';
import To from './To';
import Tr from './Tr';
import Tt from './Tt';
import Tv from './Tv';
import Tw from './Tw';
import Tz from './Tz';
import Ua from './Ua';
import Ug from './Ug';
import Um from './Um';
import Us from './Us';
import UsCa from './UsCa';
import Uy from './Uy';
import Uz from './Uz';
import Va from './Va';
import Vc from './Vc';
import Ve from './Ve';
import Vg from './Vg';
import Vi from './Vi';
import Vn from './Vn';
import Vu from './Vu';
import Wf from './Wf';
import Ws from './Ws';
import Xk from './Xk';
import Ye from './Ye';
import Yt from './Yt';
import Za from './Za';
import Zm from './Zm';
import Zw from './Zw';

export const flagSvgs = [
    {
        code: 'AD',
        region: 'Andorra',
        component: Ad,
    },
    {
        code: 'AE',
        region: 'United Arab Emirates',
        component: Ae,
    },
    {
        code: 'AF',
        region: 'Afghanistan',
        component: Af,
    },
    {
        code: 'AG',
        region: 'Antigua & Barbuda',
        component: Ag
    },
    {
        code: 'AI',
        region: 'Anguilla',
        
        component: Ai,
    },
    {
        code: 'AL',
        region: 'Albania',
        component: Al,
    },
    {
        code: 'AM',
        region: 'Armenia',
        component: Am,
    },
    {
        code: 'AO',
        region: 'Angola',
        component: Ao,
    },
    {
        code: 'AR',
        region: 'Argentina',
        component: Ar,
    },
    {
        code: 'AS',
        region: 'American Samoa',
        component: As,
    },
    {
        code: 'AT',
        region: 'Austria',
        component: At,
    },
    {
        code: 'AU',
        region: 'Australia',
        component: Au,
    },
    {
        code: 'AW',
        region: 'Aruba',
        component: Aw,
    },
    {
        code: 'AX',
        region: 'Åland Islands',
        component: Ax,
    },
    {
        code: 'AZ',
        region: 'Azerbaijan',
        component: Az,
    },
    {
        code: 'BA',
        region: 'Bosnia & Herzegovina',
        component: Ba,
    },
    {
        code: 'BB',
        region: 'Barbados',
        component: Bb,
    },
    {
        code: 'BD',
        region: 'Bangladesh',
        component: Bd,
    },
    {
        code: 'BE',
        region: 'Belgium',
        component: Be,
    },
    {
        code: 'BF',
        region: 'Burkina Faso',
        component: Bf,
    },
    {
        code: 'BG',
        region: 'Bulgaria',
        component: Bg,
    },
    {
        code: 'BH',
        region: 'Bahrain',
        component: Bh,
    },
    {
        code: 'BI',
        region: 'Burundi',
        component: Bi,
    },
    {
        code: 'BJ',
        region: 'Benin',
        component: Bj,
    },
    {
        code: 'BL',
        region: 'St. Barthélemy',
        component: Bl,
    },
    {
        code: 'BM',
        region: 'Bermuda',
        component: Bm,
    },
    {
        code: 'BN',
        region: 'Brunei',
        component: Bn,
    },
    {
        code: 'BO',
        region: 'Bolivia',
        component: Bo,
    },
    {
        code: 'BR',
        region: 'Brazil',
        
         component: Br,
    },
    {
        code: 'BS',
        region: 'Bahamas',
        component: Bs,
    },
    {
        code: 'BT',
        region: 'Bhutan',
        component: Bt,
    },
    {
        code: 'BV',
        region: 'Bouvet Island',
        component: Bv,
    },
    {
        code: 'BW',
        region: 'Botswana',
        component: Bw,
    },
    {
        code: 'BY',
        region: 'Belarus',
        component: By,
    },
    {
        code: 'BZ',
        region: 'Belize',
        component: Bz,
    },
    {
        code: 'CA',
        region: 'Canada',
        component: Ca,
    },
    {
        code: 'CC',
        region: 'Cocos (Keeling) Islands',
        component: Cc,
    },
    {
        code: 'CD',
        region: 'Congo - Kinshasa',
        component: Cd,
    },
    {
        code: 'CF',
        region: 'Central African Republic',
        component: Cf,
    },
    {
        code: 'CG',
        region: 'Congo - Brazzaville',
        component: Cg,
    },
    {
        code: 'CH',
        region: 'Switzerland',
        component: Ch,
    },
    {
        code: 'CI',
        region: 'Côte d’Ivoire',
        component: Ci,
    },
    {
        code: 'CK',
        region: 'Cook Islands',
        component: Ck,
    },
    {
        code: 'CL',
        region: 'Chile',
        component: Cl,
    },
    {
        code: 'CM',
        region: 'Cameroon',
        component: Cm,
    },
    {
        code: 'CN',
        region: 'China',
        component: Cn,
    },
    {
        code: 'CO',
        region: 'Colombia',
        component: Co,
    },
    {
        code: 'CR',
        region: 'Costa Rica',
        component: Cr,
    },
    {
        code: 'CU',
        region: 'Cuba',
        component: Cu,
    },
    {
        code: 'CV',
        region: 'Cape Verde',
        component: Cv,
    },
    {
        code: 'CW',
        region: 'Curaçao',
        component: Cw,
    },
    {
        code: 'CX',
        region: 'Christmas Island',
        component: Cx,
    },
    {
        code: 'CY',
        region: 'Cyprus',
        component: Cy,
    },
    {
        code: 'CZ',
        region: 'Czech Republic',
        component: Cz,
    },
    {
        code: 'DE',
        region: 'Germany',
        component: De,
    },
    {
        code: 'DJ',
        region: 'Djibouti',
        component: Dj,
    },
    {
        code: 'DK',
        region: 'Denmark',
        component: Dk,
    },
    {
        code: 'DM',
        region: 'Dominica',
        component: Dm,
    },
    {
        code: 'DO',
        region: 'Dominican Republic',
        component: Do,
    },
    {
        code: 'DZ',
        region: 'Algeria',
        component: Dz,
    },
    {
        code: 'EC',
        region: 'Ecuador',
        component: Ec,
    },
    {
        code: 'EE',
        region: 'Estonia',
        component: Ee,
    },
    {
        code: 'EG',
        region: 'Egypt',
        component: Eg,
    },
    {
        code: 'ER',
        region: 'Eritrea',
        component: Er,
    },
    {
        code: 'ES',
        region: 'Spain',
        component: Es,
    },
    {
        code: 'ET',
        region: 'Ethiopia',
        component: Et,
    },
    {
        code: 'EU',
        region: 'European Union',
        component: Eu,
    },
    {
        code: 'FI',
        region: 'Finland',
        component: Fi,
    },
    {
        code: 'FJ',
        region: 'Fiji',
         component: Fj
        
    },
    {
        code: 'FK',
        region: 'Falkland Islands',
         component: Fk
        
    },
    {
        code: 'FM',
        region: 'Micronesia',
        component: Fm,
    },
    {
        code: 'FO',
        region: 'Faroe Islands',
        component: Fo,
    },
    {
        code: 'FR',
        region: 'France',
        component: Fr,
    },
    {
        code: 'GA',
        region: 'Gabon',
        component: Ga,
    },
    {
        code: 'GB',
        region: 'United Kingdom',
        component: Gb,
    },
    {
        code: 'GB-ENG',
        region: 'United Kingdom',
        component: GbEng,
    },
    {
        code: 'GB-NIR',
        region: 'United Kingdom',
         component: GbNir
        
    },
    {
        code: 'GB-SCT',
        region: 'United Kingdom',
        component: GbSct,
    },
    {
        code: 'GB-WLS',
        region: 'United Kingdom',
        component: GbWls,
    },
    {
        code: 'GB-ZET',
        region: 'United Kingdom',
        component: GbZet,
    },
    {
        code: 'GD',
        region: 'Grenada',
         component: Gd
        
    },
    {
        code: 'GE',
        region: 'Georgia',
        component: Ge,
    },
    {
        code: 'GF',
        region: 'French Guiana',
        component: Gf,
    },
    {
        code: 'GG',
        region: 'Guernsey',
        component: Gg,
    },
    {
        code: 'GH',
        region: 'Ghana',
        component: Gh,
    },
    {
        code: 'GI',
        region: 'Gibraltar',
        component: Gi,
    },
    {
        code: 'GL',
        region: 'Greenland',
        
         component: Gl,
    },
    {
        code: 'GM',
        region: 'Gambia',
        component: Gm,
    },
    {
        code: 'GN',
        region: 'Guinea',
        component: Gn,
    },
    {
        code: 'GP',
        region: 'Guadeloupe',
        component: Gp,
    },
    {
        code: 'GQ',
        region: 'Equatorial Guinea',
        component: Gq,
    },
    {
        code: 'GR',
        region: 'Greece',
        component: Gr,
    },
    {
        code: 'GS',
        region: 'So. Georgia & So. Sandwich Isl.',
         component: Gs,
        
    },
    {
        code: 'GT',
        region: 'Guatemala',
        component: Gt,
    },
    {
        code: 'GU',
        region: 'Guam',
        
         component: Gu,
    },
    {
        code: 'GW',
        region: 'Guinea-Bissau',
        component: Gw,
    },
    {
        code: 'GY',
        region: 'Guyana',
        component: Gy,
    },
    {
        code: 'HK',
        region: 'Hong Kong',
        component: Hk,
    },
    {
        code: 'HM',
        region: 'Heard & McDonald Islands',
        component: Hm,
    },
    {
        code: 'HN',
        region: 'Honduras',
        component: Hn,
    },
    {
        code: 'HR',
        region: 'Croatia',
        component: Hr,
    },
    {
        code: 'HT',
        region: 'Haiti',
        
         component: Ht,
    },
    {
        code: 'HU',
        region: 'Hungary',
        component: Hu,
    },
    {
        code: 'ID',
        region: 'Indonesia',
        component: Id,
    },
    {
        code: 'IE',
        region: 'Ireland',
        component: Ie,
    },
    {
        code: 'IL',
        region: 'Israel',
        component: Il,
    },
    {
        code: 'IM',
        region: 'Isle of Man',
        component: Im,
    },
    {
        code: 'IN',
        region: 'India',
        component: In,
    },
    {
        code: 'IO',
        region: 'British Indian Ocean Territory',
        component: Io,
    },
    {
        code: 'IQ',
        region: 'Iraq',
        component: Iq,
    },
    {
        code: 'IR',
        region: 'Iran',
        component: Ir,
    },
    {
        code: 'IS',
        region: 'Iceland',
        component: Is,
    },
    {
        code: 'IT',
        region: 'Italy',
        component: It,
    },
    {
        code: 'JE',
        region: 'Jersey',
        component: Je,
    },
    {
        code: 'JM',
        region: 'Jamaica',
        component: Jm,
    },
    {
        code: 'JO',
        region: 'Jordan',
        component: Jo,
    },
    {
        code: 'JP',
        region: 'Japan',
        component: Jp,
    },
    {
        code: 'KE',
        region: 'Kenya',
        
         component: Ke,
    },
    {
        code: 'KG',
        region: 'Kyrgyzstan',
        component: Kg,
    },
    {
        code: 'KH',
        region: 'Cambodia',
        component: Kh,
    },
    {
        code: 'KI',
        region: 'Kiribati',
        component: Ki,
    },
    {
        code: 'KM',
        region: 'Comoros',
        component: Km,
    },
    {
        code: 'KN',
        region: 'St. Kitts & Nevis',
        component: Kn,
    },
    {
        code: 'KP',
        region: 'North Korea',
        component: Kp,
    },
    {
        code: 'KR',
        region: 'South Korea',
        
         component: Kr
    },
    {
        code: 'KW',
        region: 'Kuwait',
        component: Kw,
    },
    {
        code: 'KY',
        region: 'Cayman Islands',
        
         component: Ky
    },
    {
        code: 'KZ',
        region: 'Kazakhstan',
        component: Kz,
    },
    {
        code: 'LA',
        region: 'Laos',
        component: La,
    },
    {
        code: 'LB',
        region: 'Lebanon',
        component: Lb,
    },
    {
        code: 'LC',
        region: 'St. Lucia',
        component: Lc,
    },
    {
        code: 'LGBT',
        region: 'Pride',
        component: Lgbt,
    },
    {
        code: 'LI',
        region: 'Liechtenstein',
        component: Li,
    },
    {
        code: 'LK',
        region: 'Sri Lanka',
        component: Lk,
    },
    {
        code: 'LR',
        region: 'Liberia',
         component: Lr
        
    },
    {
        code: 'LS',
        region: 'Lesotho',
        component: Ls,
    },
    {
        code: 'LT',
        region: 'Lithuania',
        component: Lt,
    },
    {
        code: 'LU',
        region: 'Luxembourg',
        component: Lu,
    },
    {
        code: 'LV',
        region: 'Latvia',
        component: Lv,
    },
    {
        code: 'LY',
        region: 'Libya',
        component: Ly,
    },
    {
        code: 'MA',
        region: 'Morocco',
        component: Ma,
    },
    {
        code: 'MC',
        region: 'Monaco',
        component: Mc,
    },
    {
        code: 'MD',
        region: 'Moldova',
         component: Md
        
    },
    {
        code: 'ME',
        region: 'Montenegro',
        component: Me,
    },
    {
        code: 'MF',
        region: 'St. Martin',
        component: Mf,
    },
    {
        code: 'MG',
        region: 'Madagascar',
        component: Mg,
    },
    {
        code: 'MH',
        region: 'Marshall Islands',
        component: Mh,
    },
    {
        code: 'MK',
        region: 'Macedonia',
        component: Mk,
    },
    {
        code: 'ML',
        region: 'Mali',
        component: Ml,
    },
    {
        code: 'MM',
        region: 'Myanmar',
        component: Mm,
    },
    {
        code: 'MN',
        region: 'Mongolia',
        component: Mn,
    },
    {
        code: 'MO',
        region: 'Macau',
        component: Mo,
    },
    {
        code: 'MP',
        region: 'Northern Mariana Islands',
        component: Mp,
    },
    {
        code: 'MQ',
        region: 'Martinique',
        component: Mq,
    },
    {
        code: 'MR',
        region: 'Mauritania',
        component: Mr,
    },
    {
        code: 'MS',
        region: 'Montserrat',
         component: Ms
        
    },
    {
        code: 'MT',
        region: 'Malta',
         component: Mt
        
    },
    {
        code: 'MU',
        region: 'Mauritius',
        component: Mu,
    },
    {
        code: 'MV',
        region: 'Maldives',
        component: Mv,
    },
    {
        code: 'MW',
        region: 'Malawi',
        component: Mw,
    },
    {
        code: 'MX',
        region: 'Mexico',
        component: Mx,
    },
    {
        code: 'MY',
        region: 'Malaysia',
        component: My,
    },
    {
        code: 'MZ',
        region: 'Mozambique',
        component: Mz,
    },
    {
        code: 'NA',
        region: 'Namibia',
         component: Na
        
    },
    {
        code: 'NC',
        region: 'New Caledonia',
        component: Nc,
    },
    {
        code: 'NE',
        region: 'Niger',
        component: Ne,
    },
    {
        code: 'NF',
        region: 'Norfolk Island',
        component: Nf,
    },
    {
        code: 'NG',
        region: 'Nigeria',
        component: Ng,
    },
    {
        code: 'NI',
        region: 'Nicaragua',
        component: Ni,
    },
    {
        code: 'NL',
        region: 'Netherlands',
        component: Nl,
    },
    {
        code: 'NO',
        region: 'Norway',
        component: No,
    },
    {
        code: 'NP',
        region: 'Nepal',
         component: Np
        
    },
    {
        code: 'NR',
        region: 'Nauru',
        component: Nr,
    },
    {
        code: 'NU',
        region: 'Niue',
        component: Nu,
    },
    {
        code: 'NZ',
        region: 'New Zealand',
        component: Nz,
    },
    {
        code: 'OM',
        region: 'Oman',
        component: Om,
    },
    {
        code: 'PA',
        region: 'Panama',
        component: Pa,
    },
    {
        code: 'PE',
        region: 'Peru',
        component: Pe,
    },
    {
        code: 'PF',
        region: 'French Polynesia',
         component: Pf
        
    },
    {
        code: 'PG',
        region: 'Papua New Guinea',
        component: Pg,
    },
    {
        code: 'PH',
        region: 'Philippines',
        component: Ph,
    },
    {
        code: 'PK',
        region: 'Pakistan',
         component: Pk
        
    },
    {
        code: 'PL',
        region: 'Poland',
        component: Pl,
    },
    {
        code: 'PM',
        region: 'St. Pierre & Miquelon',
         component: Pm
        
    },
    {
        code: 'PN',
        region: 'Pitcairn Islands',
         component: Pn
        
    },
    {
        code: 'PR',
        region: 'Puerto Rico',
        component: Pr,
    },
    {
        code: 'PS',
        region: 'Palestinian Territories',
        component: Ps,
    },
    {
        code: 'PT',
        region: 'Portugal',
        component: Pt,
    },
    {
        code: 'PW',
        region: 'Palau',
        component: Pw,
    },
    {
        code: 'PY',
        region: 'Paraguay',
        component: Py,
    },
    {
        code: 'QA',
        region: 'Qatar',
        component: Qa,
    },
    {
        code: 'RE',
        region: 'Réunion',
        component: Re,
    },
    {
        code: 'RO',
        region: 'Romania',
        component: Ro,
    },
    {
        code: 'RS',
        region: 'Serbia',
         component: Rs
        
    },
    {
        code: 'RU',
        region: 'Russia',
        component: Ru,
    },
    {
        code: 'RW',
        region: 'Rwanda',
        component: Rw,
    },
    {
        code: 'SA',
        region: 'Saudi Arabia',
        component: Sa,
    },
    {
        code: 'SB',
        region: 'Solomon Islands',
        component: Sb,
    },
    {
        code: 'SC',
        region: 'Seychelles',
        component: Sc,
    },
    {
        code: 'SD',
        region: 'Sudan',
        component: Sd,
    },
    {
        code: 'SE',
        region: 'Sweden',
        component: Se,
    },
    {
        code: 'SG',
        region: 'Singapore',
        component: Sg,
    },
    {
        code: 'SH',
        region: 'St. Helena',
         component: Sh
        
    },
    {
        code: 'SI',
        region: 'Slovenia',
        component: Si,
    },
    {
        code: 'SJ',
        region: 'Svalbard & Jan Mayen',
        component: Sj,
    },
    {
        code: 'SK',
        region: 'Slovakia',
         component: Sk,
        
    },
    {
        code: 'SL',
        region: 'Sierra Leone',
        component: Sl,
    },
    {
        code: 'SM',
        region: 'San Marino',
        component: Sm,
    },
    {
        code: 'SN',
        region: 'Senegal',
        component: Sn,
    },
    {
        code: 'SO',
        region: 'Somalia',
        component: So,
    },
    {
        code: 'SR',
        region: 'Suriname',
        component: Sr,
    },
    {
        code: 'SS',
        region: 'South Sudan',
        component: Ss,
    },
    {
        code: 'ST',
        region: 'São Tomé & Príncipe',
        component: St,
    },
    {
        code: 'SV',
        region: 'El Salvador',
        component: Sv,
    },
    {
        code: 'SX',
        region: 'Sint Maarten',
        component: Sx,
    },
    {
        code: 'SY',
        region: 'Syria',
        component: Sy,
    },
    {
        code: 'SZ',
        region: 'Swaziland',
         component: Sz
        
    },
    {
        code: 'TC',
        region: 'Turks & Caicos Islands',
        component: Tc,
    },
    {
        code: 'TD',
        region: 'Chad',
        component: Td,
    },
    {
        code: 'TF',
        region: 'French Southern Territories',
        component: Tf,
    },
    {
        code: 'TG',
        region: 'Togo',
        component: Tg,
    },
    {
        code: 'TH',
        region: 'Thailand',
        component: Th,
    },
    {
        code: 'TJ',
        region: 'Tajikistan',
        component: Tj,
    },
    {
        code: 'TK',
        region: 'Tokelau',
        component: Tk,
    },
    {
        code: 'TL',
        region: 'Timor-Leste',
        component: Tl,
    },
    {
        code: 'TM',
        region: 'Turkmenistan',
         component: Tm
        
    },
    {
        code: 'TN',
        region: 'Tunisia',
        component: Tn,
    },
    {
        code: 'TO',
        region: 'Tonga',
        component: To,
    },
    {
        code: 'TR',
        region: 'Turkey',
        component: Tr,
    },
    {
        code: 'TT',
        region: 'Trinidad & Tobago',
        component: Tt,
    },
    {
        code: 'TV',
        region: 'Tuvalu',
        component: Tv,
    },
    {
        code: 'TW',
        region: 'Taiwan',
        component: Tw,
    },
    {
        code: 'TZ',
        region: 'Tanzania',
        component: Tz,
    },
    {
        code: 'UA',
        region: 'Ukraine',
        component: Ua,
    },
    {
        code: 'UG',
        region: 'Uganda',
        component: Ug,
    },
    {
        code: 'UM',
        region: 'U.S. Outlying Islands',
        component: Um,
    },
    {
        code: 'US',
        region: 'United States',
        component: Us,
    },
    {
        code: 'US-CA',
        region: 'California',
         component: UsCa
        
    },
    {
        code: 'UY',
        region: 'Uruguay',
        component: Uy,
    },
    {
        code: 'UZ',
        region: 'Uzbekistan',
        component: Uz,
    },
    {
        code: 'VA',
        region: 'Vatican City',
        component: Va,
    },
    {
        code: 'VC',
        region: 'St. Vincent & Grenadines',
        component: Vc,
    },
    {
        code: 'VE',
        region: 'Venezuela',
        component: Ve,
    },
    {
        code: 'VG',
        region: 'British Virgin Islands',
        component: Vg,
    },
    {
        code: 'VI',
        region: 'U.S. Virgin Islands',
         component: Vi
        
    },
    {
        code: 'VN',
        region: 'Vietnam',
        component: Vn,
    },
    {
        code: 'VU',
        region: 'Vanuatu',
        component: Vu,
    },
    {
        code: 'WF',
        region: 'Wallis & Futuna',
        component: Wf,
    },
    {
        code: 'WS',
        region: 'Samoa',
        component: Ws,
    },
    {
        code: 'XK',
        region: 'Kosovo',
        component: Xk,
    },
    {
        code: 'YE',
        region: 'Yemen',
        component: Ye,
    },
    {
        code: 'YT',
        region: 'Mayotte',
         component: Yt
        
    },
    {
        code: 'ZA',
        region: 'South Africa',
        component: Za,
    },
    {
        code: 'ZM',
        region: 'Zambia',
        component: Zm,
    },
    {
        code: 'ZW',
        region: 'Zimbabwe',
        component: Zw,
    }
]

export const getFlagObjectByCode = (code: string) => {
  return flagSvgs.find(flagSvg => flagSvg.code === code);
}

export const getFlagByDollarCode = (dollarCode: string): React.FC<any> | null => {
  let code = dollarCode.slice(0, 2);
  if (dollarCode === 'ANG') code = 'NL';

  const flagObject = getFlagObjectByCode(code);

  if (flagObject) return flagObject.component;
  return null;
}
