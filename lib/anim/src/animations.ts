import { Animated, Easing, Platform } from 'react-native';

const isMobile = Platform.OS === 'android' || Platform.OS === 'ios';

const animFactory = (animation: typeof Animated.spring, defaultOptions: {}) =>
  (driver: Animated.Value, toValue: number, options: {}) => {
  return animation(driver, {
    ...defaultOptions,
    ...options,
    toValue,
    useNativeDriver: isMobile,
  })
}
export const timing = animFactory(Animated.timing, { easing: Easing.linear });

export const jump = (translateY: Animated.Value, options?: {}) => Animated.loop(
  timing(translateY, 1, { duration: 2000, ...options })
);
export const spring = animFactory(Animated.spring, {});
