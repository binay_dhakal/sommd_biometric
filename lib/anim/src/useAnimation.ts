import { useRef } from 'react';
import { Animated, } from 'react-native';

type Driver = Animated.Value | { [arg0: string]: Animated.Value }

function createDriver(v: number | { [arg0: string]: number }): Driver {
  if (typeof v === 'number') return new Animated.Value(v);
  if (typeof v === 'object') {
    return Object.keys(v).reduce((res: { [arg0: string]: any }, key: string) => {
      res[key] = createDriver(v[key]);
      return res;
    }, {});
  }

  throw new Error(`Cannot create an animated value from ${v}`);
}

export default function useAnimation(init: number | Function, styler: (arg0: Driver) => any) {
  const ref = useRef<any>();
  if (!ref.current) {
    const driver = createDriver(typeof init === 'function' ? init() : init);
    const style = styler(driver);
    ref.current = [style, driver];
  }
  return ref.current;
}
