import AsyncStorage from "@react-native-community/async-storage"

export const read = async (key: string) => {
  const data = await AsyncStorage.getItem(key);

  if (!data) return null;
  return JSON.parse(data);
}

export const write = async<T> (key: string, data: T, cb?: () => void) => {
  await AsyncStorage.setItem(key, JSON.stringify(data), cb)
}
