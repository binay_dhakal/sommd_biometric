import React, {useEffect, useState} from 'react';
import {AppRegistry, Platform} from 'react-native';
import App from '@sommd/shared/src/App';
import {usePushNotification} from '@sommd/shared/src/PushService';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import SplashScreen from 'react-native-splash-screen';

// @ts-ignore
import {name as appName} from './app.json';

const SPLASH_WAITING_TIME = 3000;

function Wrapper() {
  const [visibility, setVisibility] = useState(true);

  useEffect(() => {
    const id = setTimeout(() => {
      SplashScreen.hide();
      setVisibility(false);
    }, SPLASH_WAITING_TIME);
    () => {
      clearTimeout(id);
    };
    // Alert.alert('Bundle Id', DeviceInfo.getBundleId());
  }, []);
  usePushNotification((event: any) => {
    // TODO: Process notification
    event.finish(PushNotificationIOS.FetchResult.NoData);
  });

  if (visibility) {
    return Platform.OS === 'ios' ? <App /> : null;
  }
  return <App />;
}

AppRegistry.registerComponent(appName, () => Wrapper);
