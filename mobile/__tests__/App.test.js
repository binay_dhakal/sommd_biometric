import 'react-native';
import React from 'react';
import {render, fireEvent, waitFor} from '@testing-library/react-native'
import AsyncStorage from '@react-native-community/async-storage'
import 'mutationobserver-shim';

import App from '@sommd/shared/src/App';
import { Router } from '@sommd/shared/src/Routers';
import { SESSION_KEY } from '@sommd/shared/src/session';
import { AppProvider } from '@sommd/shared/src/context';

jest.mock('@react-native-community/async-storage', () => ({ setItem: jest.fn(), getItem: jest.fn() }))

function renderWithRouter() {
  return render(
    <AppProvider value={{ user: null }}>
      <Router>
        <App />
      </Router>
    </AppProvider>
  )
}

test('full app rendering/navigation', async () => {
  const { getByText, queryByText, } = renderWithRouter();
  await waitFor(() => expect(AsyncStorage.getItem).toHaveBeenCalledWith(SESSION_KEY));

  expect(queryByText('Wine Curated Not Bought!!!')).toBeDefined();

  fireEvent.press(getByText(/Get Started/i))

  expect(queryByText('Login')).toBeDefined();
})


test('landing on a bad page', async () => {
  const { queryByText } = renderWithRouter();

  await waitFor(() => expect(queryByText('404. Route not found')).toBeDefined());
})
