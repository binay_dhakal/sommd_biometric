const fs = require('fs')
const path = require('path')
const webpack = require('webpack')

const {alias} = require('react-app-rewire-alias')

const appDirectory = fs.realpathSync(process.cwd())
const resolveApp = relativePath => path.resolve(appDirectory, relativePath)

const ReactWebConfig = require('react-web-config/lib/ReactWebConfig').ReactWebConfig;
const envFilePath = path.resolve(__dirname, '../mobile/.env');

// our packages that will now be included in the CRA build step
const appIncludes = [
  resolveApp('src'),
  resolveApp('../shared/src'),
  resolveApp('../node_modules/@react-native-community/slider'),
  resolveApp('../node_modules/react-router-native'),
  resolveApp('../node_modules/react-native-config'),
  resolveApp('../node_modules/react-native-gesture-handler'),
  resolveApp('../node_modules/react-native-google-places-autocomplete')
]

module.exports = function override(config, env) {
  // allow importing from outside of src folder
  config.resolve.plugins = config.resolve.plugins.filter(
    plugin => plugin.constructor.name !== 'ModuleScopePlugin'
  )
  config.module.rules[0].include = appIncludes
  config.module.rules[1].oneOf[2].include = appIncludes
  config.module.rules[1].oneOf[2].options.plugins = [
    require.resolve('babel-plugin-react-native-web'),
    require.resolve('@babel/plugin-proposal-class-properties'),
  ]

  config.plugins.push(
    new webpack.DefinePlugin({ __DEV__: env !== 'production' }),
    ReactWebConfig(envFilePath),
  )
  config.module.rules.forEach(rule => {
    if(rule.oneOf){
      rule.oneOf.unshift({
        test: /\.svg$/,
        exclude: /node_modules/,
        use: [{
          loader: require.resolve('@svgr/webpack'),
          options: {
            viewBox: false,
          }
        }]
      });
    }
  })
  config.resolve.extensions.push('.svg')
  alias({
    'react-native-config': 'react-web-config',
    'react-native-linear-gradient': 'react-native-web-linear-gradient',
  })(config)

  return config
}
