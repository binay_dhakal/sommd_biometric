import React from 'react';
import { render, screen, waitForElement, wait, } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import 'mutationobserver-shim';

import axiosMock from 'axios';
import { AsyncForm } from '@sommd/shared/src/components';
import { loginForm } from '@sommd/shared/src/forms';
import { AppProvider } from '@sommd/shared/src/context';
import { BASE_URL } from '@sommd/shared/src/utils/useApi';

const FORM_TO_TEST = loginForm;
const DUMMY_DATA = {
  username: 'shishir.khadka@gmail.com',
  password: 'Test@123',
}

const API_ENDPOINT = 'login';
const API_RESPONSE = { id_token: 'fake_token', expires_in: 9000 }
const SUCCESS_RESPONSE = { success: true, error: false, data: API_RESPONSE };
const ERROR_RESPONSE = { error: true, success: true, message: 'Api Error' };

const setup = (formData) => {
  const mockFn = jest.fn();
  const r = render (
    <AppProvider value={{ user: null }}>
      <AsyncForm
        formData={formData}
        endpoint="login"
        onComplete={mockFn}
      />
    </AppProvider>
  )
  const fields = FORM_TO_TEST.reduce((res, d) => {
    res[d.name] = screen.getByPlaceholderText(`${d.placeholder}`);
    return res;
  }, {});
  const button = screen.getByRole('button', { name: /submit/i });

  return { ...r, mockFn, fields, button };
}

describe('test AsyncForm with formData', () => {
  test('it renders input as defined in formData', async () => {
    setup(FORM_TO_TEST);

    FORM_TO_TEST.forEach(d => {
      expect(screen.queryByPlaceholderText(`${d.placeholder}`)).toBeInTheDocument();
    });
  });
 
  test('it submits form data after valid request and disables the button while submitting', async () => {
    axiosMock.request.mockResolvedValueOnce({
      data: SUCCESS_RESPONSE,
    });
    const { fields, button, mockFn, } = setup(FORM_TO_TEST);

    Object.keys(fields).forEach(f => {
      userEvent.type(fields[f], DUMMY_DATA[f]);
    })
    userEvent.click(button);
 
    await wait(() => expect(axiosMock.request).toHaveBeenCalledTimes(1));
    expect(axiosMock.request).toHaveBeenCalledWith({
      data: DUMMY_DATA,
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      url: BASE_URL + API_ENDPOINT,
    });
    expect(mockFn).toHaveBeenCalledWith(API_RESPONSE);
  });

  test('it renders error message on api error', async () => {
    const { fields, button } = setup(FORM_TO_TEST);
    axiosMock.request.mockResolvedValueOnce({
      data: ERROR_RESPONSE,
    })

    Object.keys(fields).forEach(f => {
      userEvent.type(fields[f], DUMMY_DATA[f] + '_'); // appending _ at the end to make it invalid
    })
    userEvent.click(button);

    await wait(() => expect(axiosMock.request).toHaveBeenCalledTimes(2));
    await waitForElement(() => screen.queryByText('Api Error'));
  })
});
