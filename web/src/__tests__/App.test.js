import React from 'react';
import {render, screen, fireEvent, wait} from '@testing-library/react'
import 'mutationobserver-shim';

import App from '@sommd/shared/src/App';
import { Router } from '@sommd/shared/src/Routers';
import { SESSION_KEY } from '@sommd/shared/src/session';

function renderWithRouter(ui, {route = '/'} = {}) {
  window.history.pushState({}, 'Test page', route)

  return render(ui, {wrapper: Router})
}


test('full app rendering/navigation', async () => {
  const {container} = renderWithRouter(<App />)

  await wait(() => expect(window.localStorage).toHaveBeenCalledWith(SESSION_KEY));
  expect(container.innerHTML).toContain('Wine Curated Not Bought!!!');

  const leftClick = {button: 0}

  fireEvent.click(screen.getByText(/Get Started/i), leftClick)

  expect(container.innerHTML).toContain('Login')
})


test('landing on a bad page', async () => {
  const {container} = renderWithRouter(<App />, {
    route: '/something-that-does-not-match',
  })

  await wait(() => expect(container.innerHTML).toContain('404. Route not found'));
})
