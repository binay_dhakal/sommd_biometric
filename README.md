## Getting Started

Setup the dependencies for the project

`yarn`

## Environment Variables

Create a `.env` file inside `mobile` package with following contents
****

	WINE_APP_BASE_URL=https://wdl9igkr97.execute-api.us-east-1.amazonaws.com
	SKIP_PREFLIGHT_CHECK=true
****


Create a `.env` file inside `mobile/ios/fastlane` package with following contents
****

	KEYCHAIN_NAME = 
	KEYCHAIN_PASSWORD =
	S3_BUCKET = 
****


Add the following variables to your `bash_profile`
****

	export AWS_DEFAULT_REGION = 
	export AWS_ACCESS_KEY_ID = 
	export AWS_SECRET_ACCESS_KEY = 
****


## Available Scripts

In the project directory, you can run:

### `yarn build`

Builds the modules for production to the `build` folder of respective workspaces.

### `yarn start:web`

Runs the web app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Running mobile simulations

## IOS 
### IOS Setup
Make sure the dependencies are installed for `React Native CLI Quickstart`
https://reactnative.dev/docs/environment-setup
#### `cd mobile/ios && pod install`

### IOS simulation
From the root folder, run the following:
#### `yarn start:mobile` and `yarn start:ios` 
on different terminals, i.e. both commands should run in parallel


## Deploying IOS app to Testflight
Make sure you have `aws` command-line installed.

From the root folder, run the following command to start the deployment process
#### `yarn deploy:ios-dev` 
