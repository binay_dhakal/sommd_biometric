import React from 'react';
import 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { write } from '@lib/storage';

import {
  ForgotPassword,
  Login,
  Signup,
  Verification,
  Onboarding,
  ProductDetail,
  PairingDetail,
  SearchByImage,
  Cellar,
  Pairing,
  Camera,
  Cart,
  Profile,
  Feed,
  AddressForm,
  FlavourProfile,
  Invitations,
  Orders,
  Addresses,
} from './screens';
import BottomNav from './BottomNav';
import { DEFAULT_TAB } from './constants';
import { DEFAULT_SCREEN, ONBOARDING_INIT_DATA_KEY } from './constants';


export const TAB_SCREENS = [
  { name: 'Cellar', Comp: Cellar, },
  { name: 'Pairing', Comp: Pairing, },
  { name: 'Search', Comp: SearchByImage, },
  { name: 'Cart', Comp: Cart, },
  { name: 'Profile', Comp: Profile },
  { name: 'Feed', Comp: Feed },
];
export interface Screen {
  name: string,
  Comp: React.FC<any>,
  route?: 'restricted' | 'protected',
  options?: any,
  props?: any,
}


export const AUTH_SCREENS: Screen[] = [
  { name: 'Login', Comp: Login, route: 'restricted' },
  { name: 'ForgotPassword', Comp: ForgotPassword, route: 'restricted' },
  { name: 'Signup', Comp: Signup, route: 'restricted' },
  { name: 'Verification', Comp: Verification, route: 'restricted' },
]
export const SCREENS: Screen[] = [
  { name: 'BottomTabs', Comp: BottomTabs, options: { headerShown: true }, route: 'protected' },
  {
    name: 'Onboarding',
    Comp: Onboarding,
    props: {
      onComplete: async ({ navigation }: { navigation : any }) => {
        await write(ONBOARDING_INIT_DATA_KEY, '');
        navigation.replace(DEFAULT_SCREEN);
      }
    },
      route: 'protected',
    },
  {
    name: 'EditPreferences',
    Comp: Onboarding,
    props: {
      onComplete: ({ navigation }: { navigation : any }) => navigation.goBack(),
      onBack: ({ navigation }: { navigation : any }) => navigation.goBack(),
      onSkip: true,
    },
    route: 'protected',
  },
  { name: 'ProductDetail', Comp: ProductDetail, route: 'protected' },
  { name: 'PairingDetail', Comp: PairingDetail, route: 'protected' },
  { name: 'Camera', Comp: Camera, route: 'protected' },
  { name: 'AddressForm', Comp: AddressForm, route: 'protected' },
  { name: 'Invitations', Comp: Invitations, route: 'protected' },
  { name: 'Addresses', Comp: Addresses, route: 'protected' },
  { name: 'FlavourProfile', Comp: FlavourProfile, route: 'protected' },
  { name: 'Orders', Comp: Orders, route: 'protected' },
]



const Tab = createBottomTabNavigator();
export function BottomTabs() {
  return (
    <Tab.Navigator
      initialRouteName={DEFAULT_TAB}
      tabBarOptions={{ 
        activeTintColor: '#fff',
        inactiveTintColor: '#fff8',
      }}
      tabBar={BottomNav}
    >
      {TAB_SCREENS.map(({ name, Comp }) => (
        <Tab.Screen key={name} name={name} component={Comp} />
      ))}
    </Tab.Navigator>
  )
}

export const LINKING = {
  prefixes: ['https://wineapp.com', 'wineapp://'],
  config: {
    screens: {
      Landing: { path: '', },
      Login: {
        path: 'login/:referral?',
        stringify: {
          referral: () => '',
        },
      },
      Signup: 'signup',
      Verification: 'verification',
      ForgotPassword: 'forgotpass',
      Onboarding: 'preferences',
      EditPreferences: 'editPreferences',
      ProductDetail: "product/:id",
      PairingDetail: "pairing/:id",
      Invitations: 'invitaions',
      Addresses: 'addresses',
      FlavourProfile: 'flavourProfile',
      Orders: 'orders',
      Camera: "camera",
      BottomTabs: {
        screens: {
          Profile: "profile",
          Pairing: "pairing",
          Search: {
            path: "search/:type?/:value?"
          },
          Cellar: "cellar",
          Feed: "feed",
        }
      }
    },
  },
}

