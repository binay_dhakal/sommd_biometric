import React from 'react';
import { Image, StyleSheet, View, } from 'react-native';
import { StackHeaderProps } from '@react-navigation/stack';

import { IconButton, NavBarTitle } from './components';
import { NAVBAR_TOPARC_HEIGHT } from './constants';

import Back from './assets/back.svg';
import Bell from './assets/bell.svg';



export default function NavBar(props: StackHeaderProps) {
  const { previous, navigation } = props;
 
  return (
    <View style={styles.container}>
      <Image
        source={require('./assets/navbarTop.png')}
        style={styles.topArc}
        resizeMode="stretch"
      />
      <View style={styles.innerContainer}>
        <NavBarTitle onPress={() => navigation.navigate('BottomTabs', { screen: 'Feed' })} />
        {previous && (
          <IconButton
            width={20}
            height={20}
            Icon={Back}
            containerStyle={styles.leftIcon}
            onPress={() => navigation.goBack()}
          />
        )}
        <IconButton
          width={30}
          height={30}
          Icon={Bell}
          color="white"
          containerStyle={styles.rightIcon}
        onPress={() => { /*TODO: handleNotification */ }}
        />
      </View>
    
    </View>
  )
}


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    overflow: 'hidden',
  },
  topArc: {
    position: 'absolute',
    height: NAVBAR_TOPARC_HEIGHT,
    width: '100%',
    opacity: 0.8
  },
  innerContainer: {
    height: NAVBAR_TOPARC_HEIGHT,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  leftIcon: {
    position: 'absolute',
    left: 16,
    top: 35
  },
  rightIcon: {
    position: 'absolute',
    right: 16,
    top: 35,
  },
})
