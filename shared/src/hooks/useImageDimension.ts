import { useEffect, useState } from 'react';
import { Image } from 'react-native';

export default function useImageDimension(uri?: string) {
	const [width, setWidth] = useState(0);
	const [height, setHeight] = useState(0);

	useEffect(() => {
		if (uri) {
			Image.getSize(
        uri,
        (width, height) => {
          setWidth(width)
          setHeight(height)
        }),
        () => {
          console.error('Cannot get image dimensions');
        }
		}
	}, [uri])

	return { width, height };
}
