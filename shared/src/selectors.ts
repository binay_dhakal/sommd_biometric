import { FilterOptions, SortOptions } from "./constants";
import { useStore } from "./context";
import { useStateOrFetch } from "./utils";

export interface NormalizedState {
  allIds: string[],
  byId: { [arg0: string]: {} }
}

export interface NormalizedQuestions {
  allIds: string[],
  byId: { [arg0: string]: Question },
  profile_complete: boolean,
}

export interface NormalizedAddresses {
  allIds: string[],
  byId: { [arg0: string]: Address }
}

export interface NormalizedCart {
  allIds: string[],
  byId: { [arg0: string]: Wine }
}
interface Answer {
  answer: string,
  answer_id: string,
  image_name: string,
  user_selection: boolean
}
export interface Question {
  question: string,
  answers: Answer[],
  answer_display: 'selection' | 'scroll',
  multiselect: boolean,
  selected: number[]
}
export interface Wine {
  wine_id: string,
  winery?: string,
  bottle?: string,
  name: string,
  vintage?: string,
  reaction?: "like" | null,
  reaction_note?: string,
  rating?: number,
  rating_note?: string,
  wine_category?: "Red" | "White",
  unique_name?: string,
  variety?: string,
  label?: string,
  details_url?: string,

  score?: number,
  price?: number,
  inventory?: number,
  available?: boolean,
  cart?: number
}

export interface Profile {
  email: string,
  full_name: string,
  roles: string[],
  wines: Wine[],
  user_id: string,
  invitations_sent: number,
  remaining_invitations: number,
}

export interface Address {
  address_line_1: string,
  address_line_2: string,
  address_line_3: string,
  city: string,
  country: string,
  id: string,
  is_billing: boolean,
  is_shipping: boolean,
  state: string,
  zipcode: string,
}


export interface Filters {
  sortBy: SortOptions | '',
  filterBy: FilterOptions[]
}

export function useQuestions(): {
  questions: NormalizedQuestions,
  error?: string,
  loading?: boolean,
} {
  const { state, error, loading } = useStateOrFetch(
    'get-questions',
    // @ts-ignore
    (state) => state.questions.allIds.length,
    'POPULATE_QUESTIONS'
  );

  // @ts-ignore
  return { questions: state.questions, error, loading };
}


export function useAddresses(): {
  addresses: NormalizedState,
  error?: string,
  loading?: boolean,
} {
  const { state, error, loading } = useStateOrFetch(
    'address/list',
    // @ts-ignore
    (state) => state.addresses.allIds.length,
    'POPULATE_ADDRESSES'
  );

  // @ts-ignore
  return { addresses: state.addresses, error, loading };
}

export function useProfile(): Profile {
	const [state] = useStore();

  return state.profile;
}
