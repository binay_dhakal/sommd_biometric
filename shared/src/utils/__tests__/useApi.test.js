import React from 'react';
import axios from 'axios';
import { render } from '@testing-library/react-native'

import useApi, { BASE_URL } from '../useApi';
import { AppProvider } from '../../context';

function setup({ initialProps } = {}) {
  const result = { current: null };
  function TestComponent(props) {
    result.current = useApi(props)
    return null
  }
  render(
    <AppProvider value={{ user: null }} >
      <TestComponent {...initialProps} />
    </AppProvider>
  );
  return result;
}

jest.mock("axios");

const MOCKS = [
  { api: 'searchEntities',
    testMsg: 'searches data from',
    method: 'GET',
    url: 'search-wines',
    req: { params: { name: 'test' }, },
    res: [{ name: 'test' }],
  },
  { api: 'fetchEntities',
    testMsg: 'fetches data from',
    method: 'GET',
    url: 'get-wines',
    res: [{ id: 1, name: 'test' }],
  },
  { api: 'submitEntity',
    testMsg: 'submits data to',
    method: 'POST',
    url: 'submit-wines',
    req: { data: { name: 'test' }, },
    res: { id: 1, name: 'test' },
  },
  { api: 'patchEntity',
    testMsg: 'patches data to',
    method: 'PATCH',
    url: 'edit-wines',
    req: { data: { id: 1, name: 'test', }, },
    res: { id: 1, },
  },
  { api: 'deleteEntity',
    testMsg: 'deletes data from',
    method: 'DELETE',
    url: 'delete-wines/1',
  },
]

describe('tests useApi hook', () => {
  afterEach(() => {
    jest.clearAllMocks();
  })
  MOCKS.forEach(v => {
    const {api, testMsg, method, url, res, req} = v;
  
    describe(`exposes the api method ${api}`, () => {
      const result = setup();
      test(`${testMsg} an API`, () => {
        const response = { data: res };
        axios.request.mockResolvedValue(response);
    
        expect(result.current[api](url, req)).resolves.toEqual(response);
        expect(axios.request).toHaveBeenCalledWith({
          "headers": {"Content-Type": "application/json"},
          "method": method,
          "responseType": "json",
          "url": BASE_URL + url,
          ...req
        });
      });
    
      test("bad response", () => {
        const errorMessage = 'Network Error';
        axios.request.mockRejectedValue(errorMessage);
        expect(result.current.searchEntities('/')).rejects.toEqual(errorMessage);
      });
    });
  })
})
