import axios from 'axios';
import apiCall, { BASE_URL } from '../apiCall';

jest.mock("axios");

describe('searchEntities', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  })
  test("search successfully data from an API", () => {
    const response = { status: 200, data: [{ id: 1, name: 'test' }]}
    axios.request.mockResolvedValue( response );

    expect(apiCall.searchEntities('/wines', { search: 'test' })).resolves.toEqual(response);
    expect(axios.request).toHaveBeenCalledWith(
      {"headers": {"Content-Type": "application/json"}, "method": "GET", "params": { search: 'test' }, "responseType": "json", "url": BASE_URL + "/wines"}
    );
  });

  test("bad response", () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(() => Promise.reject(new Error(errorMessage)));
    expect(apiCall.searchEntities('/')).rejects.toThrow(errorMessage);
  });
});


describe('fetchEntities', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  })
  test("fetches successfully data from an API", () => {
    const response = { status: 200, data: [{ id: 1, name: 'test' }]}
    axios.request.mockResolvedValue( response );

    expect(apiCall.searchEntities('/wines')).resolves.toEqual(response);
    expect(axios.request).toHaveBeenCalledWith(
      {"headers": {"Content-Type": "application/json"}, "method": "GET", "params": undefined, "responseType": "json", "url": BASE_URL + "/wines"}
    );
  });

  test("bad response", () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(() => Promise.reject(new Error(errorMessage)));
    expect(apiCall.searchEntities('/')).rejects.toThrow(errorMessage);
  });
});


describe('submitEntity', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  })

  test("successfully submits data to an API", () => {
    const response = { status: 200, data: [{ id: 1, name: 'test' }]}
    axios.request.mockResolvedValue( response );

    expect(apiCall.submitEntity({},'/wines')).resolves.toEqual(response);
    expect(axios.request).toHaveBeenCalledWith(
      {"headers": {"Content-Type": "application/json"}, "method": "POST", "responseType": "json", "url": BASE_URL + "/wines", data: {}}
    );
  });

  test("bad response", () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(() => Promise.reject(new Error(errorMessage)));
    expect(apiCall.submitEntity('/')).rejects.toThrow(errorMessage);
  });
});


describe('patchEntity', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  })

  test("successfully patches data to an API", () => {
    const response = { status: 200, data: [{ name: 'test' }]}
    axios.request.mockResolvedValue( response );

    expect(apiCall.patchEntity({},'/wines')).resolves.toEqual(response);
    expect(axios.request).toHaveBeenCalledWith(
      {"headers": {"Content-Type": "application/json"}, "method": "PATCH", "responseType": "json", "url": BASE_URL + "/wines", data: {}}
    );
  });

  test("bad response", () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(() => Promise.reject(new Error(errorMessage)));
    expect(apiCall.patchEntity('/')).rejects.toThrow(errorMessage);
  });
});


describe('deleteEntity', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  })

  test("successfully patches data to an API", () => {
    const response = { status: 200 }
    axios.request.mockResolvedValue( response );

    expect(apiCall.deleteEntity('/wines/1')).resolves.toEqual(response);
    expect(axios.request).toHaveBeenCalledWith(
      {"headers": {"Content-Type": "application/json"}, "method": "DELETE", "responseType": "json", "url": BASE_URL + "/wines/1"}
    );
  });

  test("bad response", () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(() => Promise.reject(new Error(errorMessage)));
    expect(apiCall.patchEntity('/')).rejects.toThrow(errorMessage);
  });
});
