import React from 'react';
import { render, act, waitFor } from '@testing-library/react-native'
import axios from 'axios';

import { useAsyncSubmit } from '../useAsyncSubmit';
import { AppProvider } from '../../context';
import { BASE_URL } from '../useApi';

jest.mock("axios");


function setup(endpoint = '') {
  const result = { current: null };
  function TestComponent({ endpoint }) {
    result.current = useAsyncSubmit(endpoint)
    return null;
  }
  render(
    <AppProvider value={{ user: null }} >
      <TestComponent endpoint={endpoint} />
    </AppProvider>
  );
  return result;
}


describe('test useAsyncSubmit', async () => {
  afterEach(() => {
    jest.resetAllMocks();
  });
  test('toggles loading while submitting', async () => {
    axios.request.mockResolvedValue({ data: { success: true, data: {} }});
    const result = setup('wines');
    expect(result.current.loading).toBe(false);
  
    act(() => result.current.submit());
    expect(result.current.loading).toBe(true);
    await waitFor(() => expect(axios.request).toHaveBeenCalledWith({
      "headers": {"Content-Type": "application/json"},
      "method": 'POST',
      "responseType": "json",
      "url": BASE_URL + 'wines',
      "data": {},
    }));
    expect(result.current.loading).toBe(false);

  });

  test('toggles error on bad response', async () => {
    const errorMessage = 'Network Error';
    axios.request.mockRejectedValue(new Error(errorMessage));
    const result = setup('wines');

    expect(result.current.error).toBe('');

    await act(async () => result.current.submit());
 
    expect(result.current.error).toBe(errorMessage);
  });
});
