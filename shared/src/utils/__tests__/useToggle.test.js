import React from 'react';
import { act, render } from '@testing-library/react-native';

import useToggle from '../useToggle';

function setup(initialValue = false) {
  const result = { current: null };
  function TestComponent({ initialValue }) {
    result.current = useToggle(initialValue)
    return null
  }
  render(
    <TestComponent initialValue={initialValue} />
  );
  return result;
}

test('exposes the visible and toggle/open/close methods', () => {
  const result = setup();
  expect(result.current[0]).toBe(false);

  act(() => result.current[1]());
  expect(result.current[0]).toBe(true)

  act(() => result.current[2]());
  expect(result.current[0]).toBe(true);

  act(() => result.current[3]());
  expect(result.current[0]).toBe(false);
});

test('allows customization of the initial visibility', () => {
  const result = setup(true);
  act(() => result.current[4]());
  expect(result.current[0]).toBe(true);
});

test('allows arguments while toggling the visiblity', () => {
  const result = setup('test');
  act(() => result.current[4]());
  expect(result.current[0]).toBe('test');

  act(() => result.current[1]('deleted'));
  expect(result.current[0]).toBe('deleted');
});

