import { Dimensions, Platform } from "react-native";
import { DEFAULT_REFERRAL, DEFAULT_SCREEN, DEFAULT_TAB } from "../constants";

export function pluralize(val: number, keyword: string) {
  if (val < 2) return `${val} ${keyword}`;
  return `${val} ${keyword}s`;
}

export function uppercase(v: string) { return v.split('_').map(word => word.charAt(0).toUpperCase() + word.substring(1)).join(' ')}

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function getReferral(params: { [arg0: string]: string }) {
  if (!params) return DEFAULT_REFERRAL;
  return params.referral || DEFAULT_REFERRAL;
}

export const getScreenDimension = (): number => Platform.select({
  web: Dimensions.get('window').width,
  android: Dimensions.get('screen').width,
  ios: Dimensions.get('screen').width,
}) || 0;

export const isMobile = Platform.OS === 'android' || Platform.OS === 'ios';
export const handleBack = (navigation: any) => {
  if (navigation.canGoBack()) navigation.goBack();
  else navigation.push(DEFAULT_SCREEN, { screen: DEFAULT_TAB } );  
}
