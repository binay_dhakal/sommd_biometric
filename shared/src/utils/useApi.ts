import Axios, { AxiosRequestConfig, Method } from 'axios';
import { useEffect } from 'react';
import { useUser } from '../context';
// import Config from "react-native-config";

import { loadSession, refreshToken, startSession } from '../session';


// const BASE_URL = Config.WINE_APP_BASE_URL || '/dev/';
export const BASE_URL = 'https://wdl9igkr97.execute-api.us-east-1.amazonaws.com/dev/';

const getHeaders = () => {
  const headers = {
    'Content-Type': 'application/json',
};
return headers;
};

export default function useApi(url: string = BASE_URL): {
  [arg: string]: Function
} {
  const { user } = useUser();

  useEffect(() => {
    Axios.interceptors.request.use(async request => {
      const session = await loadSession();
      if (session) {
        request.headers['Authorization'] = `Bearer ${session.id_token}`;
      }
      return request;
    })

    Axios.interceptors.response.use(undefined, async error => {
      const originalRequest = error.config;
      const session = await loadSession();

      if (error.response.status === 401 && !originalRequest._retry && session) {
        originalRequest._retry = true;
        const access_token = await refreshToken(session.refresh_token);  
        if (access_token) {
          session.id_token = access_token;
          await startSession(session);
          // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
          return Axios(originalRequest);
        }
        user && user.logout();
        return Promise.reject(error);
      }
      return Promise.reject(error);
    })
  }, []);

  const createRequest = (method: Method) => async (
    endpoint: string, commonConfig?: AxiosRequestConfig
  ) => {
    const headers = getHeaders();

    return Axios.request({
      method,
      url: url + endpoint,
      headers,
      responseType: 'json',
      ...commonConfig
    });
  }

  return {
    searchEntities: createRequest('GET'),
    fetchEntities:  createRequest('GET'),
    submitEntity: createRequest('POST'),
    patchEntity: createRequest('PATCH'),
    deleteEntity: createRequest('DELETE'),
  };
}
