import { useEffect, useState } from 'react';
import Axios, { AxiosRequestConfig } from 'axios';
// import Config from "react-native-config";

import { useStore, useUser } from '../context';
import { loadSession, refreshToken, startSession } from '../session';

// const BASE_URL = Config.WINE_APP_BASE_URL || '/dev/';
const BASE_URL = 'https://wdl9igkr97.execute-api.us-east-1.amazonaws.com/dev/';

const getHeaders = () => {
  const headers = {
    'Content-Type': 'application/json',
  };

  return headers;
};
const NO_OF_RETRY = 3;

const createRequest = () => async (
  endpoint: string, config?: AxiosRequestConfig
) => {
  const headers = getHeaders();
  const session = await loadSession();

  return Axios.request({
    url: BASE_URL + endpoint,
    responseType: 'json',
    headers: {
      ...headers,
      ...(session && { Authorization: `Bearer ${session.id_token}` }),
    },
    ...(!config?.method && { method: 'POST' }),
    ...config
  });
}


export function useAsyncSubmit(
  endpoint: string,
  config?: AxiosRequestConfig
): {
  submit: (v?: {}) => Promise<any>,
  error?: string,
  setError: Function,
  loading?: boolean,
} {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const { user } = useUser();
  const api = createRequest();


  useEffect(() => {
    Axios.interceptors.response.use(undefined, async error => {
      const originalRequest = error.config;

      if (error.response.status === 401) {
        if (originalRequest._retry < NO_OF_RETRY || !originalRequest._retry) {
          const session = await loadSession();
          
          if (session) {
            originalRequest._retry = (originalRequest._retry || 0) + 1;
            const access_token = await refreshToken(session.refresh_token);  
  
            if (access_token) {
              await startSession({ ...session, id_token: access_token });


              return Axios({
                ...originalRequest,
                headers: {
                  ...originalRequest.headers,
                  Authorization: `Bearer ${access_token}`,
                }
              });
            }
  
            user && user.logout();
          }
        } else {
          user && user.logout();
        }
      }

      return Promise.reject(error);
    })
  }, []);

  const submit = async (data: any) => {
    try {
      setError('');
      setLoading(true);

      const result = await api(
        endpoint,
        {
          ...config,
          ...(data && { data }),
        }
      );

      setLoading(false);

      if (result.data.error) {
        setError(result.data.message);
      } else if (result.data.success) {
        return result.data.data || {};
      }
    } catch(error) {
      // TODO: Handle different parsing techniques for web and mobile
      // const { message } = error.toJSON();
      setError('Error');
      setLoading(false);
    }
  };

  return { submit, error, setError, loading };
}


export function useStateOrFetch(
  endpoint: string,
  assert: (state: {}) => boolean,
  action: string,
  config?: AxiosRequestConfig,
): {
  state: {},
  error?: string,
  loading?: boolean,
} {
  const { submit, error, loading } = useAsyncSubmit(endpoint, config);
	const [state, dispatch] = useStore();

  useEffect(() => {
		async function fetchData() {
			const data = await submit();

			if (data) {
				dispatch({ type: action, payload: data });
			}
		}
		if (!assert(state)) fetchData();
  }, []);

  return { state, error, loading };
}
