import { request } from 'axios';
export const BASE_URL = 'https://wdl9igkr97.execute-api.us-east-1.amazonaws.com';

const getHeaders = () => {
  const headers = {
    'Content-Type': 'application/json',
  };
  return headers;
};

export default {
  /**
   * Retrieve list of entities from server using AJAX call.
   *
   * @param {String} endpoint - url to fetch
   * @param {Object} parameters - parameters to query
   * @param {String} responseType - response format, defaults to json
   *
   * @returns {Promise} - Result of ajax call.
   */
  searchEntities(endpoint, parameters, responseType = 'json') {
    return request({
      method: 'GET',
      url: BASE_URL + endpoint,
      headers: getHeaders(),
      params: parameters,
      responseType,
    });
  },

  /**
   * Retrieve list of entities from server using AJAX call.
   *
   * @param {String} endpoint - url to fetch
   * @param {String} responseType - response format, defaults to json
   *
   * @returns {Promise} - Result of ajax call.
   */
  fetchEntities(endpoint, responseType = 'json') {
    return this.searchEntities(endpoint, {}, responseType );
  },

  /**
   * Submit entity to server using AJAX call.
   *
   * @param {Object} entity - Request body to post/patch.
   * @param {String} endpoint - url to post/patch
   * @param {String} method - method of http action
   * @returns {Promise} - Result of ajax call.
   */
  submitEntity(entity, endpoint, method = 'POST') {
    return request({
      method,
      url: BASE_URL + endpoint,
      headers: getHeaders(),
      responseType: 'json',
      data: entity,
    });
  },

  /**
   * Patch entity using AJAX call.
   *
   * @param {Object} entity - Request body to patch.
   * @param {String} endpoint - url to post/patch
   * @returns {Promise} - Result of ajax call.
   */

  patchEntity(entity, endpoint) {
    return this.submitEntity(entity, endpoint, 'PATCH');
  },

  /**
   * Delete entity using AJAX call.
   *
   * @param {String} endpoint - url of entity to be deleted
   * @returns {Promise} - Result of ajax call.
   */

  deleteEntity(endpoint) {
    return request({
      method: 'DELETE',
      url: BASE_URL + endpoint,
      headers: getHeaders(),
      responseType: 'json',
    });
  },
};
