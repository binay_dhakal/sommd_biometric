import React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer, } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Store from './Store';
import { AppProvider } from './context';
import { LINKING, } from './AppScreens';
import { makeApp } from './loader';
import User from './User';

import type { Screen } from './AppScreens';
import { Session } from './session';

type CommonProps = {
  screens: Screen[], 
  navigatorOptions: any, 
  screenOptions: any,
}

export function Container({ children, value }: { children: React.ReactNode, value: {} }) {
  return (
    <Store>
      <AppProvider value={value}>
        <NavigationContainer linking={LINKING}>
          {children}
        </NavigationContainer>
      </AppProvider>
    </Store>
  )
}

const MainNavigator = createStackNavigator();
function DetailNavigation({ user, screens,  navigatorOptions, screenOptions }: CommonProps & { user: User | null } ) {
  return makeApp(screens, navigatorOptions, screenOptions, user);
}

export function Authenticated({ ctx, screens, navigatorOptions, screenOptions, }: CommonProps & {
  ctx: {
    user: User | null,
    onLogin?: (session: Session) => void
  }
}) {
  return (
    <Container value={ctx}>
      <MainNavigator.Navigator mode="modal" screenOptions={{ headerShown: false }}>
        <MainNavigator.Screen name="DetailNavigation">
          {(props) => (
            <DetailNavigation
              {...props} 
              user={ctx.user}
              screens={screens}
              navigatorOptions={navigatorOptions}
              screenOptions={screenOptions}
            />
          )}
        </MainNavigator.Screen>
      </MainNavigator.Navigator>
    </Container>
  )
}
