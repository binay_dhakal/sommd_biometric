import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';


import { Text, IconResolver } from './components';
import { colors, LayoutStyles, TextStyles } from './styles';
import { getScreenDimension } from './utils';

import Profile from './assets/profile.svg';
import Pairing from './assets/pairing.svg';
import Search from './assets/search.svg';
const Cellar = require('./assets/barrels.png');
import Cart from './assets/cart.svg';
const CellarSelected = require('./assets/barrelsSelected.png');


// function findMedianNavBarItem(arr: any[]) {
//   if (arr.length % 2 === 0) return -1;
//   return Math.floor(arr.length / 2); 
// }

const icons = {
  Profile: [Profile],
  Pairing: [Pairing],
  Search: [Search],
  Cellar: [Cellar, CellarSelected],
  Cart: [Cart], 
}
const width = getScreenDimension();

export default function BottomNav({ state, descriptors, navigation }: {
  state: any,
  descriptors: any,
  navigation: any,
}) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }
  // const medianIdx = findMedianNavBarItem(Object.keys(icons));

  return (
    <>
      <View style={[LayoutStyles.shadow, styles.shadowContainer]} />
      {/* <View style={[LayoutStyles.shadow, styles.centerContainer]} /> */}
      <View style={styles.container}>
        {state.routes.map((route: { name: string, key: string }, index: number) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };
          const dimension = isFocused ? FOCUSED_ICON_SIZE : ICON_SIZE;
          const color = isFocused ? colors.brand : '#A1A1A1';
          // @ts-ignore
          const currentIcon = icons[route.name];
          if (!currentIcon) return null;
          const Icon = isFocused ? currentIcon[1] ?? currentIcon[0] : currentIcon[0];
          const fillColor = isFocused ? '#C85F57' : '#A1A1A1';

          return (
            <TouchableOpacity
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              // style={[styles.itemContainer, index === medianIdx  && { bottom: 8 }]}
              style={styles.itemContainer}
              key={route.key}
            >
              <IconResolver color={fillColor} Icon={Icon} width={dimension} height={dimension} />
              <Text style={[styles.label, { color }, TextStyles.smaller,]}>
                {label}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
}

const FOCUSED_ICON_SIZE = 30;
const ICON_SIZE = 24;
const BOTTOMNAV_HEIGHT = 75;
const TOTAL_NAV_ITEMS = Object.keys(icons).length;
const NAVITEM_WIDTH = width / TOTAL_NAV_ITEMS;
const LABEL_POSITION_FROM_TOP = 40;
const common = {
  backgroundColor: '#F8F8F8',
  shadowOffset: { width: 0, height: -2 },
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: BOTTOMNAV_HEIGHT,
    backgroundColor: '#F8F8F8',
    elevation: 8,
  },
  label: { 
    position: 'absolute',
    top: LABEL_POSITION_FROM_TOP
  },
  itemContainer: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 8,
    paddingBottom: 16,
  },
  shadowContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: BOTTOMNAV_HEIGHT,
    ...common
  },
  centerContainer: {
    position: 'absolute',
    bottom: 16,
    left: NAVITEM_WIDTH * 2 + ((NAVITEM_WIDTH - BOTTOMNAV_HEIGHT) / 2),
    height: BOTTOMNAV_HEIGHT,
    width: BOTTOMNAV_HEIGHT,
    borderRadius: BOTTOMNAV_HEIGHT / 2,
    alignItems: 'center',
    justifyContent: 'center',
    ...common
  },
})
