import { Wine } from "./selectors"

type Data = { [arg0: string]: Wine }


export const PAIRINGS: Data = {
  1: { wine_id: '1', name: 'Sassicaia', winery: 'Tenuta San Guido', vintage: '2006', score: 94, price: 220.00, inventory: 2, },
  2: { wine_id: '2', name: 'Grand Vin de Chateau Latour', winery: 'Chateau Latour', vintage: '1996', score: 96, price: 390.00, inventory: 11, },
  3: { wine_id: '3', name: 'Insignia', winery: 'Joseph Phelps', vintage: '2001', score: 94, price: 220.00, available: false, },
}
