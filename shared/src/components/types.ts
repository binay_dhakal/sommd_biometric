import { ViewStyle } from "react-native";

export interface InputProps {
  value: any,
  options: { label: string, value: number }[],
  onChange: (v?: any) => void,
  selected?: any,
  style?: ViewStyle,
}
