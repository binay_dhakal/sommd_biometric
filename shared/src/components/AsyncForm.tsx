import React from 'react';
import { View, StyleSheet, TextStyle, ViewStyle } from 'react-native';
import { FormProvider, useForm } from 'react-hook-form';

import { useAsyncSubmit } from '../utils';
import { FormType } from '../forms';
import LineInput from './LineInput';
import Text from './Text';
import Button from './Button';
import { TextStyles } from '../styles';


type Props<T> = {
  onComplete: (arg0: any, arg1: any) => void,
  endpoint: string,
  formData: FormType[],
  buttonText?: string,
  formatData?: (d: T) => void,
  HeadComponent?: React.ReactNode,
  TailComponent?: React.ReactNode,
  ButtomTailComponent?: React.ReactNode,
  defaultValues?: any,
  containerStyles?: ViewStyle,
  inputStyles?: ViewStyle,
  buttonStyles?: ViewStyle,
  errorStyles?: TextStyle,
  buttonType?: 'primary' | 'secondary' | 'transparent',
} 

export default function AsyncForm<T>({
  onComplete,
  endpoint,
  formData,
  buttonText,
  formatData,
  TailComponent,
  HeadComponent,
  ButtomTailComponent,
  defaultValues,
  containerStyles,
  inputStyles,
  buttonStyles,
  errorStyles,
  buttonType,
}: Props<T>) {
  const methods = useForm({ defaultValues });
  const { handleSubmit, formState: { errors }, control, reset, getValues } = methods;
  const { submit, loading, error } = useAsyncSubmit(endpoint);

  const onSubmit = async (data: any) => {
    const formattedData = formatData ? formatData(data) : data;
    const result = await submit(formattedData);
    if (result) {
      onComplete(result, data);
      reset();
    }
  }

  const onPress = handleSubmit(onSubmit);

  return (
    <FormProvider {...methods}>
      {HeadComponent}
      <View style={[styles.container, containerStyles]}>
        {formData.map((f) => {
          let validation = { ...f.validation };
          if (f.customValidation) {
            validation.validate = (data: any) => {
              // @ts-ignore
              return f.customValidation(getValues(), data);
            }
          }

          return (
            <LineInput
              {...f}
              validation={validation}
              key={f.name}
              control={control}
              error={errors[f.name]}
              config={{ ...f.config, onSubmitEditing: onPress }}
              containerStyle={{ ...styles.lineInput, ...inputStyles }}
            />
          )
        })}
        {TailComponent}
        <Text style={[TextStyles.smallPrimaryVariant, styles.error, errorStyles]}>{error || ''}</Text>
        <Button
          title={buttonText || 'Submit'}
          onPress={onPress}
          disabled={loading}
          type={buttonType}
          style={buttonStyles}
        />
        {ButtomTailComponent}
      </View>
    </FormProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  lineInput: {
    paddingVertical: 4,
  },
  error: {
    paddingVertical: 14,
  }
})