import React from 'react';
import { TextStyle, TouchableOpacity, TouchableOpacityProps as RNTouchableOpacityProps } from 'react-native';
import { ButtonStyles, colors, TextStyles } from '../styles';
import Text from './Text';

type TouchableOpacityProps = RNTouchableOpacityProps & {
  type?: 'primary' | 'secondary' | 'transparent',
  title: string,
  textStyle?: TextStyle
}

const textStyles = {
  primary: colors.brandPrimary,
  secondary: colors.primaryVariant,
  transparent: colors.brandPrimary,
}

export default function Button({
  type = 'primary', title, style, textStyle, disabled, ...rest }: TouchableOpacityProps
) {
  const btnStyle = ButtonStyles[type];
  const txtStyle = textStyles[type];

  return (
    <TouchableOpacity
      style={[btnStyle, disabled && ButtonStyles.disabled, style]}
      disabled={disabled}
      {...rest}
    >
      <Text style={[{ color: txtStyle }, disabled && TextStyles.disabled, TextStyles.btn, textStyle]}>
        {title}
      </Text>
    </TouchableOpacity>
  )
}
