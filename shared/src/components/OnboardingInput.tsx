import React from 'react';
import { Image, ScrollView, StyleSheet, TouchableOpacity, View } from 'react-native';

import { URL } from '../constants';
import { colors, TextStyles } from '../styles';
import { getScreenDimension } from '../utils';
import CardBack from '../assets/cardBackground.svg';
import type { Question } from '../selectors';

import Text from './Text';
import { NavBarTitle } from './NavHelpers';
import type { InputProps } from './types';

function mapSelectionToFooter(val: number[], data: { answer: string }[]) {
  const valLen = val.length;
  const dataLen = data.length;

  if (!val) return 'Select an option';
  if (!valLen) return 'None';
  if (valLen === 1) return data[val[0] - 1].answer;
  if (dataLen === valLen) {
    if (dataLen === 2) return 'Both';
    return 'All';
  }
  return val.map((id) => data[id - 1].answer).join(', ')  
}

const screenWidth = getScreenDimension();
// @ts-ignore
const width = screenWidth - 16;


const OnboardingInput = React.forwardRef((
  { data, onChange, value, error }: InputProps & {
    data: Question,
    error?: { type?: string, message?: string },
  },
  _ref: any
) => {
  const { question, answers, answer_display, multiselect } = data;

  const renderSlideComponent = answer_display === 'scroll';

  const imgStyle = renderSlideComponent ?
    // @ts-ignore
    { width: width - 16, height: '100%' } :
    { width: 150, height: 125, }

  const formattedFooter = mapSelectionToFooter(value, answers);
  const body = answers.map(o => {
    const isSelected = value.includes(o.answer_id);
    return (
      <TouchableOpacity
        key={o.answer_id} 
        onPress={() => {
          if (multiselect) {
            if (isSelected) onChange([...value].filter(v => v !== o.answer_id));
            else onChange([...value, o.answer_id]);
          } else {
            onChange([o.answer_id])
          }
        }}
        style={[styles.image, isSelected && styles.selected]}
      >
        <Image source={{ uri: `${URL}/${o.image_name}` }} style={imgStyle} />
      </TouchableOpacity>
    )
  })

  const firstSelectedIndex = renderSlideComponent ? answers.findIndex(a => value.includes(a.answer_id)) : 0
  const contentOffset = firstSelectedIndex * width;

  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <CardBack width="100%" height="100%" preserveAspectRatio="none" style={{ position: 'absolute', top: 0 }} />
        <NavBarTitle containerStyle={styles.navbarTitle} />
        <Text style={[styles.title, TextStyles.headerFour]}>{question}</Text>
        {renderSlideComponent ? (
          <ScrollView
            horizontal
            style={styles.margin}
            snapToInterval={width}
            contentOffset={{ x: contentOffset, y: 0 }}
          >
            {body}
          </ScrollView>
        ): (
          <View style={[styles.imageContainer, styles.margin]}>
            {body}
          </View>
        )}
      </View>
      <View style={styles.footer}>
        <View style={styles.selectionContainer}>
          <Text style={styles.selection}>{formattedFooter}</Text>
        </View>
        <Text style={[TextStyles.smallSecondaryVariant, styles.error]}>{error?.message}</Text>
      </View>
    </View>
  )
})

export default OnboardingInput;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  card: {
    height: '60%',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 8
  },
  navbarTitle: {
    bottom: 16
  },
  title: {
    paddingTop: 24,
    paddingBottom: 8,
    paddingHorizontal: 36,
    color: '#3C3C3C',
    fontFamily: 'SFProText-Light',
    textAlign: 'center'
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    margin: 1,
    borderWidth: 1,
    borderColor: '#0003',
  },
  margin: {
    marginHorizontal: 16,
    marginBottom: 16,
  },
  selected: {
    borderColor: colors.brandPrimary,
  },
  footer: {
    width: '100%',
    paddingVertical: 32,
    paddingHorizontal: 16,
    alignItems: 'center'
  },
  selectionContainer: {
    width: '100%',
    borderWidth: 1,
    padding: 8,
    borderRadius: 8,
    alignItems: "center", 
    borderColor: colors.brandPrimary,
    backgroundColor: 'white'
  },
  selection: {
    color: colors.brandPrimary,
    textTransform: 'capitalize',
    fontFamily: 'SFProText-Medium'
  },
  error: {
    position: 'absolute',
    bottom: 8,
  },
})
