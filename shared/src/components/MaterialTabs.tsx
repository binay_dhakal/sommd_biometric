import React from 'react';
import { StyleSheet, View, TouchableOpacity, } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
// import Animated from 'react-native-reanimated';

import { colors, TextStyles } from '../styles';
import Text from './Text';
import { getScreenDimension } from '../utils';

const ACTIVE_COLOR = colors.brandPrimary;
const INACTIVE_COLOR = '#7B7B7B';

const dimension = getScreenDimension();

// const TabItem = ({ color, label, }: { color: string, label: string, }) => (
//   <Text style={[{ color }, TextStyles.small, styles.tabItem]} numberOfLines={1}>{label}</Text>
// )

const Tab = createMaterialTopTabNavigator();
export default function MaterialTabs({ tabs, containerStyle, tabStyle }: {
  tabs: { name: string, Component: React.ComponentType<any>, config?: any }[],
  containerStyle?: {},
  tabStyle?: {}
}) {
	return (
		<Tab.Navigator
      lazy
      removeClippedSubviews
      // tabBarOptions={{
        // activeTintColor: ACTIVE_COLOR,
        // inactiveTintColor: INACTIVE_COLOR,
        // style: [styles.tabContainer, tabStyle],
        // labelStyle: [TextStyles.small, styles.tabItem],
        // indicatorStyle: styles.tabIndicator,
        // tabStyle: { width: 100 },
        // scrollEnabled: true,
        // tabStyle: { width: 90 }
      // }}
      tabBar={(props: any) => <CustomTabBar {...{ ...props, tabStyle, containerStyle }} />}
      style={containerStyle}
    >
      {tabs.map(tab => {
        const { name, Component, config } = tab;
        return (
          <Tab.Screen key={name} name={name}>
            {props => <Component {...config} {...props} />}
          </Tab.Screen>
        )
      })}
    </Tab.Navigator>
	)
}


function CustomTabBar({
  state,
  descriptors,
  navigation,
  // position
}: any) {
  return (
    <View style={styles.container} >
      {state.routes.map((route: { key: string, name: string }, index: number) => {
        const { options } = descriptors[route.key];
        const label =
          // eslint-disable-next-line no-nested-ternary
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
            // if (scrollEnabled) scrollRef.current.scrollTo({ x: index * 50, animated: true })
          }
        };


        // const opacity = Animated.interpolateNode(position, {
        //   inputRange,
        //   outputRange: inputRange.map(i => (i === index ? 1 : 0.8)),
        // });

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            key={route.key}
            // style={{ width: dimension / state.routes.length }}
          >
            {/* <Animated.Text */}
            <Text
              numberOfLines={1}
              style={[
                TextStyles.small,
                styles.label, 
                {
                  // opacity,
                  color: isFocused ? ACTIVE_COLOR : INACTIVE_COLOR,
                  width: dimension / state.routes.length,
                },
                // index === 0 && styles.first,
                // index === state.routes.length - 1 && styles.last,
              ]}
            >
              {label}
            {/* </Animated.Text> */}
            </Text>
            {isFocused && <View style={styles.indicator} />}
          </TouchableOpacity>
        );
      })}
    </View>
  );
}


const styles = StyleSheet.create({
  // tabContainer: {
  //   backgroundColor: '#F2F2F2',
  //   borderBottomWidth: 1,
  //   borderColor: '#CFCFCF',
  // },
  // tabItem: {
  //   flex: 1,
  //   fontFamily: 'SFProText-Regular',
  //   textTransform: 'none',
  //   paddingHorizontal: 8,
  // },
  // tabIndicator: {
  //   height: 3,
  //   backgroundColor: INACTIVE_COLOR,
  // },

  container: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    backgroundColor: '#F2F2F2',
    borderBottomWidth: 1,
    borderColor: '#CFCFCF',
  },
  indicator: {
    height: 3,
    backgroundColor: INACTIVE_COLOR,
  },
  label: {
    // backgroundColor: 'red',
    // flex: 1,
    fontFamily: 'SFProText-Regular',
    paddingHorizontal: 16,
    paddingVertical: 16,
    textAlign: 'center',
  },
  // first: {
  //   paddingLeft: 16,
  // },
  // last: {
  //   paddingRight: 16,
  // }
})
