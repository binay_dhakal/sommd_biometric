import React from 'react';
import {
  StyleSheet,
  TextInput as RNTextInput,
  TextInputProps } from 'react-native';
import { colors, TextStyles } from '../styles';

type Props = TextInputProps & {
  onChange: (v: string|number) => void,
  error?: boolean,
  onChangeEnd?: (value: string) => void,
}

const TextInput = React.forwardRef(({ onChange, style, error, onChangeEnd, ...rest}: Props, ref: any) => {
  const handleChangeText = (text: string) => {
    onChange(text);
    if (onChangeEnd) onChangeEnd(text)
  }

  return (
    <RNTextInput
      ref={ref}
      onChangeText={handleChangeText}
      style={[styles.textinput, error && styles.error, style]}
      placeholderTextColor="#979797"
      autoCapitalize="none"
      autoCorrect={false}
      {...rest}
    />
  )
});

export default TextInput;

const styles = StyleSheet.create({
  textinput: {
    ...TextStyles.regular,
    width: '100%',
    backgroundColor: '#F2F2F2',
    padding: 12,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#979797',
    color: colors.brandSecondary,
  },
  error: {
    borderColor: colors.brandPrimary,
  }
});
