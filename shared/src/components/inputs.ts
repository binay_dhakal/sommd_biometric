import { ElementType } from 'react';
import TextInput from './TextInput';
import OnboardingInput from './OnboardingInput';


const inputTypes: { [arg0: string]: ElementType } = {
  'textinput': TextInput,
  'onboardinginput': OnboardingInput
};

export default inputTypes;
