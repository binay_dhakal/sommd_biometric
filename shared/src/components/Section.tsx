import React from 'react';
import { View, StyleSheet, ViewStyle,  } from 'react-native';

import Text from './Text';
import { colors, TextStyles } from '../styles';


export default function Section({ header, children, editText = 'Edit', onEdit, style, containerStyle }: {
  children?: React.ReactNode,
  header?: string,
  containerStyle?: ViewStyle,
  style?: ViewStyle,
  onEdit?: () => void,
  editText?: string
}) {
  return (
    <>
      <View style={[styles.sectionContainer, containerStyle]}>
        <Text style={styles.header}>{header}</Text>
        {onEdit && (
          <Text onPress={onEdit} style={[TextStyles.small, { color: colors.brandPrimary }]}>
            {editText}
          </Text>
        )}
      </View>
      <View style={style}>
        {children}
      </View>
    </>
  )
}


const styles = StyleSheet.create({
  sectionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 8 ,
    borderTopWidth: 1,
    borderTopColor: colors.brandPrimary,
  },
  header: {
    color: colors.brandPrimary
  },
})
