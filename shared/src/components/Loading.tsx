import React from 'react';
import { Modal, StyleSheet } from 'react-native';

import { LayoutStyles, TextStyles } from '../styles';
import Text from './Text';
import GradientWrapper from './GradientWrapper';

import WineIcon from '../assets/whiteWineIcon.svg';

export default function Loading() {
  return (
    <Modal visible transparent>
      <GradientWrapper containerStyle={LayoutStyles.center}>
        <WineIcon width={80} height={140} />
        <Text style={[TextStyles.primaryVariant, styles.text]}>Curating your collection...</Text>
      </GradientWrapper>
    </Modal>
  )
}

const styles = StyleSheet.create({
  text: {
    paddingVertical: 32,
    fontFamily: 'SFProText-Light',
  },
});
