import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import User from './User';
import { Session } from './session';
import { getReferral, isMobile } from './utils';
import type { Screen } from './AppScreens';


function createRoute(user: User | null, RouteComp: React.FC<any>) {
  return (Comp: React.FC<any>, options?: {}) => {
    return <RouteComp {...options} user={user} Comp={Comp} />
  }
}
type Props = {
  user: User | null,
  Comp: React.FC<any>,
  navigation: any,
  route: any
}

function ProtectedScreen({ user, Comp, ...other }: Props) {
  const { navigation } = other; 
  useEffect(() => {
    if (!user) navigation.replace('Login', { referral: Comp.name })
  }, []);

  if (user) return <Comp {...other} />
  return null;
}

function RestrictedScreen({ user, Comp, ...other }: Props) {
  const { navigation, route } = other; 
  useEffect(() => {
    if (user) navigation.replace(getReferral(route.params));
  }, []);

  if (user) return null;
  return <Comp {...other} />;
}


function makeRoute(user: User | null, route?: string) {
  if (isMobile) return null;

  const restrictedRoute = createRoute(user, RestrictedScreen);
  const protectedRoute = createRoute(user, ProtectedScreen);

  return route === 'restricted' ? restrictedRoute : route === 'protected' ? protectedRoute : null;
}

const Stack = createStackNavigator();


export function makeApp(screens: Screen[], navigatorOptions: any, screenOptions: any, user: User | null) {
  return (
    <Stack.Navigator {...navigatorOptions}>
      {screens.map(screen => {
        const { name, Comp, props, route } = screen;

        const wrapper = makeRoute(user, route);
        const options = screenOptions[name];

        return (
          <Stack.Screen key={name} name={name} options={options}>
            {(config) => wrapper ? wrapper(Comp, { ...props, ...config }) : <Comp {...config} {...props} />}
          </Stack.Screen>
        )
      })}
    </Stack.Navigator>
  )
}


export function useAppInit() {
  const [user, setUser] = useState<User | null>();

  useEffect(() => {
    User.load().then(setUser);
  }, []);

  const onLogin = async (session: Session) => {
    await User.create(session).then(setUser);
  }

  return { user, setUser, onLogin };
}


