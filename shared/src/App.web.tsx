import React from 'react';

import { Authenticated } from './Container';
import { useAppInit, } from './loader';
import { AUTH_SCREENS, SCREENS, } from './AppScreens';
import { Landing } from './screens';
import NavBar from './NavBar';


const STACK_OPTIONS = {
  detachInactiveScreens: true,
  screenOptions: {
    header: NavBar,
    headerShown: false,
  },
  headerMode: "screen"
}

const SCREENS_OPTIONS = {
  'BottomTabs': { headerShown: true },
  'PairingDetail': { headerShown: true, headerTitle: 'Pairing' },
}

const screens = [
  ...AUTH_SCREENS,
  ...SCREENS,
  { name: 'Landing', Comp: Landing, },
]

export default function App() {
  const { user, setUser, onLogin } = useAppInit();

  if (user === undefined) return null;
  if (user) user.onLogout = () => setUser(null);
  return (
    <Authenticated
      ctx={{ user, onLogin }}
      screens={screens}
      screenOptions={SCREENS_OPTIONS}
      navigatorOptions={STACK_OPTIONS}
    />
  )
}
