export const URL = 'http://app-dev.sommd.com';
export const DEFAULT_TAB = 'Feed';
export const DEFAULT_SCREEN = 'BottomTabs';
export const ONBOARDING_INIT_DATA_KEY = 'onboarding_init_data';

export const SORT_OPTIONS: { [arg0: string]: string } = {
  recent: 'Recently added',
  score: 'Sommd Score',
  name: 'Name',
  bottles: 'Number of bottles',
}

export const FILTER_OPTIONS: { [arg0: string]: string } = {
  like: 'Loved',
  appellation: 'Appellation',
  vintage: 'Vintage',
  grape: 'Grape',
  country: 'Country',
}

export type SortOptions = 'recent' | 'score' | 'Grape' | 'bottles';
export type FilterOptions = 'like' | 'appellation' | 'vintage' | 'grape' | 'country';



export const NAVBAR_HEIGHT = 150;
export const NAVBAR_TOPARC_HEIGHT = 90;
export const NAVBAR_EXTENSION_HEIGHT = 50;
export const NAVBAR_BOTTOMARC_HEIGHT = NAVBAR_EXTENSION_HEIGHT;
export const NAVBAR_TITLE_HEIGHT = NAVBAR_HEIGHT - NAVBAR_TOPARC_HEIGHT;

