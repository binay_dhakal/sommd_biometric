import React from 'react';
import { render } from '@testing-library/react-native';
import { renderHook } from '@testing-library/react-hooks';

import { useUser, AppProvider } from '../AppContext';


test('useUser shows default value', () => {
  const { result } = renderHook(useUser);
  
  expect(result.current).toBe(null);
})

test('useUser shows value from provider', () => {
  const result = { current: null };
  function HookComponent() {
    result.current = useUser();
    return null;
  }
  render(
    <AppProvider value={{ user: 'test' }} >
      <HookComponent />
    </AppProvider>
  );

  expect(result.current.user).toBe('test');
})
