import React, { useContext } from 'react';

const StoreContext = React.createContext<{} | null>(null);

export const useStore = () => {
  //@ts-ignore
  const { state, dispatch } = useContext(StoreContext);
  return [state, dispatch];
}

export const StoreProvider = StoreContext.Provider;
