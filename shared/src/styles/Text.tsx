import { StyleSheet } from "react-native";
import colors from "./colors";

const primary = {
  color: colors.primary,
}

const secondary = {
  color: colors.secondary,
}

const primaryVariant = {
  color: colors.primaryVariant,
}

const secondaryVariant = {
  color: colors.secondaryVariant,
}

const small = {
  fontSize: 14,
}

export const TextStyles = StyleSheet.create({
  regular: {
    ...primary,
    fontFamily: 'SFProText-Regular',
    fontSize: 18,
  },
  btn: {
    fontFamily: 'SFProText-Medium',
    fontSize: 16,
  },
  small,
  smaller: {
    fontSize: 10,
  },
  large: {
    fontSize: 22,
  },
  headerOne: { fontSize: 60, },
  headerTwo: { fontSize: 48, },
  headerThree: { fontSize: 30, },
  headerFour: { fontSize: 24, },
  primary,
  primaryVariant,
  secondary,
  secondaryVariant,
  disabled: {
    opacity: 0.5,
    color: colors.primaryVariant,
  },
  error: {
    ...small,
    color: colors.brandPrimary,
  },
  smallPrimaryVariant: {
    ...small,
    ...primaryVariant,
  },
  smallSecondary: {
    ...small,
    ...secondary,
  },
  smallSecondaryVariant: {
    ...small,
    ...secondaryVariant,
  }
});
