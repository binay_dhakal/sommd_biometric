const colors = {
  brand: '#C85F57',
  brandVariant: '#CC3333',
  brandPrimary: '#C42124',
  brandSecondary: '#911A1F',
  primary: '#000000',
  primaryVariant: '#ffffff',
  secondary: '#777777',
  secondaryVariant: '#333333',
  disabled: '#0005',
}
export default colors;
