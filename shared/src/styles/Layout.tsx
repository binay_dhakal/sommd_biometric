import { StyleSheet } from 'react-native';

const shadow = {
  shadowColor: '#000000',
  shadowOffset: { width: 0, height: 2 },
  shadowOpacity: 0.2,
  shadowRadius: 2,  
  elevation: 8
}

export const LayoutStyles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  shadow,
  card: {
    ...shadow,
    backgroundColor: 'white',
    borderRadius: 8,
  }
})

export const IconStyles = {
  regular: {
    width: 20,
    height: 20,
  },
  large: {
    width: 32,
    height: 32,
  }
}
