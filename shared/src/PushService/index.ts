import {useEffect} from 'react';
// @ts-ignore
import PushNotification from 'react-native-push-notification';

const DEFAULT_CHANNEL = 'fcm_fallback_notification_channel';
const pushListenerers: Function[] = [];
const pushEvents: Function[] = [];
let notiToken = {};

function registerPushListener(listener: Function) {
  pushEvents.length = 0;
  pushListenerers.push(listener);
  return () => {
    const idx = pushListenerers.indexOf(listener);
    pushListenerers.splice(idx, 1);
  };
}

PushNotification.configure({
  onRegister: function (token: string) {
    notiToken = token;
  },
  onNotification: (event: Function) => {
    if (pushListenerers.length === 0) {
      pushEvents.push(event);
    } else {
      pushListenerers.forEach((listener: Function) => listener(event));
    }
  },
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: false,
});

export function usePushNotification(listener: any) {
  useEffect(() => {
    PushNotification.createChannel(
      {
        channelId: DEFAULT_CHANNEL,
        channelName: 'Default channel',
        channelDescription: 'A default channel',
        soundName: 'default',
        importance: 4,
        vibrate: true,
      },
      (created: string[]) =>
        console.log(`createChannel ${DEFAULT_CHANNEL} returned '${created}'`),
    );

    pushEvents.forEach(listener);
    return registerPushListener(listener);
  }, [listener]);
}

export function getNotiToken(): {
  os?: 'android' | 'ios';
  token?: string;
} {
  return notiToken;
}
