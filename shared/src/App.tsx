import React from 'react';


import { Authenticated, Container } from './Container';
import { DEFAULT_SCREEN } from './constants';
import { makeApp, useAppInit, } from './loader';
import { AUTH_SCREENS, SCREENS, } from './AppScreens';
import NavBar from './NavBar';

const config = {
  animation: 'spring',
  config: {
    stiffness: 100,
    mass: 8,
    overshootClamping: true,
    restDisplacementThreshold: 0.01,
    restSpeedThreshold: 0.01,
  },
};

const AUTH_STACK_OPTIONS = {
  initialRouteName: "Login",
  screenOptions: {
    headerShown: false,
    transitionSpec: {
      open: config,
      close: config,
    }
  }
}

const STACK_OPTIONS = {
  initialRouteName: DEFAULT_SCREEN,
  screenOptions: {
    header: NavBar,
  },
  headerMode: "screen"
}

const SCREENS_OPTIONS = {
  'Onboarding': { headerShown: false },
  'EditPreferences': { headerShown: false },
  'ProductDetail': { headerShown: false },
  'PairingDetail': { headerTitle: 'Pairing' },
  'Camera': { headerShown: false },
  'AddressForm': { headerShown: false },
  'Invitations': { headerShown: false },
  'Addresses': { headerShown: false },
  'FlavourProfile' : { headerShown: false },
  'Orders' : { headerShown: false },
}

function App() {
  const { user, setUser, onLogin } = useAppInit();
  if (user === undefined) return null;
  if (user === null) return (
    <Container value={{ onLogin }}>
      {makeApp(AUTH_SCREENS, AUTH_STACK_OPTIONS, {}, user)}
    </Container>
  )
  user.onLogout = () => setUser(null);

  return (
    <Authenticated
      ctx={{ user }}
      screens={SCREENS}
      screenOptions={SCREENS_OPTIONS}
      navigatorOptions={STACK_OPTIONS}
    />
  )
}

export default App;
