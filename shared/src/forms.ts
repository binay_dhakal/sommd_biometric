import { TextInputProps } from "react-native";

import { uppercase, EMAIL_REGEX } from "./utils";

type InputType =
  'textinput' |
  'sliderinput' |
  'onboardinginput';


interface CommonTypes {
  name: string,
  label?: string,
  customValidation?: (data: any, value: any) => boolean,
}
export interface FormType extends CommonTypes {
  validation: any,
  input: InputType,
  config?: TextInputProps & {
    options?: { label: string, value: string | number }[],
    selected?: number,
    onChangeEnd?: (value: string) => void,
  },
}

const createField = ({ name, label, validation = {}, config = {}, input, customValidation, optional }: CommonTypes & {
  validation? : {},
  config? : {},
  input?: InputType,
  optional?: boolean,
}): FormType => {
  const formattedLabel = label || uppercase(name);
  if (!optional) validation = { ...validation, required: 'Required field.' };
  // @ts-ignore
  if (!input && !config.placeholder) config.placeholder = `Enter ${formattedLabel}`;
  

  return {
    name,
    label: formattedLabel,
    validation,
    config,
    input: input || 'textinput',
    customValidation,
  }
}


const name = createField({ name: 'name' });
export const userName = createField({ name: 'username', validation: { minLength: 3 }, config: { autoCompleteType: "email" } });
const email = createField({ name: 'email', validation: { pattern: EMAIL_REGEX, } })
const passwordConfig = { secureTextEntry: true, autoCompleteType: "password" }

export const password = createField({
  name: 'password',
  validation: { minLength: 8 },
  config: passwordConfig
});
const verifyPassword = createField({
  name: 'verify_password',
  customValidation: (data, val) => {
    return data.password === val;
  },
  config: { ...passwordConfig, placeholder: 'Enter Password again to verify' }
});
const verificationCode = createField({ label: 'Verification code', name: 'code' });

export const loginForm = [ userName, password, ];
export const signUpForm = [
  name,
  email,
  createField({
    label: 'Invitation code',
    name: 'invitation_code',
    validation: { minLength: 6, maxLength: 6, },
    config: { placeholder: 'Enter Access Code', },
  }),
  password,
  verifyPassword
];

export const changePassword = [
  userName,
  { ...password, config: { ...password.config, placeholder: 'Enter new password' }},
  verifyPassword,
  verificationCode
]

export const verificationForm = [
  userName,
  verificationCode,
]

export const resendVerificationForm = [ userName ];
export const invitationsForm = [ email ];

export const addressForm = [
  createField({ name: 'address_line_1', }),
  createField({ name: 'address_line_2', optional: true }),
  createField({ name: 'address_line_3', optional: true }),
  createField({ name: 'city', }),
  createField({ name: 'state', }),
  createField({ name: 'zipcode', }),
  createField({ name: 'country', optional: true }),
]

// THESE KEYS ARE MAPPED ON THE BASIS OF KEYS DEFINITION ABOVE in the addressForm.
// ANY CHANGES HERE TO THE KEY VALUES HERE SHOULD BE REFLECTED IN THE KEYS in the addressForm or vice versa.
export const PLACEAPI_KEYS_MAPPER = {
  street_number: 'address_line_1',
  route: 'address_line_1',
  locality: 'city',
  administrative_area_level_1: 'state',
  postal_code: 'zipcode',
  postal_code_suffix: 'zipcode',
  country: 'country',
}

// The first key in addressForm that is not in PLACEAPI_KEYS_MAPPER
export const PLACEAPI_KEY_FOCUS = 'address_line_2'
