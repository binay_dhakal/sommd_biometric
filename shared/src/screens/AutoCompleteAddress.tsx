import React from 'react';
import { StyleSheet, } from 'react-native';
import { GooglePlacesAutocomplete, AddressComponent } from 'react-native-google-places-autocomplete';
import { useFormContext } from 'react-hook-form';

import { TextStyles } from '../styles';
import { TextInput } from '../components';
import { PLACEAPI_KEYS_MAPPER, PLACEAPI_KEY_FOCUS, } from '../forms';
import Config from '../config';



function processDetail(addressComponents: AddressComponent[]): { [arg0: string]: string } {
  const res: { [arg0: string]: string } = {};
  for(const d of addressComponents){
    const componentType = d.types[0];

    switch(componentType) {
      case "street_number": {
        const key = PLACEAPI_KEYS_MAPPER['street_number'];
        res[key] = `${d.long_name}${res[key] || ''}`;
        break;
      }

      case "route": {
        const key = PLACEAPI_KEYS_MAPPER['route'];
        res[key] =  `${res[key] || ''}${d.short_name ? ` ${d.short_name}` : ''}`;
        break;
      }

      case "postal_code": {
        const key = PLACEAPI_KEYS_MAPPER['postal_code'];
        res[key] = `${d.long_name}${res[key] || ''}`;
        break;
      }

      case "postal_code_suffix": {
        const key = PLACEAPI_KEYS_MAPPER['postal_code_suffix'];
        res[key] = `${res[key] || ''}${d.long_name ? `-${d.long_name}` : ''}`;
        break;
      }

      case "locality": {
        const key = PLACEAPI_KEYS_MAPPER['locality'];
        res[key] = d.long_name;
        break;
      }

      case "administrative_area_level_1": {
        const key = PLACEAPI_KEYS_MAPPER['administrative_area_level_1'];
        res[key] = d.short_name;
        break;
      }

      case "country": {
        const key = PLACEAPI_KEYS_MAPPER['country'];
        res[key] = d.long_name;
        break;
      }
    }
  }
  return res;
}


export default function AutoCompleteAddress() {
  const { setValue, setFocus } = useFormContext();

  return (
    <GooglePlacesAutocomplete
      placeholder='Search address'
      onPress={(_data, details) => {
        // 'details' is provided when fetchDetails = true
        if(details) {

          const processedDetails = processDetail(details.address_components);
          let fieldToFocus = '';
          for (const name of Object.values(PLACEAPI_KEYS_MAPPER)) {
            const value = processedDetails[name] || '';
            if (!value && !fieldToFocus) fieldToFocus = name;
            setValue(name, value)
          }

          setFocus(fieldToFocus || PLACEAPI_KEY_FOCUS);
        }
      }}
      query={{
        key: Config.GOOGLE_PLACES_APIKEY,
        language: 'en',
      }}
      requestUrl={{
        useOnPlatform: 'web',
        url: 'https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api',
      }}
      fetchDetails
      debounce={500}
      isRowScrollable={false}
      minLength={3}
      enablePoweredByContainer={false}
      textInputProps={{
        InputComp: TextInput,
        errorStyle: TextStyles.error
      }}
      styles={{
        container: StyleSheet.absoluteFillObject,
        textInput: [
          TextStyles.regular,
          {
            backgroundColor: '#F2F2F2',
            paddingVertical: 12,
            borderRadius: 8,
            height: undefined,
          }
        ],
      }}
    />
  )
}
