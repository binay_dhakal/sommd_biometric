import React from 'react';
import { FlatList, Image, StyleSheet, TouchableOpacity, View, } from 'react-native';

import { Text, MaterialTabs } from '../components';

const images: {
  [arg0: string]: number
} = {
  'Bold Red': require('../assets/pairingTileRedWine.png'),
  'Rich White': require('../assets/pairingTileWhiteWine.png'),
  'Medium Red': require('../assets/pairingTileRedWine.png'),
  'Sweet White': require('../assets/pairingTileWhiteWine.png'),
  'Light Red': require('../assets/pairingTileLightRedWine.png'),
  'Dry White': require('../assets/pairingTileWhiteWine.png'),
  'Dessert': require('../assets/pairingTileDessert.png'),
  'Sparkling': require('../assets/pairingTileBlueWine.png'),
  'Red Meat': require('../assets/pairingTileRedMeat.png'),
  'Vegetables': require('../assets/pairingTileVegetables.png'),
  'White Meat': require('../assets/pairingTileWhiteMeat.png'),
  'Starches': require('../assets/pairingTileStarches.png'),
  'Cured Meat': require('../assets/pairingTileCuredMeat.png'),
  'Soft Cheese': require('../assets/pairingTileSoftCheese.png'),
  'Hard Cheese': require('../assets/pairingTileHardCheese.png'),
  'Rich Fish': require('../assets/pairingTileFish.png'),
  'Sweets': require('../assets/pairingTileSweets.png'),
}

const WINE = ['Bold Red', 'Rich White', 'Medium Red', 'Sweet White', 'Light Red', 'Dry White', 'Dessert', 'Sparkling'];
const CUISINE = ['Red Meat', 'Vegetables', 'White Meat', 'Starches', 'Cured Meat', 'Soft Cheese', 'Dessert', 'Hard Cheese', 'Rich Fish', 'Sweets'];


export default function Pairing({ navigation }: { navigation: any }) {
  return (
    <MaterialTabs
      tabs={[
        { name: 'by wine', Component: List, config: { data: WINE, navigation }},
        { name: 'by cuisine', Component: List, config: { data: CUISINE, navigation }},
      ]}
    />
  )
}

function List({ data, navigation }: { data: string[], navigation: any }) {
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.flatList}
      data={data}
      renderItem={props => <ListItem {...{...props, navigation}} />}
      numColumns={2}
      keyExtractor={item => item}
      ItemSeparatorComponent={() => <View style={styles.listSeperator} />}
    />
  )
}

function ListItem({ item, index, navigation }: { item: string, index: number, navigation: any }) {
  const spacing = index % 2 === 0 ? { marginRight: 4, } : { marginLeft: 4, };
  const navigate = () => {
    navigation.navigate('PairingDetail', { id: item })
  }

  return (
    <TouchableOpacity onPress={navigate} style={[styles.listContainer, spacing, ]}>
      <Image source={images[item]} style={styles.listImage} resizeMode="contain" />
      <Text style={styles.listItem}>{item}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  flatList: {
    paddingTop: 64,
    paddingVertical: 32,
  },
  listSeperator: { margin: 32 },
  listContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listImage: { height: 100, width: '100%' },
  listItem: {
    color: '#7B7B7B',
    fontSize: 16,
    marginTop: 16,
  }
})
