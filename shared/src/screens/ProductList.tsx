import React, { useState } from 'react';
import { FlatList } from 'react-native';

import { useStore } from '../context';
import { Wine } from '../selectors';
import { colors } from '../styles';

import CartOrCellarModal from './CartOrCellarModal';
import ConfirmationModal from './ConfirmationModal';
import ProductItem from './ProductItem';


import ThumbsUp from '../assets/thumbsUp.svg';
import ThumbsDown from '../assets/thumbsDown.svg';
const Cellar = require('../assets/barrels.png');


type Props = {
  data: Wine[]
}

export default function ProductList({ data = [] }: Props) {
  const [_, dispatch] = useStore();

  const [modal, setModal] = useState('');
  const [wine, setWine] = useState<Wine>();

  const items = wine && ((modal === 'Cart' && wine.cart || 0) || (modal === 'Inventory' && wine.inventory))
  
  const closeModal = () => {
    setModal('')
  }

  const handleConfirm = async (cartCellar: number) => {
    if (modal) {
      dispatch({
        type: 'UPDATE_WINE',
        payload: {
          id: wine?.wine_id,
          [modal.toLowerCase()]: cartCellar
        }
      })
      setModal('');
    }

  }

  const handleCartCellar = (modal: string) => (wine: Wine) => {
    setWine(wine);
    setModal(modal);
  }


  const orderAction = [
    {
      key: 'order',
      label: 'Order more',
      backgroundColor: colors.brandPrimary,
      color: '#F2F2F2',
      Icon: Cellar
    }
  ];

  
  const reactionActions = [
    {
      key: 'thumbsUp',
      label: 'Thumbs Up',
      backgroundColor: '#F2F2F2',
      color: colors.brandPrimary,
      Icon: ThumbsUp,
      onPress: () => setModal('Thumbs Up')
    },
    {
      key: 'thumbsDown',
      label: 'Thumbs Down',
      backgroundColor: colors.brandPrimary,
      color: '#F2F2F2',
      Icon: ThumbsDown,
      onPress: () => setModal('Thumbs Down')
    }
  ];


  const renderCartCellarModal = modal === 'Cart' || modal === 'Inventory';
  const renderConfirmModal = modal === 'Thumbs Up' || modal === 'Thumbs Down';

	return (
    <>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        renderItem={({ item }) => (
          <ProductItem
            {...item}
            actions={{ left: orderAction, right: reactionActions }}
            handleCart={handleCartCellar('Cart')}
            handleCellar={handleCartCellar('Inventory')}
          />
        )}
        keyExtractor={(item: Wine) => item.wine_id}
      />
      {renderConfirmModal && (
        <ConfirmationModal title={modal} onClose={closeModal} />
      )}
      {renderCartCellarModal && (
        <CartOrCellarModal
          title={modal}
          items={items || 0}
          onConfirm={handleConfirm}
          onClose={closeModal}
        />
      )}
    </>
	)
}
