import React, { useState } from 'react';
import { StyleSheet, View, } from 'react-native';

import { Text, Button, Modal } from '../components';

export default function CartOrCellarModal({ title, items, onClose, onConfirm }: {
  title: string,
  items?: number,
  onClose: () => void,
  onConfirm: (arg0: number) => void,
}) {
  const [curr, setCurr] = useState<number>(items || 0);

  const adder = (add: number) => setCurr(prev => prev + add);
  const subtractor = (subtract: number) => setCurr(prev => prev - subtract);

  return (
    <Modal visible={!!title} title={`Add to ${title}`} onClose={onClose}>
      <Text>{`Total items in ${title} : ${curr}`}</Text>
      <View style={styles.container}>
        <Row value={curr} count={1} add={() => adder(1)} sub={() => subtractor(1)} />
        <Row value={curr} count={6} add={() => adder(6)} sub={() => subtractor(6)} />
        <Row value={curr} count={12} add={() => adder(12)} sub={() => subtractor(12)} />
      </View>
      <Button title="Confirm" onPress={() => onConfirm(curr)} disabled={curr < 0} />
    </Modal>
  )
}

function Row({ count, value, add, sub }: { count: number, add: () => void, sub: () => void, value: number }) {
  return (
    <View style={styles.rowContainer}>
      <Button title={`Add ${count}`} onPress={add} />
      <Button title={`Remove ${count}`} onPress={sub} disabled={value < count} />
    </View>
  )
}


const styles = StyleSheet.create({
	container: {
    marginVertical: 24,
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 8
  }
})

