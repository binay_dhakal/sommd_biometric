import React, { useEffect, useState } from 'react';
import { Animated, ScrollView, StyleSheet, TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native';
import { timing, useAnimation } from '@lib/anim';

import { Text } from '../components';
import { colors, LayoutStyles, TextStyles, } from '../styles';
import DropDown from '../assets/dropDown.svg';
import { useStore } from '../context';
import { SORT_OPTIONS, FILTER_OPTIONS } from '../constants';



const dropdownStyler = (driver: any) => ({
	transform: [
    {
			rotate: driver.interpolate({
				inputRange: [0, 1],
				outputRange: ['0deg', '180deg'],
			})
    },
  ],
});

const filterStyler = (driver: any) => ({
	transform: [
    {
			scale: driver.interpolate({
				inputRange: [0, 0.5, 1],
				outputRange: [0.5, 0.9, 1],
			})
    },
  ],
});

export default function Filters({ results }: { results: number }) {
	const [dropdownStyles, driver] = useAnimation(0, dropdownStyler);
	const [filterStyles, filterDriver] = useAnimation(0, filterStyler);
	const [visible, setVisible] = useState(false);
  const [state, dispatch] = useStore();
	
	useEffect(() => {
		if (visible) {
			timing(driver, 1, { duration: 100, }).start();
			timing(filterDriver, 1, { duration: 100, }).start();
		}
		else {
			timing(driver, 0, { duration: 100, }).start();
			timing(filterDriver, 0, { duration: 100, }).start();
		}
	},[visible])

	const handleSort = (payload: any) => {
		dispatch({ type: 'SORT_BY', payload })
	}
	const handleFilter = (payload: any) => {
		dispatch({ type: 'FILTER_BY', payload })
	}
	const handleReset = () => {
		dispatch({ type: 'RESET_FILTERS' })
	}

	const selectedSort = (s: string) => s === state.filters.sortBy;
	const selectedFilter = (s: string) => state.filters.filterBy.includes(s);


	const filtersSelected = !!state.filters.sortBy || !!state.filters.filterBy.length;

	return (
		<>
			<View style={[styles.rowContainer, styles.topContainer]}>
				{/* <Text style={styles.text}>{`${profile.wines.length} Results`}</Text> */}
				<Text style={styles.text}>{`${results} Results`}</Text>
				<TouchableOpacity style={styles.row} onPress={() => setVisible(prev => !prev)}>
					{filtersSelected && (
						<TouchableOpacity onPress={handleReset}>
							<Text style={styles.text}>Reset</Text>
						</TouchableOpacity>
					)}
					<Text style={[styles.text, styles.filterText]}>{'Filters & Sort'}</Text>
					<Animated.View style={dropdownStyles}>
						<DropDown width={10} height={10} />
					</Animated.View>
				</TouchableOpacity>
			</View>
			{visible && (
				<Animated.View style={[styles.bottomContainer, filterStyles]}>
					<Row
						title="Sort By"
						data={SORT_OPTIONS}
						getSelectedValue={selectedSort}
						onPress={handleSort}
					/>
					<Row
						title="Filters"
						data={FILTER_OPTIONS}
						getSelectedValue={selectedFilter}
						onPress={handleFilter}
					/>
				</Animated.View>
			)}
		</>
	)
}

function Row({ title, data, getSelectedValue, onPress }: {
	title: string,
	data: { [arg0: string]: string },
	onPress: (s: string) => void,
	getSelectedValue: (s: string) => boolean
}) {
	

	return (
		<View style={styles.row}>
			<Text style={styles.text}>{title}</Text>
			<ScrollView horizontal style={styles.scrollView} showsHorizontalScrollIndicator={false}>
				{Object.keys(data).map(key => {
					const label = data[key];
					const isSelected = getSelectedValue(key)
					const handlePress = () => onPress(key)
					const borderColor = isSelected ? colors.brandPrimary : '#4C4C4C' 

					return (
						<TouchableOpacity key={key} style={[styles.filterContainer, { borderColor }]} onPress={handlePress}>
							<Text style={TextStyles.smallSecondaryVariant}>{label}</Text>
						</TouchableOpacity>
					)
				})}
			</ScrollView>
		</View>
	)
}


const row: ViewStyle = {
	flexDirection :'row',
	alignItems: 'center',
}
const text: TextStyle = {
	fontSize: 12,
	color: '#4C4C4C',
	fontFamily: 'SFProText-Light',
}

const styles = StyleSheet.create({
	row,
	text,
	rowContainer: {
		...row,
		...LayoutStyles.shadow,
		marginVertical: 2,
    backgroundColor: '#F8F8F8',
	},
	topContainer: {
		padding: 12,
		justifyContent: 'space-between',
	},
	filterText: {
		marginHorizontal: 8,
	},
	bottomContainer: {
		paddingLeft: 12,
	},
	scrollView: {
		flex: 1,
		marginLeft: 12,
	},

	filterContainer: {
		borderWidth: 1,
		borderRadius: 8,
		borderColor: '#4C4C4C',
		margin: 4,
		padding: 8,
		minWidth: 75,
		alignItems: 'center',
	}
})
