import React, { useEffect } from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  Animated,
  Image,
} from 'react-native';

import { useAnimation, jump } from '@lib/anim';

import { Button, Text, } from '../../components';
import { ButtonStyles, LayoutStyles, TextStyles } from '../../styles';
import { DEFAULT_SCREEN } from '../../constants';

const image = {
  background: require('../../assets/background.jpg'),
  down: require('../../assets/down.png'),
  logo: require('../../assets/logo.png'),
}

const styler = (driver: Animated.Value) => ({
  transform: [
    {
      translateY: driver.interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, 16, 0]
      }),
    },
  ],
})

export default function Background({ navigation }: { navigation : any }) {
  // @ts-ignore
  const [animStyles, driver] = useAnimation(0, styler);

  useEffect(() => {
    jump(driver).start();
  }, [driver]);

  const handlePress = () => {
    navigation.navigate(DEFAULT_SCREEN);
  }

  return (
    <ImageBackground
      style={styles.image}
      source={image.background}
    >
      <View style={[LayoutStyles.center, styles.absolute]}>
        <Image source={image.logo} style={styles.logo} />
        <Text style={[TextStyles.primaryVariant, TextStyles.headerTwo, styles.tagline]}>
          Wine Curated Not Bought!!!
        </Text>
        <Button
          style={ButtonStyles.transparent}
          textStyle={TextStyles.primaryVariant}
          title="Get Started"
          onPress={handlePress}
        />
      </View>
      <Animated.Image source={image.down} style={[styles.indicator, styles.absolute, animStyles]} />
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  image: {
    height: '100%',
    resizeMode: 'cover',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  absolute: {
    position: 'absolute'
  },
  tagline: {
    paddingVertical: 24,
  },
  logo: {
    width: 200,
    height: 200,
  },
  indicator: {
    width: 16,
    height: 16,
    margin: 36,
    tintColor: 'white',
    bottom: 0,
  },
});
