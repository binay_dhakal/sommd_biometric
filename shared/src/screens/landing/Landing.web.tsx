import React from 'react';
import { View, StyleSheet, } from 'react-native';

import Background from './Background';
import { Text } from '../../components';
import { TextStyles } from '../../styles';


const BODY = [
  {
    title: 'Learning Platform',
    body: [
      'Help users identify wine that fits their preferences',
      'Personalized recommendations based on user\'s feedback',
      'Suggest wine and food pairing',
    ],
    background: '#E9ECEF'
  },
  {
    title: 'Concierge Service',
    body: ['Provides concierge / curation advice to assist users in acquiring wines they desire'],
    background: '#DEE2E6'
  },
  {
    title: 'Cellar management',
    body: [
      'Keep track of your inventory',
      'Find the best drinking window for your wine',
    ],
    background: '#CED4DA'
  }
]


export default function Landing(props: any) {
  return (
    <>
      <Background {...props} />
      {BODY.map(b => <Info key={b.title} {...b} />)}
    </>
  )
}


function Info({ title, body, background }: {
  title: string, body: string[], background: string
}) {
  return (
    <View style={[styles.infoContainer, { backgroundColor: background }]}>
      <Text style={[TextStyles.headerTwo, styles.header]} accessibilityRole="header">{title}</Text>
      {body.map((b, i) => <Text style={TextStyles.headerFour} key={i + 1}>{b}</Text>)}
    </View>
  )
}

const styles = StyleSheet.create({
  infoContainer: {
    justifyContent: 'center',
    height: '100%',
    textAlign: 'center',
  },
  header: {
    marginVertical: 32,
  },
});
