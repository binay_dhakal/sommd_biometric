import React from 'react';
import { KeyboardAvoidingView, ScrollView, StyleSheet, Platform } from 'react-native';

import { AsyncForm, Text, GradientWrapper } from '../../components';
import { FormType } from '../../forms';
import { Session } from '../../session';
import { colors, TextStyles } from '../../styles';
import { useStore } from '../../context';

import WineIcon from '../../assets/whiteWineIcon.svg';

type Props<T> = {
  title?: string | React.ReactNode,
  formData: FormType[],
  onComplete: (data: Session) => Promise<void> | void,
  endpoint: string,
  buttonText?: string,
  TailComponent?: React.ReactNode,
  ButtomTailComponent?: React.ReactNode,
  formatData?: (d: T) => void,
}

export default function AuthScreen<T>({ title, ...rest }: Props<T>) {
  const keyboardBehaviour: "padding" | undefined =
    Platform.select({ android: undefined, ios: 'padding', web: undefined });

  const [store] = useStore();
  
  return (
    <GradientWrapper>
      <KeyboardAvoidingView style={styles.container} behavior={keyboardBehaviour}>
        <ScrollView contentContainerStyle={styles.scrollView} keyboardShouldPersistTaps="handled">
          <WineIcon width={80} height={140} />
          <Text style={[TextStyles.headerOne, styles.headerOne]}>
            somm
            <Text style={[TextStyles.headerOne, styles.headerTwo]}>'d</Text>
          </Text>
          <AsyncForm
            {...rest}
            defaultValues={store.auth}
            containerStyles={styles.authContainer}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </GradientWrapper>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16
  },
  headerOne: {
    color: colors.primaryVariant,
    fontFamily: 'SFProText-Light',
  },
  headerTwo: {
    color: colors.brandVariant,
    fontFamily: 'SFProText-Medium',
  },
  authContainer: {
    paddingTop: 32,
  }
})
