import React, { useEffect, useRef, useState } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { FormProvider, useForm, useFormContext } from 'react-hook-form';
import Swiper from 'react-native-web-swiper';
import { read, write } from '@lib/storage';

import { Button, LineInput, Text, Loading, IconButton, GradientWrapper } from '../../components';
import { useAsyncSubmit, useDeepNestedEffect, } from '../../utils';
import { NormalizedQuestions, useQuestions } from '../../selectors';
import { useStore } from '../../context';
import { colors, LayoutStyles, TextStyles } from '../../styles';
import { ONBOARDING_INIT_DATA_KEY } from '../../constants';

import Back from '../../assets/back.svg';

type NavProps = { navigation: any, route: any };
type Props = NavProps & {
  onComplete?: (arg0: NavProps) => void,
  onBack?: ({ navigation }: { navigation: any }) => void
  onSkip?: boolean,
  defaultValues?: {},
};

export default function OnboardingWrapper(props: Props) {
  const { questions, error, loading } = useQuestions();
  const [defaultValues, setDefaultValues] = useState<undefined | {}>();

  useEffect(() => {
    async function setup() {
      if (props.onSkip) {
        setDefaultValues({});
      } else {
        const data = (await read(ONBOARDING_INIT_DATA_KEY)) || {};
        setDefaultValues(data);
      }
    }

    setup();
  }, [questions.allIds]);


  if (loading || !defaultValues) return <Loading />;
  if (error) return <Text>Error</Text>;
  return <Onboarding {...questions} {...props} defaultValues={defaultValues} />
}

function DotComponent({ isActive, onPress }: {
  isActive: boolean,
  onPress: () => void
}) {
  const activeStyle = isActive ? {
    backgroundColor: '#fff',
  }: {
    backgroundColor: '#fff5',
  };
  return <TouchableOpacity onPress={onPress} style={[styles.dotsContainer, activeStyle]} />
}

function Onboarding({
  allIds,
  onComplete,
  onBack,
  onSkip,
  navigation,
  route,
  defaultValues,
}: NormalizedQuestions & Props) {
  const methods = useForm({ defaultValues });
  const { submit, error, loading } = useAsyncSubmit('add-flavor-profile')
  const [, dispatch] = useStore();
  const swiperRef = useRef(null);
  const [isLastIndex, setIsLastIndex] = useState(false);

  const onSubmit = async (d: { [arg0: string]: string }) => {
    const formatted = Object.keys(d).map((e) => ({
      question_id: e,
      answer_ids: d[e]
    }));
    await submit({ answers: formatted, });
    dispatch({ type: 'UPDATE_QUESTIONS', payload: formatted })
    onComplete && onComplete({ navigation, route });
  }
  const isFirst = useRef(true);
  const formData: { [arg0: string]: any } = methods.watch();

  useEffect(() => {
    if (!onSkip) {
      const firstEmptyIndex = Object.keys(formData).findIndex(f => !formData[f].length);
  
      // @ts-ignore
      swiperRef?.current?.goTo(firstEmptyIndex);
    }
  }, []);

  useDeepNestedEffect(() => {
    async function setup() {

      if (!isFirst.current && Object.keys(formData).length) {
        await write(ONBOARDING_INIT_DATA_KEY, formData);
      }

      if (Object.keys(formData).length) isFirst.current = false;
    }

    if (!onSkip) setup();
  }, [formData])

  const handleIndexChanged = (index: number) => {
    setIsLastIndex(index === allIds.length);
  }
  const handleBack = () => {
    if (onBack) onBack({navigation})
  }

  if (!allIds.length) return null;
  return (
    <GradientWrapper>
      {loading && <Loading />}
      <FormProvider {...methods}>
        <Swiper
          ref={swiperRef}
          controlsProps={{
            prevTitle: 'Prev',
            nextTitle: 'Next',
            dotsTouchable: true,
            dotsWrapperStyle: { marginBottom: 32 },
            prevTitleStyle: { marginBottom: 32, color: 'white', fontSize: 16, },
            nextTitleStyle: { marginBottom: 32, color: 'white', fontSize: 16, },
            // @ts-ignore
            DotComponent
          }}
          minDistanceToCapture={50}
          onIndexChanged={handleIndexChanged}
        >
          {allIds.map((k) => <OnboardingItem key={k} id={k} />)}
          <Last {...{ onSubmit, error, loading, swiperRef, ...(onBack && { onCancel: handleBack }) }} />
        </Swiper>
        {onBack && <IconButton Icon={Back} containerStyle={styles.backIcon} onPress={handleBack} />}
        {onSkip && !isLastIndex && (
          // @ts-ignore  
          <TouchableOpacity style={styles.skipContainer} onPress={() => swiperRef.current.goTo(allIds.length) }>
            <Text style={styles.skip}>Skip</Text>
          </TouchableOpacity>
        )}
      </FormProvider>
    </GradientWrapper>
  )
}

function OnboardingItem({ id }: { id: string }) {
  const { control, formState: { errors } } = useFormContext();
  const [state] = useStore();
  const v = state.questions.byId[id];
  const error = errors[id];
  const config = {
    selected: v.selected,
    data: v,
  }

  return (
    <LineInput
      input="onboardinginput"
      control={control}
      name={id}
      defaultValue={v.selected}
      validation={{ required: 'Required field.' }}
      config={config}
      error={error}
      containerStyle={styles.container}
    />
  )
}


type LastProps = {
  onSubmit: (d: { [arg0: string]: string; }) => Promise<void>,
  error?: string,
  loading?: boolean,
  swiperRef: any,
  onCancel?: () => void
}

function findFirstErrorIndex(dict: {}): number {
  return Number(Object.keys(dict)[0]);
}

function Last({ onSubmit, error, loading, swiperRef, onCancel }: LastProps) {
  const { handleSubmit, formState: { errors }} = useFormContext();

  useEffect(() => {
    const index = findFirstErrorIndex(errors)
    if (index) {
      swiperRef.current.goTo(index - 1);
    }
  }, [errors]);

  return (
    <View style={LayoutStyles.center}>
      <Text>{error && 'Error while submitting'}</Text>
      <Button style={styles.button} disabled={loading} title="Submit" onPress={handleSubmit(onSubmit)} />
      {onCancel && (
        <>
          <Text style={[TextStyles.smallPrimaryVariant, styles.separator]}>or</Text>
          <TouchableOpacity onPress={onCancel}>
            <Text style={TextStyles.primaryVariant}>Cancel</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    paddingHorizontal: 32,
    paddingVertical: 16,
  },
  backIcon: {
    position: 'absolute',
    top: 40,
    left: 8
  },
  separator: {
    paddingVertical: 8
  },
  
  skip: {
    color: colors.primaryVariant,
  },
  skipContainer: {
    position: 'absolute',
    top: 40,
    right: 8
  },
  dotsContainer: {
    marginHorizontal: 5,
    height: 10,
    width: 10,
    borderRadius: 5,
  }
})
