import React from 'react';

import { Text } from '../../components';
import { signUpForm } from '../../forms';
import { TextStyles } from '../../styles';
import AuthScreen from './AuthScreen';

export default function Signup({ navigation }: {
    navigation: any,
    route: any
  }) {
  const handleSignup = () => {
    navigation.push('Verification')
  }
  const appendUserName = (d: { email: string, verify_password?: string }) => {
    delete d.verify_password;
    return { ...d, username: d.email };
  }
  return (
    <AuthScreen
      onComplete={handleSignup}
      formData={signUpForm}
      title="Sign up"
      formatData={appendUserName}
      endpoint="signup"
      buttonText="Create Account"
      TailComponent={(
        <Text
          style={TextStyles.smallPrimaryVariant}
          onPress={() => navigation.navigate('Login')}
        >
          Back to Login
        </Text>
      )}
    />
  )
}
