import React from 'react';

import { verificationForm, resendVerificationForm } from '../../forms';
import { Text } from '../../components';
import AuthScreen from './AuthScreen';
import { TextStyles } from '../../styles';
import { useToggle } from '../../utils';

export default function Verification({ navigation }: {
    navigation: any,
    route: any
  }) {
  const [visible, toggle] = useToggle();

  const body = visible ? (
    <AuthScreen
      formData={resendVerificationForm}
      endpoint="resend-confirmation"
      onComplete={() => { toggle(); }}
      TailComponent={(
        <Text style={TextStyles.smallPrimaryVariant} onPress={() => toggle()}>Back to Verification</Text>
      )}
    />
  ): (
    <AuthScreen
      formData={verificationForm}
      endpoint="confirm-signup"
      onComplete={() => navigation.push('Login')}
      TailComponent={(
        <Text style={TextStyles.smallPrimaryVariant} onPress={() => toggle()}>Send verification code again.</Text>
      )}
    />
  )
  return body;
}
