import React from 'react';

import { resendVerificationForm, changePassword } from '../../forms';
import { Text } from '../../components';
import AuthScreen from './AuthScreen';
import { TextStyles } from '../../styles';
import { useToggle } from '../../utils';

type Props = {
  navigation: any,
  route: any
}

export default function ForgotPassword({ navigation }: Props) {
  const [visible, toggle] = useToggle();

  const formatData = (d: { email: string, verify_password?: string }) => {
    delete d.verify_password;
    return d;
  }

  const body = visible ? (
    <AuthScreen
      formData={changePassword}
      formatData={formatData}
      endpoint="confirm-forgot-password"
      onComplete={() => navigation.push('Login')}
      TailComponent={(
        <Text style={TextStyles.smallPrimaryVariant} onPress={() => toggle()}>Send code again</Text>
      )}
    />
  ): (
      <AuthScreen
        onComplete={() => { toggle(); }}
        endpoint="forgot-password"
        formData={resendVerificationForm}
        title="Forgot Password"
        TailComponent={(
          <Text style={TextStyles.smallPrimaryVariant} onPress={() => navigation.navigate('Login')}>
            Back to Login
          </Text>
        )}
      />
    )
  return body;
}
