import React, { useEffect, useState } from 'react';
import { View, Platform, StyleSheet, TouchableOpacity, Alert } from 'react-native';

import { useStore, useUser } from '../../context';
import { Text } from '../../components';
import { FormType, userName, password } from '../../forms';
import { Session } from '../../session';
import { getReferral } from '../../utils';
import { getNotiToken } from '../../PushService';
import { colors, TextStyles } from '../../styles';
import AuthScreen from './AuthScreen';
import ReactNativeBiometrics from 'react-native-biometrics'
import * as Keychain from 'react-native-keychain';

export default function Login({ navigation, route }: {
    navigation: any,
    route: any
  }) {

  const [ bioAvailable, setBioAvailable ] = useState<Boolean>(false);
  const { onLogin } = useUser();
  const [_store, dispatch] = useStore();


  useEffect(() => {
    checkBiometricAvailability();
  }, []);

  const checkBiometricAvailability = async() => {
   const biotype =  await Keychain.getSupportedBiometryType();
   const  credentials = await Keychain.getGenericPassword();
   console.log(credentials)
   console.log(biotype)

   if(biotype && credentials){
     setBioAvailable(true);
   }else {
     setBioAvailable(false);
   }
  }

  const handleBiometric = async () => {
   
  // let username = Math.round((new Date()).getTime() / 1000).toString();



  await ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
  .then(async (resultObject) => {
    const { success } = resultObject
 
    if (success) {
      try {
        // Retrieve the credentials
        const  credentials = await Keychain.getGenericPassword();
        if (credentials) {
        // check the biometric token saved in the mobile and present in the server
          if(credentials.username === "hardwork"){
            Alert.alert("Login success give the token to the specific user");
            // now here insted of alert give the user the token to access the dashboard
          }else{
            Alert.alert("Wrong Biometric, Reset the Biometric")
          }

    
        } else {
          // console.log('No credentials stored');
          Alert.alert("Biometric is not enabled")
        }
      } catch (error) {
        Alert.alert("Keychain couldn't be accessed!", error);
      }
    } else {
      console.log('user cancelled biometric prompt')
    }
  })
  .catch(() => {
    console.log('biometrics failed')
  })




    
  }

  const handleLogin = async (data: Session) => {
    // @ts-ignore
    await onLogin(data);
    const path = getReferral(route.params);
    path && navigation.push(path);
  }
  const appendPushToken = (d: { device_type?: string, device_id?: string }) => {
    if (Platform.OS === 'web') return d;
    const pushToken = getNotiToken();
    if (pushToken) {
      d.device_type = pushToken.os;
      d.device_id = pushToken.token;
    }
    return d;
  }


  const loginUserName: FormType = {
    ...userName,
    config: { 
      ...userName.config, 
      onChangeEnd: (value: string) => {
        dispatch({
          type: 'UPDATE_AUTH_INFO',
          payload: {
            username: value,
            name: value,
          }
        });
      }
    }
  }

  const formData: FormType[] = [ loginUserName, password ];

  return (
    <AuthScreen
      onComplete={handleLogin}
      formData={formData}
      title={() => (
        <>
          <Text style={[TextStyles.primaryVariant, TextStyles.headerThree]}>Login to</Text>
          <Text style={[TextStyles.primaryVariant, TextStyles.headerThree]}>Your Account</Text>
        </>
      )}
      formatData={appendPushToken}
      buttonText="Log in"
      endpoint="login"
      TailComponent={(
        <View style={styles.loginSupplementContainer}>
          <Text style={TextStyles.smallPrimaryVariant} onPress={() => navigation.navigate('Signup')}>Register</Text>
          <Text style={TextStyles.smallPrimaryVariant} onPress={() => navigation.navigate('ForgotPassword')}>Forgot password?</Text>
        </View>
      )}
      ButtomTailComponent={(
       
        <View style={styles.buttontailContainer}>
          <TouchableOpacity onPress={handleBiometric}>

          <Text style={TextStyles.smallPrimaryVariant} >{bioAvailable ? "Use Biometric Login" : null }</Text>
          </TouchableOpacity>
        </View>
      )}
    />
  )
}

const styles = StyleSheet.create({
  loginSupplementContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    color: colors.primaryVariant
  },
  buttontailContainer: {
    alignItems: "center",
    marginTop: 40
  }
})
