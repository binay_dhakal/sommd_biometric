import React, { useEffect, useState } from 'react';

import { Loading, MaterialTabs, Text } from '../components';
import { Wine } from '../selectors';
import { useAsyncSubmit } from '../utils';
import { useStore } from '../context';
import { FilterOptions } from '../constants';

import Filters from './Filters';
import ProductList from './ProductList';
import Cart from './Cart';


type WrapperProps = {
	navigation: any,
	Comp: React.ComponentType<any>,
}

export function Wrapper({ Comp, navigation }: WrapperProps) {
	const [end, setEnd] = useState(false);
	const { submit: submitQuestions, error: errorQuestions } = useAsyncSubmit('get-questions');
	const { submit, error } = useAsyncSubmit('get-user-profile', { method: 'GET' });
	const [state, dispatch] = useStore();


	useEffect(() => {
		const fetchProfile = async () => {
			const res = await submit();

			if (res) {
				dispatch({ type: 'INIT_PROFILE', payload: res });
				setEnd(true);
			}
		}

		const checkProfile = () => {
			if (!state.profile) fetchProfile();
			else setEnd(true);
		}

		const fetchQuestions = async () => {
			const res = await submitQuestions();

			if (res) {
				dispatch({ type: 'POPULATE_QUESTIONS', payload: res });
				if (!res.profile_complete) navigation.replace('Onboarding')
				else checkProfile();
			}
		}

		if (!state.questions.allIds.length) fetchQuestions();
		else checkProfile();
	}, []);

	const { profile } = state;

	if (errorQuestions) return <Text>Error while fetching your preferences.</Text>;
  if (error) return <Text>Error while fetching your profile</Text>
	if (!end) return <Loading />;
	if (!profile) return null;
	return <Comp />;
}


export default function Cellar({ navigation }: { navigation: any }) {
	return <Wrapper Comp={Main} navigation={navigation} />
}

function Main() {
	return (
		<MaterialTabs
			tabs={[
				{ name: 'Recommendations', Component: List },
				{ name: 'Inventory', Component: Inventory },
			]}
	 	/>
	)
}

function List() {
	const [state] = useStore();
	const { sortBy, filterBy } = state.filters;
	const filteredData = state.profile.wines.filter(
		(w: Wine) => (
			filterBy.reduce((res: boolean, r: FilterOptions) => {
				if (r === 'like') res = w.reaction === 'like';
				return res;
			}, true)
		)
	).sort((a: Wine, b: Wine) => {
		if (sortBy === 'name') {
			return a?.name.localeCompare(b?.name);
		}
		return 0;
	})

	return (
		<>
			<Filters results={filteredData.length} />
			<ProductList data={filteredData} />
		</>
	)
}

function Inventory() {
	return <Cart stateKey="inventory" />
}
