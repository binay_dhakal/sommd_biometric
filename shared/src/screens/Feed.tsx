import React from 'react';
import { View, Image, StyleSheet, ScrollView, } from 'react-native';

import { IconButton, Section, Text } from '../components';
import { LayoutStyles, TextStyles } from '../styles';

import Flag from '../assets/flag.svg';
import Favorite from '../assets/heartOutline.svg';
import Comment from '../assets/comment.svg';
import Share from '../assets/share.svg';
import RedWineIcon from '../assets/redWineIcon.svg';
import YellowWineIcon from '../assets/yellowWineIcon.svg';


type FeedItemProps = { 
  id: number,
  user: {
    name: string,
    image: number,
  },
  image: number,
  address: string,
  wine: {
    name: string,
    type: 'red' | 'white',
  },
  comments: {
    total: number,
    top: { name: string, comment: string }[]
  }
}

const DUMMY_FEED_POSTS: FeedItemProps[] = [
  {
    id: 1,
    user: {
      name: 'Christina Villamonte',
      image: require('../assets/feedPostUser1.png'),
    },
    image: require('../assets/feedPost1.png'),
    address: 'Midtown Manhattan',
    wine: {
      name: '1997 HillSide Select',
      type: 'red',
    },
    comments: {
      total: 24,
      top: [
        { name: 'J.Trajillio', comment: 'Happy Bday CC! wish I was there' }
      ]
    }
  },
  {
    id: 2,
    user: {
      name: 'Carl Williams',
      image: require('../assets/feedPostUser2.png'),
    },
    address: 'Macari Vineyards',
    image: require('../assets/feedPost2.png'),
    wine: {
       name: '2016 Dos Aguas',
       type: 'red',
    },
    comments: {
      total: 43,
      top: []
    }
  },
  {
    id: 3,
    user: {
      name: 'Isabella Halliday',
      image: require('../assets/feedPostUser3.png'),
    },
    image: require('../assets/feedPost3.png'),
    address: 'Domaine Paul Pillot',
    wine: {
      name: '2016 CruChassagne-Montrachet',
      type: 'white',
    },
    comments: {
      total: 24,
      top: []
    }
  }
]

export default function Feed() {
  const seePopular = () => {
    console.log('see popular wines');
  }

  const seeCircle = () => {
    console.log('see somm\'d circles');
  }

  const seeVines = () => {
    console.log('see grape vines posts');
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.topContainer}>
        <View style={styles.feturedWineContainer}>
          <Image source={require('../assets/detail_bottle.png')} style={styles.image} />
          <View style={styles.featuredWineDetail}>
            <Text>Red Blend</Text>
            <Text style={[TextStyles.btn, TextStyles.headerFour]}>2006 Sassicaia</Text>
            <Text style={styles.featuredWineText}>Tenuta San Guido</Text>
            <Flag style={{ position: 'absolute', bottom: 0, right: 0 }} width={24} height={24} />
          </View>
        </View>
        <Section containerStyle={styles.popularWinesContainer} header="Popular in somm'd" editText="See All" onEdit={seePopular}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <Card label="Choice Selection" image={require('../assets/feedPopularPlaceholder1.png')} />
            <Card label="Curated Cases" image={require('../assets/feedPopularPlaceholder2.png')} />
            <Card label="How members fill their Cellar" image={require('../assets/feedPopularPlaceholder3.png')} />
          </ScrollView>
        </Section>
      </View>
      <Section containerStyle={styles.sommdCircleContainer} header="somm'd Circle" editText="See All" onEdit={seeCircle}>
        {DUMMY_FEED_POSTS.map(post => <FeedItem key={post.id} {...post} />)}
      </Section>
      <Section containerStyle={styles.grapeVine} header="The GrapeVine" editText="See All" onEdit={seeVines}>
        <GrapeVineContent
          image={require('../assets/feedFooter1.png')}
          title="Wine-enjoying mini-boom in the Covid-19 era"
          data={[
            'Wine drinkers across key consumption markets have been turning more often to wine as a beverage during the Covid-19 pandemic, spurred by new ‘lockdown’ occasions and more drinking outside of mealtimes, according to a new report.',
            'The growth in wine consumption occasions comes as online sales of wine have exploded in markets where regulations permitted this channel, with younger and more involved wine drinkers leading the surge.',
            'Consumer responses about their broader economic behaviour also indicate a general tightening of belts will occur during the coming months, with household savings prioritised ahead of any large purchases, and the motivation to spend on luxuries, which initially surged in the early weeks of lockdown, has now subsided.',
          ]}
        />
        <GrapeVineContent
          image={require('../assets/feedFooter2.png')}
          title="What Wineries Face in 2021"
          data={[
            'One of the silver linings of the pandemic was that it provided the opportunity, and the motivation, for wine producers and intermediaries to reach out and connect more strongly with their consumer bases. Many players had already been running e-commerce transaction platforms, but many were not well funded or loved.',
            'In 2020, the website and app sales channels became the heroes. Will it be the same in 2021? Perhaps not. The pickings won’t be as easy, as retailers and on-premise become unshackled from pandemic restrictions. However, the Covid era has provided strong momentum to businesses that have invested in their direct relationships with consumers – and 2021 offers an opportunity to consolidate this bridgehead. Meanwhile, in-person cellar door visitors will be limited to day-tripper locals for some time to come, and it will be late next year before we see the re-emergence of the classic out-of-town tourist trade.',
          ]}
        />
      </Section>
    </ScrollView>
  )
}

const Card = ({ image, label }: { image: number, label: string  }) => {
  return (
    <View style={[LayoutStyles.card, styles.card]}>
      <Image source={image} style={styles.cardImage} />
      <Text style={[TextStyles.smallSecondaryVariant, styles.cardLabel]}>{label}</Text>
    </View>
  )
}

const FeedItem = ({ user, address, wine, image, comments }: FeedItemProps) => {
  const WineIcon = wine.type == 'red' ? RedWineIcon: YellowWineIcon;

  return (
    <View>
      <View style={styles.feedHeader}>
        <Image source={user.image} style={styles.feedUser} />
        <Text style={TextStyles.small}>{user.name}</Text>
        <Text style={[TextStyles.smaller, styles.feedAddress]}>{address}</Text>
      </View>
      <Image source={image} style={styles.feedImage} />
      <View style={styles.feedFooter}>
        <View style={styles.feedCta}>
          <IconButton color="black" Icon={Favorite} containerStyle={styles.feedCtaSpacing} onPress={console.log} />
          <IconButton Icon={Comment} containerStyle={styles.feedCtaSpacing} onPress={console.log} />
          <IconButton color="black" Icon={Share} onPress={console.log} />
        </View>
        <View style={styles.feedInfo}>
          <Text style={[TextStyles.small, styles.feedInfoText]}>{wine.name}</Text>
          <WineIcon height={25} width={15} />
        </View>
      </View>
      <View style={styles.commentContainer}>
        {comments.top.map((comment, idx) => (
          <View key={`${idx + 1}`} style={styles.comment}>
            <Text style={[TextStyles.smaller, styles.commentName]}>{comment.name}</Text>
            <Text style={[TextStyles.smaller, styles.commentText]}>{comment.comment}</Text>
          </View>
        ))}
        <Text style={[TextStyles.smaller, styles.moreCommentText]}>{`View all ${comments.total} comments`}</Text>
      </View>
    </View>
  )
}

const GrapeVineContent = ({ image, title, data }: {
  image: number,
  title: string,
  data: string[],
}) => {
  return (
    <View style={styles.feedFooterContainer}>
      <Image source={image} style={styles.feedFooterImage} />
      <View style={styles.feedFooterTextContainer}>
        <Text style={styles.feedFooterTitle}>{title}</Text>
        {data.map((d, idx) => <Text key={`${idx + 1}`} style={[TextStyles.small, styles.commentText]}>{d}</Text>)}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  topContainer: {
    marginHorizontal: 16
  },
  feturedWineContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  featuredWineDetail: {
    flex: 1,
    marginLeft: 16,
    justifyContent: 'flex-end',
  },
  featuredWineText: {
    fontFamily: 'SFProText-Light',
    fontSize: 20,
  },
  image: {
    width: 40,
    height: 150,
  },

  popularWinesContainer: {
    marginVertical: 16
  },
  card: {
    alignItems: 'center',
    marginRight: 16,
    marginBottom: 16,
  },
  cardImage: {
    height: 100,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  cardLabel: {
    paddingVertical: 8,
    paddingHorizontal: 16,
  },

  sommdCircleContainer: {
    margin: 16
  },
  feedHeader: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  feedUser: {
    width: 36,
    height: 36,
    marginRight: 8,
  },
  feedAddress: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'SFProText-Light',
  },
  feedImage: {
    height: 200,
    width: '100%',
    marginVertical: 8,
  },
  feedFooter: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    paddingVertical: 8,
    alignItems: 'center',
  },
  feedCta: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  feedCtaSpacing : {
    marginRight: 8
  },
  feedInfo: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  feedInfoText: {
    marginRight: 8
  },
  commentContainer: {
    paddingHorizontal: 16,
  },
  comment: {
    flexDirection: 'row',
  },
  commentName: {
    fontFamily: 'SFProText-Bold',
    marginRight: 8,
  },
  commentText: {
    fontFamily: 'SFProText-Light'
  },
  moreCommentText: {
    fontFamily: 'SFProText-Light',
    textAlign: 'right',
    paddingVertical: 8,
  },

  grapeVine: {
    marginHorizontal: 16,
  },
  feedFooterContainer: {
    paddingVertical: 16
  },
  feedFooterImage: {
    height: 250,
    width: '100%',
  },
  feedFooterTextContainer: {
    paddingHorizontal: 16,
  },
  feedFooterTitle: {
    fontFamily: 'SFProText-Light',
    fontSize: 20,
    paddingVertical: 16
  }
})