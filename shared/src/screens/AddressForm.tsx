import React from 'react';
import { TouchableOpacity, StyleSheet, KeyboardAvoidingView, SectionList, } from 'react-native';

import { Text, AsyncForm, Button } from '../components';
import { TextStyles } from '../styles';
import { useStore } from '../context';
import { addressForm } from '../forms';
import { useAsyncSubmit } from '../utils';

import AutoCompleteAddress from './AutoCompleteAddress';
import DetailPage from './profile/DetailPage';


export default function AddressForm({ navigation, route }: {
  navigation: any,
  route: any
}) {

  const selected = route.params?.address || '';

  const onClose = () => navigation.goBack();

  const [state, dispatch] = useStore();
  const { loading: billingLoader, submit: defaultBill } = useAsyncSubmit('address/make-billing-address');
  const { loading: shippingLoader, submit: defaultShip } = useAsyncSubmit('address/make-shipping-address');
  const { loading: deletingLoader, submit: deleteAddress } = useAsyncSubmit('address/remove');


  const data = selected ? state.addresses.byId[selected] : {};
  const endpoint = selected ? 'address/update' : 'address/add';
  const cta = selected && 'Edit';
  const action = selected ? 'UPDATE_ADDRESS': 'ADD_ADDRESS';
  const header = selected ? 'Edit' : 'Add';

  const handleFormatData = (d: any) => {
    if (selected) d.address_id = selected;
    return d;
  }

  const handleComplete = async (res: any, data: any) => {
    let payload = data;
    if (!selected) payload.address_id = res;

    dispatch({ type: action, payload })
    onClose();
  }

  const handleBilling = async () => {
    await defaultBill({ address_id: selected });

    dispatch({ type: 'UPDATE_BILLING', payload: selected })
  }

  const handleShipping = async () => {
    await defaultShip({ address_id: selected });

    dispatch({ type: 'UPDATE_SHIPPING', payload: selected })
  }

  const handleDelete = async () => {
    await deleteAddress({ address_id: selected });

    dispatch({ type: 'REMOVE_ADDRESS', payload: selected });
  }
  
  const loading = billingLoader || shippingLoader || deletingLoader;

  return (
    <DetailPage header={`${header} Address`}>
      <KeyboardAvoidingView style={styles.container}>
        <SectionList
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          keyExtractor={(item, index) => item + index}
          sections={[{ data: [''] }]}
          renderItem={() => (
            <AsyncForm
              endpoint={endpoint}
              onComplete={handleComplete}
              formData={addressForm}
              errorStyles={TextStyles.error}
              defaultValues={data}
              buttonText={cta}
              formatData={handleFormatData}
              containerStyles={{ paddingTop: 56 }}
              TailComponent={<AutoCompleteAddress />}
            />
          )}
          ListFooterComponent={selected ? (
            <>
              {selected  && <Button title="Delete" disabled={loading} onPress={handleDelete} style={styles.delete}/>}
              <TouchableOpacity onPress={handleBilling} disabled={loading || data.is_billing}>
                <Text style={[styles.formCta, TextStyles.smallSecondaryVariant]}>
                  {data.is_billing ? 'This is default billing addresss' : 'Make default billing'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={handleShipping} disabled={loading || data.is_shipping}>
                <Text style={[styles.formCta, TextStyles.smallSecondaryVariant]}>
                  {data.is_shipping ? 'This is default shipping addresss' : 'Make default shipping'}
                </Text>
              </TouchableOpacity>
            </>
          ): null}
        />
      </KeyboardAvoidingView>
    </DetailPage>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 24,
    paddingHorizontal: 16
  },
  formCta: {
    paddingVertical: 8
  },
  delete: {
    marginVertical: 16
  }
});
