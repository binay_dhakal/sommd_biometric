import React, { useRef } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Animated, I18nManager } from 'react-native';
import { RectButton, Swipeable } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

import { IconButton, BadgeIcon, Text, Progress, IconResolver } from '../components';
import { URL } from '../constants';
import { colors, LayoutStyles, TextStyles } from '../styles';
import { useAsyncSubmit } from '../utils';
import { Wine } from '../selectors';
import { useStore } from '../context';

import RedWineIcon from '../assets/redWineIcon.svg';
import YellowWineIcon from '../assets/yellowWineIcon.svg';
import HeartOutline from '../assets/heartOutline.svg';
import Heart from '../assets/heart.svg';
const Cellar = require('../assets/barrels.png');
import Flag from '../assets/flag.svg';
import Cart from '../assets/cart.svg';


type ActionGroup =  {
  key: string,
  label: string,
  backgroundColor: string,
  color: string,
  Icon: React.ComponentType<any>,
  onPress?: () => void
} ;

interface ActionProp {
  left: ActionGroup[],
  right: ActionGroup[]
}

type Props = {
  actions?: ActionProp,
  handleCart?: (wine: Wine) => void,
  handleCellar?: (wine: Wine) => void,

  // TODO: Handle reaction for items that are not in store
  disableReaction?: boolean
}


const renderAction = (action: ActionGroup, x: number, progress: Animated.AnimatedInterpolation, onClose: () => void) => {
  const { label, backgroundColor, color, Icon, onPress } = action;

  const trans = progress.interpolate({
    inputRange: [0, 1],
    outputRange: [x, 0],
  });
  const pressHandler = () => {
    if (onPress) onPress();
    onClose();
  };
  return (
    <Animated.View key={x} style={[styles.actionContainer, { transform: [{ translateX: trans }] }]}>
      <RectButton
        style={[styles.action, { backgroundColor }]}
        onPress={pressHandler}>
        <IconResolver width={32} height={32} Icon={Icon} />
        <Text style={[styles.actionText, { color  }, TextStyles.small]}>{label}</Text>
      </RectButton>
    </Animated.View>
  );
};

const ACTION_WIDTH = 100;
const renderActions = (
  progress: Animated.AnimatedInterpolation,
  actions: ActionGroup[],
  multiplier: number,
  onClose: () => void
) => {
  return (
    <View
      style={{
        width: ACTION_WIDTH * actions.length,
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
      }}>
        {actions.map((action, index) => {
          const x = multiplier * ACTION_WIDTH * (actions.length - index)
          return renderAction(action, x, progress, onClose)
        })}
    </View>
  );
}


function ProductItem({
  actions,
  handleCart,
  handleCellar,
  ...wine
}: Wine & Props) {
  const [_, dispatch] = useStore();
  const swipeableRef = useRef<Swipeable | null>(null);
  const navigation = useNavigation();

  const { 
    bottle,
    wine_id,
    winery,
    name,
    vintage,
    wine_category,
    reaction,
    cart,

    score,
    price,
    inventory,

    disableReaction
  } = wine;

  const closeSwipeable = () => {
    swipeableRef.current?.close();
  }

  const makeAction = (multiplier: 1 | -1, actions?: ActionGroup[]) => (progress: Animated.AnimatedInterpolation) => {
    if (actions) return renderActions(progress, actions, multiplier, closeSwipeable)
    return null;
  }

  const renderRightActions = makeAction(1, actions?.right);
  const renderLeftActions = makeAction(-1, actions?.left);
  
 
  const { submit: like, loading: likeLoader } = useAsyncSubmit('wine-reaction/like');
  const { submit: removeLike, loading: removeLikeLoader } = useAsyncSubmit('wine-reaction/remove-like');
  // const { submit: addCellar, loading: cellarLoader } = useAsyncSubmit('cellar/add');

  const favorite = reaction === 'like';
  const disabled = likeLoader || removeLikeLoader;
  const WineIcon = wine_category === 'Red' ? RedWineIcon : YellowWineIcon;
  const HeartIcon = (favorite || disabled) ? Heart : HeartOutline;
  const fillColor = favorite ? '#C85F57' : '#CCCCCC';
  const heartIconColor = disabled ? colors.disabled : fillColor;
  const iconDimension = { width: 20, height: 20 };


  const likeWine = async () => {
    if (disableReaction) return;

    const payload = { wine_id };
    const api = favorite ? removeLike : like;

    await api(payload);
    dispatch({ type: 'UPDATE_WINE', payload: { id: wine_id, reaction: favorite ? null : 'like' } });
  }

  return (
    <Swipeable
      ref={swipeableRef}
      leftThreshold={30}
      rightThreshold={40}
      renderRightActions={renderRightActions}
      renderLeftActions={renderLeftActions}
    >
      <View style={[LayoutStyles.shadow, styles.container]}>
        <View style={styles.iconsContainer}>
          <IconButton
            {...iconDimension}
            color={heartIconColor}
            Icon={disabled ? HeartIcon : HeartIcon}
            onPress={likeWine}
            disabled={disabled}
          />
          <IconResolver Icon={WineIcon} {...iconDimension} />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('ProductDetail', { id: wine_id })}>
          {bottle ? (
            <Image source={{ uri: URL + bottle}} style={styles.productImage} />
          ): (
            <View style={[styles.productImage, { backgroundColor: colors.secondary }]} />
          )}
        </TouchableOpacity>
        <View style={styles.detailContainer}>
          <View style={styles.infoContainer}>
            <View style={styles.infoTop}>
              <Text style={styles.vintage}>{vintage || '2001'}</Text>
              <Text numberOfLines={2} style={[TextStyles.secondaryVariant, styles.winery]}>{winery || 'Insignia'}</Text>
              <Text numberOfLines={1} style={[TextStyles.smallSecondaryVariant, styles.name]}>{name || 'Joseph Phelps'}</Text>
            </View>
            <View style={styles.infoBottom}>
              <View style={styles.priceContainer}>
                <Text style={styles.price}>{price ? `$ ${price}.00` : '$200.00'}</Text>
              </View>
              <BadgeIcon
                Icon={Cart}
                color="#A1A1A1"
                badge={cart}
                style={iconDimension}
                containerStyle={styles.cart}
                onPress={() => handleCart && handleCart(wine)}
              />
              <BadgeIcon
                Icon={Cellar}
                badge={inventory}
                style={iconDimension}
                onPress={() => handleCellar && handleCellar(wine)}
              />
            </View>
          </View> 
          <View style={styles.scoreContainer}>
            <Flag width={30} height={30} />
            <Progress
              animated={false}
              size={40}
              strokeWidth={5}
              color="#F98711"
              score={Number(score?.toPrecision(3)) || 94}
              label="Somm'd Score"
              labelStyle={{ ...TextStyles.smaller, ...styles.progressLabel}}
              childrenStyle={styles.progressChildren}
            />
          </View>
        </View>
      </View>
    </Swipeable>
	)
}

export default React.memo(ProductItem);

const styles = StyleSheet.create({
	container: {
    marginHorizontal: 4,
    paddingHorizontal: 8,
    marginVertical: 4,
    flexDirection: 'row',
    backgroundColor: colors.primaryVariant
  },
  iconsContainer: {
    justifyContent: 'space-between',
    paddingVertical: 8,
  },
  productImage: {
    width: 50,
    height: 135,
    marginHorizontal: 16,
  },
  detailContainer: {
    flex: 1,
    paddingBottom: 8,
    flexDirection: 'row',
  },
  infoContainer: {
    flex: 1,
  },
  infoTop: {
    flex: 1,
    marginBottom: 4,
  },
  vintage: {
    position: 'absolute',
    fontFamily: 'SFProText-Bold',
    color: '#EEEEEE',
    fontSize: 70,
    top: 0,
    left: 8,
  },
  winery: {
    position: 'absolute',
    top: 24,
  },
  name: {
    position: 'absolute',
    bottom: 0,
    fontFamily: 'SFProText-Light',
    color: '#808080',
  },
  infoBottom: {
    flexDirection: 'row',
    alignItems: 'center',
  },
 
  priceContainer: {
    backgroundColor: '#EEEEEE',
    borderRadius: 8,
    paddingHorizontal: 8,


    paddingVertical: 4,
  },
  price: {
    fontSize: 12,
    color: colors.brand,
  },
  cart: {
    marginLeft: 16,
    marginRight: 8,
  },
  cta: {
    color: colors.brand,
    padding: 8,
  },
  scoreContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 8
  },
  badge: {
    position: 'absolute',
    right: 0,
  },
  action: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionText: {
    textAlign: 'center',
    paddingTop: 16,
  },
  leftActionContainer: {
    width: ACTION_WIDTH,
  },
  actionContainer: {
    flex: 1,
  },
  progressLabel: {
    width: 50,
  },
  progressChildren: {
    color: '#FA6400'
  }
})

