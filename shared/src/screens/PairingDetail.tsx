import React from 'react';
import { Image, StyleSheet, View, ScrollView } from 'react-native';

import { Text } from '../components';
import ProductItem from './ProductItem';
import { colors, TextStyles } from '../styles';
import { PAIRINGS } from '../dummyData';

import Flag from '../assets/flag.svg';



export default function PairingDetail() {

  return (
    <>
      <View style={styles.headerContainer}>
        <View style={styles.infoContainer}>
          <Image source={require('../assets/detail_bottle.png')} style={styles.bottle} />
          <View style={styles.innerContainer}>
            <Text style={[TextStyles.small, styles.lightFont]}>Cabernet Sauvignon</Text>
            <Text style={TextStyles.headerFour}>1997 Hillside Select</Text>
            <Text style={styles.lightFont}>Shafer</Text>
          </View>
          <Flag style={{ position: 'absolute', bottom: 0, right: 0 }} width={24} height={24} />
        </View>
        <Text style={[TextStyles.small, styles.description]}>
          Cabernet Sauvignon food pairing is best with nearly all red meat, including prime rib, New York strip and filet mignon. Also try lamb or pepper- crusted ahi tuna. The wine is best enjoyed with food and is great in sauces or reductions.
        </Text>
        <View style={styles.moreInfo}>
          <Text style={[TextStyles.small, styles.moreInfoText]}>Also pairs well with…</Text>
        </View>
      </View>
      <ScrollView>
        {Object.values(PAIRINGS).map((wine, idx) => <ProductItem key={idx+1} disableReaction {...wine} />)}
      </ScrollView>
    </>
  )
}

const styles = StyleSheet.create({
  headerContainer: {
    paddingHorizontal: 16,
  },
  infoContainer: {
    flexDirection: 'row',
  },
  bottle: {
    width: 40,
    height: 150,
  },
  innerContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    marginLeft: 16,
  },
  lightFont: {
    fontFamily: 'SFProText-Light'
  },
  moreInfo: {
    borderTopColor: colors.brandPrimary,
    borderTopWidth: 1,
    paddingVertical: 8
  },
  moreInfoText: {
    color: colors.brandPrimary
  },
  description: {
    paddingTop: 16,
    paddingBottom: 8,
    fontFamily: 'SFProText-Light'
  }
});
