import React from 'react';
import { StyleSheet } from 'react-native';

import { Text } from '../components';
import { useStore } from '../context';
import { TextStyles } from '../styles';

import { Wine } from '../selectors';

import ProductList from './ProductList';

export default function Cart({ stateKey = 'cart' }) {
  const [state] = useStore();
  const wines = state.profile ? state.profile.wines : [];

  const { items, total } = wines.reduce(
    (res: { items: Wine[], total: number }, curr: any) => {

      if (curr[stateKey]) {
        res.items.push(curr)
        res.total += (curr[stateKey] || 0)
      }

      return res;
    }, { items: [], total: 0 }
  )

  return (
    <>
      {items.length ? (
        <ProductList data={items} />
      ): (
        <Text style={[TextStyles.smallSecondaryVariant, styles.empty]}>
          {`You have not added any wines to your ${stateKey}. Add some to see them here.`}
        </Text>
      )}
      {items.length ? (
        <Text style={[TextStyles.secondary, styles.footer]}>
          {`Total items in ${stateKey} : ${total}`}
        </Text>
      ) : null}
    </>
  );
}

const styles = StyleSheet.create({
  empty: {
    textAlign: 'center',
    padding: 16,
  },
  footer: {
    padding: 16,
  }
})
