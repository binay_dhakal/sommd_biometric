import React, { useRef, } from 'react';
import { StyleSheet, View, StatusBar, Dimensions, ViewStyle, } from 'react-native';
import { RNCamera } from 'react-native-camera';
import { launchImageLibrary, ImagePickerResponse, ImageLibraryOptions, } from 'react-native-image-picker';

import { IconButton, Text } from '../../components';
import { colors } from '../../styles';
import Close from '../../assets/close.svg';
import Capture from '../../assets/capture.svg';
import Gallery from '../../assets/gallery.svg';
import { DEFAULT_SCREEN } from '../../constants';


const mask = {
  width: Dimensions.get('window').width - (2 * 16),
  height: 50,
  bgColor: '#ecf0f1',
};

const CAMERA_OPTIONS = { quality: 0.5, base64: true, };
const GALLERY_OPTIONS: ImageLibraryOptions = {
  mediaType: 'photo',
  quality: 0.5,
  includeBase64: true,
};

const Arc = ({ style }: { style?: ViewStyle }) => (
  <View style={styles.arcContainer} >
    <View style={[style, styles.arc]} />
  </View>
)

export default function Camera({ navigation }: { navigation: any }) {
	const cameraRef = useRef<RNCamera | null>(null);

  const searchImage = (img: string) => {
    navigation.navigate(DEFAULT_SCREEN, { screen: 'Search', params: { image: img } });
  }

  const takePicture = async (camera: any) => {
    const data = await camera.takePictureAsync(CAMERA_OPTIONS);
    searchImage(data.base64);
  }

  const choosePicture = (response: ImagePickerResponse) => {
    if (response.didCancel) return;
    if (response.assets[0].base64) searchImage(response.assets[0].base64);
  }
  
  return (
		<View style={styles.container}>
      <StatusBar backgroundColor={colors.brandPrimary} />
        <RNCamera
          ref={cameraRef}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        >
          {({ camera, status, }) => {
            if (status === 'PENDING_AUTHORIZATION') return <Text>Pending</Text>;
            if (status === 'NOT_AUTHORIZED') return <Text>Not authoried to use camera</Text>;
            return (
              <>
                <View style={styles.innerContainer}>
                  <Arc style={styles.topArc} />
                  <View style={styles.arcBody} />
                  <Arc style={styles.bottomArc} />
                </View>
                <View style={styles.footer}>
                  <IconButton Icon={Close} onPress={() => navigation.goBack()} width={20} height={20} />
                  <IconButton Icon={Capture} onPress={() => takePicture(camera)} width={72} height={72} />
                  <IconButton Icon={Gallery} onPress={() => launchImageLibrary(GALLERY_OPTIONS, choosePicture)} width={20} height={20} />
                </View>
              </>
            )
          }}
        </RNCamera>
    </View>
	)
}
const styles = StyleSheet.create({
	container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  innerContainer: {
    flex: 1,
    width: '100%',
    marginVertical: 16,
  },
  topArc: { 
    top: 0,
  },
  bottomArc: {
    bottom: 0,
  },
  arcBody: {
    flex: 1,
    marginHorizontal: 16,
    borderColor: colors.secondary,
    borderLeftWidth: 1,
    borderRightWidth: 1,
  },
  footer: {
    flex: 0,
    height: '25%',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.brandSecondary,
    paddingHorizontal: 32
  },



  arcContainer: {
    height: 51,
    marginHorizontal: 16,
    overflow: 'hidden',
  },
  arc: {
    borderRadius: mask.width,
    width: mask.width * 2,
    height: mask.width * 2,
    marginLeft: -(mask.width / 2),
    position: "absolute",
    overflow: "hidden",
    borderColor: colors.secondary,
    borderWidth: 1,
  }, 
  
})
