import React, { useCallback, useState } from 'react';
import { Modal, View, StyleSheet, TouchableOpacity } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';

import UploadImage from './UploadImage';
import CameraWeb from './CameraWeb';
import { Text } from '../../components';
import { handleBack } from '../../utils';
import { colors, LayoutStyles, TextStyles } from '../../styles';
import { DEFAULT_SCREEN } from '../../constants';

type OptionType = 'camera' | 'select';

export default function Camera({ navigation }: { navigation: any }) {
  const [option, setOption] = useState<OptionType | ''>('');
  const onSelect = (f: string) => {
    setOption('');
    const image = f.split(',')[1]
    navigation.navigate(DEFAULT_SCREEN, { screen: 'Search', params: { image } });
  }

  useFocusEffect(useCallback(() => {
    setOption('select');
    }, [])
  )

  const handleCancel = () => {
    setOption('');
    handleBack(navigation);
  }

  if (option === 'camera') return <CameraWeb {...{ navigation, onSelect, onHide: handleCancel}} />
  if (option === 'select')return (
    <Modal visible transparent animationType="slide">
      <View style={[styles.container, LayoutStyles.center]}>
        <View style={styles.innerContainer}>
          <Text style={TextStyles.headerFour}>Select Image</Text>
          <View style={styles.optionsContainer}>
            <TouchableOpacity onPress={() => setOption('camera')} style={styles.option}>
              <Text>Take Photo..</Text>
            </TouchableOpacity>
            <UploadImage onSelect={onSelect} />
          </View>
          <Text onPress={handleCancel} style={[TextStyles.secondaryVariant, { alignSelf: 'flex-end' }]}>Cancel</Text>
        </View>
      </View>
    </Modal>
  )
  return null;
}


const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    backgroundColor: colors.disabled,
  },
  innerContainer: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    height: '25%',
    width: '50%',
    padding: 16,
    borderRadius: 16,
    backgroundColor: '#fff',
  },
  optionsContainer: {
    flex: 1,
    width: '100%',
    justifyContent: 'center'
  },
  option: {
    padding: 16,
    flex: 1,
  }
})
