import React from 'react';
import { TouchableOpacity, StyleSheet, } from 'react-native';
import {  } from 'react-native-safe-area-context';

import { Text, Button, Loading } from '../../components';
import { colors, TextStyles } from '../../styles';

import { useStore } from '../../context';
import { useAddresses } from '../../selectors';

import DetailPage from './DetailPage';


export default function Addresses({ navigation }: { navigation: any }) {
  const { error, loading, addresses } = useAddresses();

  
  if (loading) return <Loading />
  if (error) return <Text style={TextStyles.smallSecondaryVariant}>Cannot load addresses.</Text>

  return (
    <DetailPage header="Addresses">
      {addresses.allIds.map(address => (
        <TouchableOpacity key={address} onPress={() => navigation.push('AddressForm', { address })}>
          <Address id={address} onPress={() => navigation.push('AddressForm', { address })}/>
        </TouchableOpacity>
      ))}
      <Button title="Add new address" onPress={() => navigation.navigate('AddressForm')} style={styles.button} />
    </DetailPage>
  );
}


const makeData = (...arr: string[]) => <Text style={TextStyles.small}>{arr.filter(a => a).join(', ')}</Text>
function Address({ id, onPress }: { id: string, onPress: () => void }) {
  const [state] = useStore();
  const data = state.addresses.byId[id];

  const { address_line_1, address_line_2, address_line_3, city, country, state: addressState, zipcode } = data;

  return (
    <TouchableOpacity style={styles.addressContainer} onPress={onPress}>
      {makeData(address_line_1, address_line_2, address_line_3)}
      {makeData(city, zipcode)}
      {makeData(addressState, country)}
      {data.is_billing && <Text style={[TextStyles.small, styles.detail, styles.footer]}>Default Billing</Text>}
      {data.is_shipping && <Text style={[TextStyles.small, styles.detail]}>Default Shipping</Text>}
    </TouchableOpacity>
  )
}


const styles = StyleSheet.create({
  button: {
    margin: 16
  },

  addressContainer: {
    padding: 16,
    marginBottom: 16,
    borderColor: colors.secondary,
    borderWidth: StyleSheet.hairlineWidth,
    backgroundColor: colors.primaryVariant,
  },
  footer: {
    paddingTop: 16,
    paddingBottom: 8
  },
  detail: {
    fontStyle: 'italic',
    color: colors.brandPrimary,
  },
});
