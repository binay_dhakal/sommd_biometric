import React, { useEffect, useState } from 'react';
import { FlatList, Keyboard, StyleSheet, TouchableOpacity, View } from 'react-native';

import { AsyncForm, Loading, Text } from '../../components';
import { useAsyncSubmit } from '../../utils';
import { colors, TextStyles } from '../../styles';
import { useStore } from '../../context';
import { invitationsForm } from '../../forms';

import DetailPage from './DetailPage';

interface Invitation {
  invitation_to: 'string',
  is_active: boolean,
  signed_up: boolean
}


export default function Invitations() {
  const [state, dispatch] = useStore();
  const { profile: { invitations_sent: sent, remaining_invitations: remaining } } = state;
  const [invitations, setInvitations] = useState<Invitation[]>([]);
  const { submit, loading } = useAsyncSubmit('invitations/list', { method: 'GET' })

  useEffect(() => {
    async function init() {
      const res = await submit();
      if (res) setInvitations(res);
    }

    init()
  }, []);

  const onDelete = (invitation: Invitation) => {
    setInvitations(prev => prev.filter(p => p.invitation_to !== invitation.invitation_to));
    dispatch({
      type: 'INIT_PROFILE',
      payload: {
        remaining_invitations: remaining - 1,
        invitations_sent: sent + 1
      }
    })
  }

  const onComplete = (_r: { invitations_remaining: number }) => {
    dispatch({
      type: 'INIT_PROFILE',
      payload: {
        remaining_invitations: remaining - 1,
        invitations_sent: sent + 1
      }
    })

    Keyboard.dismiss();
  }

  if (loading) return <Loading />

  return (
    <DetailPage header="Invitations">
      <Text style={[TextStyles.secondary, styles.infoText]}>{`Sent Invitations: ${sent} / ${(sent + remaining)}`}</Text>
      <AsyncForm
        endpoint="invite-user"
        onComplete={onComplete}
        formData={invitationsForm}
        errorStyles={TextStyles.error}
        buttonType="secondary"
        containerStyles={{ padding: 16 }}
      />
      <FlatList
        data={invitations}
        renderItem={({ item }) => (
          <Invite
            data={item}
            onDelete={onDelete}
          /> 
        )}
        keyExtractor={keyExtractor}
        ItemSeparatorComponent={() => <View style={styles.border} />}
      />
    </DetailPage>
  )
}
const keyExtractor = (item: Invitation) => item.invitation_to;

function Invite({ data, onDelete, }: {
  data: Invitation,
  onDelete: (s: Invitation) => void,
}) {
  const { submit: deleteInvitation, loading: deleting } = useAsyncSubmit('invitations/delete')
  const { submit: resendInvitation, loading: resending } = useAsyncSubmit('invite-user')

  const handleDelete = async () => {
    const res = await deleteInvitation({ invited_user: data.invitation_to });
    if (res) onDelete(data);
  }
  const handleResend = async () => {
    await resendInvitation({ email: data.invitation_to });
  }

  return (
    <View style={styles.inviteContainer}>
      <View style={styles.inviteInnerContainer}>
        <Text style={TextStyles.secondary}>{data.invitation_to}</Text>
        <Text style={TextStyles.secondary}>{data.signed_up ? 'Signed up' : 'Not signed up'}</Text>
      </View>
      {!data.signed_up && (
        <>
          <TouchableOpacity onPress={handleDelete} disabled={deleting}>
            <Text style={[TextStyles.smallSecondary, styles.inviteCta]}>
              {`${deleting ? 'Deleting' : 'Delete'} Invitation`}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleResend}>
            <Text style={[TextStyles.smallSecondary, styles.inviteCta]}>
              {`${resending ? 'Resending' : 'Resend'} Invitation`}
            </Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  infoText: {
    padding: 16,
  },

  inviteContainer: {
    paddingVertical: 8,
    paddingHorizontal: 16
  },
  border: {
    borderBottomWidth: 1,
    borderColor: colors.secondary,
  },
  inviteInnerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inviteCta: {
    paddingTop: 8,
    fontFamily: 'SFProText-Bold',
  },
})
