import React from 'react';
import { ScrollView } from 'react-native';

import { useStore } from '../../context';
import { Question } from '../../selectors';

import Section from './Section';
import DetailPage from './DetailPage';

const formatAnswers = (data: Question) => {
  return data.selected ? data.selected.map(id => data.answers[id - 1].answer): null;
}


export default function FlavourProfile({ navigation }: { navigation: any }) {
  const [{ questions }] = useStore();

  return (
    <DetailPage header="Flavour Profile" right={{ label: 'Edit', onPress: () => navigation.navigate('EditPreferences')}}>
      <ScrollView>
        {questions.allIds.map((id: string) => {
          const val = questions.byId[id];
          const answers = formatAnswers(val);
          return (
            <Section key={id} title={val.question} items={[ { label:  answers ? answers.join(', '): 'N/A'}]} />
          );
        })}
      </ScrollView>
    </DetailPage>
  )
}
