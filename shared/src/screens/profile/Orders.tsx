import React from 'react';
import { View } from 'react-native';

import { Text } from '../../components';
import { LayoutStyles } from '../../styles';

import DetailPage from './DetailPage';

export default function Orders() {
  return (
  <DetailPage header="Orders">
    <View style={LayoutStyles.center}>
      <Text>Orders</Text>
    </View>
  </DetailPage>
  )
}
