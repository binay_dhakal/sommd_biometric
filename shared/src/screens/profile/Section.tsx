import React from 'react';
import { View, StyleSheet, TouchableOpacity, } from 'react-native';
import { useNavigation, } from '@react-navigation/native';

import { Text, IconResolver } from '../../components';
import { colors, TextStyles } from '../../styles';
import Next from '../../assets/next.svg';


export default function Section({ title, items }: { items: { label: string, screen?: string }[], title?: string}) {
  const navigation = useNavigation();

  return (
    <>
      <Text style={[TextStyles.smallSecondaryVariant, styles.title]}>{title}</Text>
      {items.map(({ label, screen }, idx) => {
        const props = {
            key: label,
            style: [styles.container, idx === 0 && styles.firstItem],
        }
        const body = <Text style={styles.label}>{label}</Text>;

        return screen ? (
          <TouchableOpacity {...props} onPress={() => navigation.navigate(screen)}>
            {body}
            <IconResolver Icon={Next} color={colors.secondary} width={20} height={20} />
          </TouchableOpacity>
        ) : <View {...props}>{body}</View>
      })}
    </>
  )
}


const styles = StyleSheet.create({
  title: {
    paddingHorizontal: 16,
    paddingVertical: 8,
    color: colors.brandPrimary
  },
  container: {
    flexDirection: 'row', 
    alignItems: 'center',
    padding: 16,
    backgroundColor: colors.primaryVariant,
    borderBottomColor: colors.secondary,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  label: {
    flex: 1,
    color: colors.secondary
  },
  firstItem: {
    borderTopColor: colors.secondary,
    borderTopWidth: StyleSheet.hairlineWidth
  }
});
