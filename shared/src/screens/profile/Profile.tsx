import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Switch, TouchableOpacity, Alert,TextInput } from 'react-native';

import { Button, Text, Modal } from '../../components';
import { colors, TextStyles } from '../../styles';
import { useStore, useUser } from '../../context';

import { Wrapper } from '../Cellar';

import Section from './Section';
import { LOGOUT_REDIRECT } from '../../constants';
import * as Keychain from 'react-native-keychain';

export default function Profile({ navigation }: { navigation: any }) {
  return <Wrapper Comp={Main} navigation={navigation} />
}

export type Props = {
  navigation: any,
}

function Main({ navigation }: { navigation: any }) {
  const [state, dispatch] = useStore();
  const { user } = useUser();
  const { profile } = state;
  const {
    email, 
    full_name,
    roles,
  } = profile;

  const [isEnabled, setIsEnabled] = useState(false);
  const [isBiometrySupported, setIsBiometrySupported] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [cancelBiometric, setCancelBiometric] = useState(false);
  const [ password, setPassword ] = useState<string>("");


  useEffect(() => {


    checkBiometricAvailability();
      // check if the Biometric is enabled or not 
      // retrive the token from mobile keystore 
      biometricEnableCheck();

  }, []);

  const checkBiometricAvailability = async() => {
    const biotype =  await Keychain.getSupportedBiometryType();
    console.log(biotype)
 
    if(biotype){
      setIsBiometrySupported(true);
    }
   }


  const biometricEnableCheck = async() => {
    // retrive the token from mobile keystore 
    const credentials = await Keychain.getGenericPassword();
    // Send credentials.username to the backend to verify the token 
    // If the token is verifed then the biometric is enabled else the token from the backend is deleted and biometric is disabled
    // Here is the demo 
    try {
      if (credentials) {
     
        if(credentials.username === "hardwork"){
          setIsEnabled(true);
        }else{
        //  delete the token from the backend and send response false 
        setIsEnabled(false);
        }
  
      } else {
        setIsEnabled(false);
      }
    } catch (error) {
      console.log("Keychain couldn't be accessed!", error);
    }
  }



  const whenPressed = () => {
    if (isEnabled){
      setCancelBiometric(true);
    }else{
      setModalVisible(true);
  }

  }


  const handleBiometricSetup = async() => {
    console.log("password:", password)

    // Password is send to the backend and check is done whether the password is correct or not 
    // user password credentials check is done here 
    // just demo is shown here 
     // if the password is correct then the token is saved in the mobile keystore 
    //  suppose the password is hardwork then 

      if(password === "hardwork"){ // this password check is done in backend  here is done for demo purpose only

      //  Now if the password is correct then a keypair with name username and password is saved in the 
      // mobile keysotre where username will be some token or number saved in the mobile keystore and password is just sommd for all as provided by the package we should sava by using username and password
      // and in the backend database username token is saved with user credentials 


        let username = "hardwork";  // just for demo username should be some generated token // here for demo token is taken as hardwork
        let password = "sommd";
      
      
        await Keychain.setGenericPassword(username,password);
        // await saveBiometricToken(username); --> backend biometric data saved with user credentials 
        setModalVisible(false);
        setIsEnabled(!isEnabled);
        console.log(password)
        Alert.alert(
          "Biometric Login Activated"
        )
      }else {
        setModalVisible(false);
        console.log(password)
        Alert.alert(
          "Wrong Password"
        )
      }
  }

  const handleCancelBiometric = async() => {
    // Token from the mmobile keystore and from the backend is deleted here
    await Keychain.resetGenericPassword(); // Token from mobile is deleted 
    //await deleteBiometricToken(user); // Token from the backend is deleted 
    setCancelBiometric(false);
    setIsEnabled(!isEnabled);
  } 

  const handleLogout = async () => {
    await user?.logout();
    LOGOUT_REDIRECT && navigation.navigate(LOGOUT_REDIRECT);
    dispatch({ type: 'RESET_STORE' });
  }

  return (
    <>
      <View style={styles.profileContainer}>
        <View style={styles.profileImage} />
        <View style={styles.profileDetailContainer}>
          <Text numberOfLines={1} style={[TextStyles.headerFour, styles.name]}>{full_name}</Text>
          <Text numberOfLines={1} style={TextStyles.smallSecondary}>{email}</Text>
          <Text numberOfLines={1} style={TextStyles.smallSecondary}>{roles?.join(', ')}</Text>
          {isBiometrySupported ? (
            <TouchableOpacity style={styles.confirmButton} onPress={whenPressed}>
            <Text numberOfLines={1} style={styles.confirmButtonText}>Biometric Login</Text>
            <Switch
              disabled
              trackColor={{ false: "#767577", true: "#767577" }}
              thumbColor={isEnabled ? "#eb838e" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              value={isEnabled}
            />
            </TouchableOpacity>
          ) : (null)}
          
        </View>

      </View>
      
      <Section
        title="Profile"
        items={[
          { label: 'Flavour Profile', screen: 'FlavourProfile' },
          { label: 'Invitations', screen: 'Invitations' },
          { label: 'Addresses', screen: 'Addresses' },
        ]}
      />
      <Section
        title="Orders"
        items={[
          { label: 'Orders', screen: 'Orders' },
        ]}
      />
      <Button title="Logout" onPress={handleLogout} style={styles.logout}/>
      <Modal
        transparent={true}
        visible={modalVisible}
        title= "Activate Biometric Login"
      >
          <TextInput style={styles.inputStyle} secureTextEntry={true}  onChangeText={(val) => setPassword(val)}  placeholder="Login Password" />
          <View style={{display: "flex", flexDirection: "row", justifyContent: "space-around"}}>
          <Button title="Cancel" style={{ width: "49%", margin: 10}} onPress={() => setModalVisible(false)} />
          <Button title="Activate" style={{ width: "49%", margin: 10}} onPress={handleBiometricSetup} />
          </View>
        </Modal>
        <Modal
        transparent={true}
        visible={cancelBiometric}
        title= "Biometric"
      >
          <Text>Do you want to disable ?</Text>
          <View style={{display: "flex", flexDirection: "row", justifyContent: "space-around"}}>
          <Button title="No" style={{ width: "49%", margin: 10}} onPress={() => setCancelBiometric(false)} />
          <Button title="Yes" style={{ width: "49%", margin: 10}} onPress={handleCancelBiometric} />
          </View>
        </Modal>
    </>
  )
}

const IMAGE_SIZE = 150
const SPACING = 16

const styles = StyleSheet.create({
  profileContainer: {
    flexDirection: 'row',
    padding: 16,
  },
  profileImage: {
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
    borderRadius: IMAGE_SIZE / 2,
    backgroundColor: '#F8F8F8',
  },
  profileDetailContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: SPACING,
  },
  name: {
    color: colors.brandSecondary,
  },
  tabsViewContainer: { marginHorizontal: 16 },
  tabsContainer: { marginBottom: 16 },
  logout: {
    marginHorizontal: 16,
    marginVertical: 32,
  },
  confirmButton: {
    display: "flex",
    flexDirection: "row",
    top: 50,
    left: 20,
    alignItems: "center"
  },
  confirmButtonText: {
    marginRight: 10,
    alignContent: "center"
  },
  inputStyle: {
    width: '100%',
    backgroundColor: '#F2F2F2',
    padding: 12,
    borderRadius: 8,
    borderWidth: 2,
    borderColor: '#979797',
  }

});
