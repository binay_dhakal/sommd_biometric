import React from 'react';
import { View, StyleSheet, SafeAreaView, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { Text, IconButton } from '../../components';
import Back from '../../assets/back.svg';
import { colors, TextStyles } from '../../styles';


export default function DetailPage({ header, right, children }: {
  header: string,
  right?: {
    label: string,
    onPress: () => void,
  },
  children?: React.ReactNode
}) {
  const navigation = useNavigation();
  const { label , onPress } = right || {};

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.formHeader}>
        <IconButton
          Icon={Back}
          width={20}
          height={20}
          color={colors.secondary}
          onPress={() => navigation.goBack()} 
        />
        <Text style={[styles.header, TextStyles.headerFour]}>{header}</Text>
        {right && (
          <TouchableOpacity onPress={onPress}>
            <Text style={TextStyles.smallSecondary}>{label}</Text>
          </TouchableOpacity>
        )}
      </View>
      {children}
    </SafeAreaView>
  )
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  },
  formHeader: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    flex: 1,
    textAlign: 'center',
  },
  formBack: {
    position: 'absolute',
    top: 0,
    left: 0
  },
});
