import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, TextInput, Modal, Keyboard, SafeAreaView, ScrollView } from 'react-native';

import { IconButton, Loading } from '../components';
import { colors, LayoutStyles, TextStyles } from '../styles';
import { getScreenDimension, useAsyncSubmit } from '../utils';
import { Wine } from '../selectors';
import { URL } from '../constants';
import { useImageDimension } from '../hooks';

// import DropDown from '../assets/dropDown.svg';
import Search from '../assets/search.svg';
import Camera from '../assets/camera.svg';
import Back from '../assets/back.svg'
import ProductItem from './ProductItem';


type History = { id: string, image_url?: string, keywords: [] }

const ITEMS_TO_RENDER = 3;
const DIMENSION = (getScreenDimension() - 24 - 16) / ITEMS_TO_RENDER;
// const LIMIT = 100; // in kilobyte (LIMIT IS 10 MB)
// function validateSize(img: string) {
// 	let mod = img.split(',')[1]
// 	const len = mod.length;
// 	let padding;

// 	if (mod.endsWith('==')) padding = 2;
// 	else if (mod.endsWith('=')) padding = 1;
// 	else padding = 0;

// 	const size = (len * (3 / 4) - padding) / 1000
// 	return size < LIMIT;
// }

export default function SearchByImage({ navigation, route }: {
	navigation: any,
	route: any
}) {
	const [search, setSearch] = useState<string>();
	const [isSearching, setIsSearching] = useState<boolean>(false);
	const [history, setHistory] = useState<History[]>();
	const [visible, setVisible] = useState(ITEMS_TO_RENDER);
	const [results, setResults] = useState<null | {
		wines?: { score: number, wine: Wine }[],
		words?: string[],
		vintage?: string[],
		image?: string
	}>();

	const inputRef = useRef(null);

	const { submit, error, loading, setError } = useAsyncSubmit('search-wine');
	const {
		submit: fetchHistory,
		loading: historyLoading,
	} = useAsyncSubmit('get-user-search-history', { method: 'GET' });

	const handleSearch = async (param: {}) => {
		// const valid = validateSize(image);
		// if (!valid) return setError('Image too large. The limit is 10MB');
		const result = await submit(param);
		result?.wines.sort((a: Wine, b: Wine) => (b.score || 0) - (a.score || 0));
		setResults(result || {});
	}

	const searchByImage = (image: string) => {
		handleSearch({ image })
	}

	const searchByText = async () => {
		handleSearch({
			text: {
				name: search,
				...(results?.vintage && { vintage: results.vintage[0] })
			 }
		})
	}

	useEffect(() => {
		const getHistory = async () => {
			const res = await fetchHistory();
			setHistory(res);
		};

		getHistory();
	}, []);

	useEffect(() => {
		if (route.params?.image) {
			searchByImage(route.params.image);
		}
	}, [route.params?.image]);

	const handleClear = () => {
		setSearch('');
		setIsSearching(false);
		setResults(null);
		setError('');
		navigation.setParams({ image: '' });
	};

	const handleNew = () => {
		navigation.navigate('Camera')
	};

	const handleCancel = () => {
		if (search) setSearch('');
		setIsSearching(false);
		Keyboard.dismiss();
	}

	const selectWord = (w: string) => {
		setSearch(w);
		setIsSearching(true);
		// @ts-ignore
		inputRef?.current.focus();
	}

	const image = route.params?.image ? `data:image/jpeg;base64,${route.params.image}` : results?.image && `${URL}${results.image}`;
	if (loading || historyLoading || !history) return <Loading />
	return (
		<ScrollView style={styles.container} keyboardShouldPersistTaps="handled">
			<View style={styles.searchContainer}>
				<View style={[LayoutStyles.shadow, styles.inputContainer]}>
					<IconButton Icon={Search} color="#A1A1A1" style={{ width: 18, height: 18, }} onPress={searchByText} />
					<TextInput
						ref={inputRef}
						value={search}
						placeholder="Search wines"
						onChangeText={(v) => setSearch(v)}
						placeholderTextColor="#8E8E93"
						style={styles.input}
						onFocus={() => setIsSearching(true)}
						onBlur={() => setIsSearching(false)}
						onSubmitEditing={searchByText}
					/>
					{!isSearching && (
						<IconButton color="#A1A1A1" style={{ width: 18, height: 18, }} Icon={Camera} onPress={handleNew} />
					)}
				</View>
				{isSearching && (
					<TouchableOpacity onPress={handleCancel} style={styles.cancel}>
						<Text style={styles.ctaText}>Cancel</Text>
					</TouchableOpacity>
				)}
			</View>
			{results ? (
				<>
					<View style={styles.innerContainer}>
						{image ?
							(
								<View style={styles.imageContainer}>
									<View>
										<Text style={[TextStyles.secondaryVariant, styles.bold]}>Search results for image:</Text>
										<TouchableOpacity onPress={handleClear}>
											<Text style={[TextStyles.secondaryVariant, styles.topSpacing]}>Clear Results</Text>
										</TouchableOpacity>
									</View>
									<ImageViewer uri={image} />
								</View>
							) : null}
							{results?.vintage ? (
								<Text style={TextStyles.secondaryVariant}>{`Vintage: ${results.vintage}`}</Text>
							): null}
							{results?.words && (
								<>
									<Text style={[TextStyles.secondaryVariant, styles.topSpacing, styles.bold]}>Pick words to search further:</Text>
									<View style={styles.wordsContainer}>
										{results?.words.map((w, idx) => (
											<TouchableOpacity key={`${w}${idx}`} style={styles.words} onPress={() => selectWord(w)}>
												<Text style={TextStyles.secondaryVariant}>{w}</Text>
											</TouchableOpacity>
										))}
									</View>
								</>
							)}
					</View>
					{results?.wines?.length ? results.wines.map((w) => (
						<ProductItem key={w.wine.wine_id} disableReaction={true} score={w.score} {...w.wine} />
					)) : (
						<View style={[LayoutStyles.center, styles.topSpacing]}>
							<Text>{loading && 'Loading' || error || !results.image && 'Nothing to show.'}</Text>
						</View>
					)}
				</>
			) : history.length ? (
				<>
					<Text style={[TextStyles.secondaryVariant, { textAlign: 'center', paddingVertical: 12 }]}>Your Search History</Text>
					<View style={{ margin: 12, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center' }}>
						{history.slice(0, visible).map((h, idx) => (
							<TouchableOpacity
								key={h.id}
								onPress={() => setResults({ words: h.keywords, image: h.image_url })}
								style={{
									width: DIMENSION,
									height: DIMENSION,
									padding: 8,
									...(((idx + 1) % ITEMS_TO_RENDER) !== 0 && { marginRight: 8 }),
									marginBottom: 8,
									backgroundColor: colors.primaryVariant,
									borderRadius: 8,
									alignItems: 'center',
									justifyContent: 'center',
								}}
							>
								<Image style={{ width: '100%', height: '100%' }} source={{ uri: `${URL}${h.image_url}` }} />
							</TouchableOpacity>
						))}
						{history.length > visible ? (
							<TouchableOpacity onPress={() => setVisible(prev => prev + ITEMS_TO_RENDER)} style={{ paddingVertical: 8, alignItems: 'center' }}>
								<Text style={TextStyles.secondaryVariant}>View More</Text>
							</TouchableOpacity>
						) : null}
					</View>
				</>
			): null}
		</ScrollView>
	)
}

function ImageViewer({ uri }: { uri?: string }) {
	const [visible, setVisible] = useState(false);
	const { width, height } = useImageDimension(uri);

	if (!uri) return null;
	return (
		<>
			<TouchableOpacity onPress={() => setVisible(true)}>
				<Image
					source={{ uri }}
					style={[styles.image, {
						...(width && height && { aspectRatio: width/height })
					}]}
				/>
			</TouchableOpacity>
			<Modal visible={visible} animationType="fade" onRequestClose={() => setVisible(false)} transparent>
				<SafeAreaView style={styles.imageViewerContainer}>
					<IconButton Icon={Back} onPress={() => setVisible(false)} containerStyle={styles.closeImageViewer} />
					<Image source={{ uri }} style={styles.fullImage} resizeMode="contain" />
				</SafeAreaView>
			</Modal>
		</>
	)
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	searchContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		marginTop: 12,
		paddingHorizontal: 12,
	},
	inputContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems :'center',
		borderColor: '#979797',
		borderWidth: 1,
		borderRadius: 8,
		paddingHorizontal: 12,
		height: 40,
		backgroundColor: '#fff'
	},
	input: {
		flex: 1,
		margin: 0,
		marginLeft: 8,
		padding: 0,
		color: colors.brandPrimary,
	},
	ctaText: {
		color: colors.brandSecondary,
	},
	cancel: {
		paddingLeft: 8,
	},
	imageContainer: {
		flexDirection: 'row',
		paddingVertical: 16,
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	image: {
		height: 150,
	},
	innerContainer: {
		paddingHorizontal: 12,
	},
	bold: {
		fontFamily: 'SFProText-Bold',
	},
	wordsContainer: {
		paddingBottom: 16,
	},
	words: {
		paddingTop: 8,
	},
	topSpacing: {
		paddingTop: 16,
	},


	imageViewerContainer: {
		flex: 1,
		backgroundColor: '#0008'
	},
	closeImageViewer: {
		marginLeft: 16,
	},
	fullImage: {
		width: '100%',
		height: '100%',
	}
})
