import React from 'react';
import {
  View,
  StyleSheet,
  Animated,
  Platform
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { Text, LinearGradient, IconButton } from '../../components';
import { getScreenDimension, handleBack } from '../../utils';

import Back from '../../assets/back.svg';
import Like from '../../assets/heartOutline.svg';
import Share from '../../assets/share.svg';
import Flag from '../../assets/flag.svg';
import BackgroundBottom from '../../assets/backgroundBottom.svg';

const HEADER_HEIGHT = 450;
const HEADER_HEIGHT_SMALL = 250;
export const BOTTLE_HEIGHT = 100;
const YEAR_LARGE_FONT = 180;
const width = getScreenDimension();


export const createInterpolation = (val: Animated.Value) => (start: any, end: any) => (
  val.interpolate({
    inputRange: [0, HEADER_HEIGHT],
    outputRange: [start, end],
    extrapolate: 'clamp',
  })
)

export default function Header({ navigation, animate, wine_category }: {
  navigation: any,
  animate: any,
  wine_category?: 'Red' | 'White'
}) {
  const { top } = useSafeAreaInsets();
  const safeAreaPadding = top + (Platform.OS === 'ios' ? 0 : 32);
  const headerTranslateYStart = top * 2 + (Platform.OS === 'ios' ? 0 : largeSpacing * 2)
  const subtitleYEnd = Platform.OS === 'ios' ? 15 : 35 ;
  const extranavbarY = safeAreaPadding + spacing + 10;


  const height = animate(HEADER_HEIGHT, HEADER_HEIGHT_SMALL);

  const textSize = animate(YEAR_LARGE_FONT, 20);
  const textColor = animate('#fff', '#000');
  const textOpacity = animate(0.6, 1);
  const headerTranslateX = animate(0, -100);
  const headerTranslateY = animate(headerTranslateYStart, 10);

  const subtitleY = animate(YEAR_LARGE_FONT, subtitleYEnd + extranavbarY);

  const detailX = animate(0, -50);
  const detailY = animate(YEAR_LARGE_FONT, 25 + extranavbarY);

  const footerX = animate(0, 125);
  const footerY = animate(0, -50);
  
  const gradientColor = wine_category === 'Red' ? ['#FFE4E4', '#C85F57'] : ['#FFE4E4', '#FFCA3B'];
  const GradientBackground = wine_category === 'Red' ? '#C85F57' : '#F7B500';

  return (
    <Animated.View style={[{ height }, styles.headerContainer]}>
      <View style={styles.headerBackground}>
        <Animated.View style={[{ height }, styles.header]}>
          <LinearGradient colors={gradientColor} style={styles.headerGradient}>
            <Animated.Text
              style={[styles.year, {
                fontSize: textSize,
                color: textColor,
                opacity: textOpacity,
                transform: [
                  { translateX: headerTranslateX },
                  { translateY: headerTranslateY },
                ]
              }]}
              numberOfLines={1}
            >
              2019
            </Animated.Text>
            <BackgroundBottom preserveAspectRatio="none" color={GradientBackground} height="100%" width="120%" style={{ position: 'absolute', bottom: -4, }} />
            <View style={styles.headerInfo}>
              <Animated.Text
                style={[styles.subtitle, {
                  transform: [
                    { translateY: subtitleY }
                  ]
                }]}
                numberOfLines={2}
              >
                1er Cru Chassagne- Montrachet "La..."
              </Animated.Text>
              <Animated.View
                style={[styles.detailContainer, {
                  transform: [
                    { translateX: detailX },
                    { translateY: detailY },
                  ]
                }]}
              >
                <Text>Chardonnay</Text>
                <Text style={styles.detail}>Paul Pillot</Text>
              </Animated.View>
              <View style={styles.footer}>
                <Animated.Text
                  style={{
                    transform: [
                      { translateX: footerX },
                      { translateY: footerY },
                    ]
                  }}
                >
                  Burgandy - FR
                </Animated.Text>
              </View>
            </View>
            <View style={[styles.navbar, { paddingTop: safeAreaPadding }]}>
              <IconButton Icon={Back} onPress={() => handleBack(navigation)} />
              <View style={styles.navIcons}>
                <IconButton Icon={Share} onPress={console.log} />
                <IconButton Icon={Like} style={{ marginLeft: spacing }} onPress={console.log} />
              </View>
            </View>
          </LinearGradient>
        </Animated.View>
      </View>
      <Flag style={{
        position: 'absolute',
        right: spacing,
        bottom: 0,
      }} width={24} height={24} />
    </Animated.View>
  )
}


const largeSpacing = 32;
const spacing = 16;
const smallSpacing = 8;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: 'white',
    overflow: 'hidden',
    alignItems: 'center'
  },
  headerBackground: {
    position: 'absolute',
    height: width * 2.5,
    width: width * 2.5,
    borderRadius: width * 1.25,
    bottom: 0,
    backgroundColor: 'red',
    overflow: 'hidden',
    alignItems: 'center'
  },
  header: {
    width,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
  },
  headerGradient: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center'
  },
  navbar: {
    position: 'absolute',
    paddingHorizontal: spacing,
    paddingBottom: spacing,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  navIcons: { flexDirection: 'row', alignItems: 'center' },
  year: {
    position: 'absolute',
    left: -75,
    right: -75,
    lineHeight: YEAR_LARGE_FONT,
    fontFamily: 'SFProText-Bold',
    textAlign: 'center',
  },
  headerInfo: {
    flex: 1,
    paddingLeft: spacing + BOTTLE_HEIGHT + spacing,
    paddingBottom: spacing,
    paddingRight: spacing,
  },
  subtitle: {
    fontSize: 26,
    fontFamily: 'SFProText-SemiBold',
    textAlign: 'left',
  },
  detailContainer: {
    paddingTop: smallSpacing,
  },
  detail: {
    paddingTop: spacing,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end'
  },
})
