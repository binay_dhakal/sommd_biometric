import React, { useRef } from 'react';
import {
  Image,
  View,
  StyleSheet,
  ScrollView,
  Animated,
  ImageBackground,
  Platform,
  TouchableOpacity
} from 'react-native';

import { Text, Button, Progress, BadgeIcon, } from '../../components';
import { colors, TextStyles } from '../../styles';
import { useAsyncSubmit } from '../../utils';
import Header, { createInterpolation, BOTTLE_HEIGHT } from './Header';
import Rating from './Rating';
import { Wine } from '../../selectors';
import { useStore } from '../../context';

const Cellar = require('../../assets/barrels.png');

export default function ProductDetail({ route, navigation }: {
  route: any,
  navigation: any
}) {
  // @ts-ignore
  const { params } = route; 

  const [state, dispatch] = useStore();
  const wines = state.profile ? state.profile.wines : [];
  const currentWine = wines.find((wine: Wine) => wine.wine_id === params.id);

  const scrollY = useRef(new Animated.Value(0)).current; 
  const { submit, loading, error } = useAsyncSubmit('wine-reaction/rate-wine');
  const { submit: pullCellar, loading: cellarLoader } = useAsyncSubmit('cellar/pull');

  const animate = createInterpolation(scrollY);

  const bottleTopStart = Platform.OS === 'ios' ? 100 : 75;
 
  const bottleLeft = animate(16, 0);
  const bottleTop = animate(bottleTopStart, -75);
  const bottleScale = animate(1, 0.4);

  const handleRating = async (rating: number) => {
    await submit({
      wine_id: params.id,
      rating,
    });

    dispatch({ type: 'UPDATE_WINE', payload: { id: params.id, rating } })
  }

  const pullFromCellar = async () => {
    await pullCellar({
      wine_id: params.id,
      count: 1
    });
  }

  return (
    <>
      <Header {...{ animate, navigation, wine_category: currentWine?.wine_category }} />
      <ScrollView
        style={styles.container}
        showsVerticalScrollIndicator={false}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: false }
        )}
      >
        <View style={styles.contentContainer}>
          <View style={styles.scoreContainer}>
            <Progress color="#F28730" score={94.5} label="Somm'd Score">
              <Text style={[TextStyles.headerThree, { color: '#F28730' }]}>94.5</Text>
            </Progress>
            <Progress color="#337125" score={60} label="Drinking Window">
              <View style={styles.center}>
                <Text style={styles.drinkingWindow}>2019</Text>
                <Text style={styles.drinkingWindow}>|</Text>
                <Text style={styles.drinkingWindow}>2032</Text>
              </View>
            </Progress>
            <Progress onPress={() => navigation.navigate('PairingDetail',)} color="#3BA3DF" label="Food Pairing">
              <Image source={require('../../assets/food_pairing.png')} style={styles.pairingDetail} resizeMode="contain" />
            </Progress>
          </View>
          <Button type="secondary" title="Find me this wine  |  $205" />
          <View style={styles.ctaContainer}>
            <BadgeIcon Icon={Cellar} />
            <Text style={[TextStyles.small, styles.ctaText]}>Bottles in your cellar: 2</Text>
            <TouchableOpacity onPress={pullFromCellar} disabled={cellarLoader} style={styles.ctaWrapper}>
              <Text style={[TextStyles.small, styles.cta]}>Pull from cellar</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.largeSpacing}>
            <Text style={[TextStyles.smallSecondary, styles.textLabel]}>WineMaker Notes:</Text>
            <Text style={TextStyles.smallSecondary}>Pillot works with just 0.26-hectares of 30+-year-old vines in this 'island' of a vineyard, high up on the slope. In fact, this is one of Chassagne's highest sites (above Les Grandes Ruchottes and around the same level as La Romanée). This is a first-rate terroir, on the same sweeping band of chalky, terres blanches soil that flows north towards the Grand Cru vineyards.</Text>
            <View style={styles.row}>
              <Info label="Grape" value="Chardonnay" />
              <Info label="Varietal" value="Chardonnay" />
            </View>
            <View style={styles.row}>
              <Info label="Alchohol Content" value="14%" />
              <Info label="Bottle size" value="750mL" />
            </View>
          </View>
          <Text style={[styles.scoreLabel, TextStyles.smallSecondary]}>Score this wine:</Text>
          <Rating defaultvalue={currentWine?.rating} onComplete={handleRating} loading={loading} />
          <Text style={TextStyles.error}>{error}</Text>
          <Button type="primary" title="Show me similar wines" style={styles.buttonSpacing} />
          <View style={styles.spacing}>
            <Text style={[TextStyles.smallSecondary, styles.textLabel]}>Domaine Paul Pillot</Text>
            <Text style={TextStyles.smallSecondary}>Domaine Paul Pillot was founded in 1900 by Jean-Baptiste Pillot, a barrel maker by trade, who decided to focus full-time on his vines. His sons, Alphonse and Henri, succeeded him after the conclusion of World War I, increasing the Domaine’s holdings significantly and beginning to bottle their own wines. Henri’s son Paul then took over the Domaine in 1968, and acquiring the prestigious Chassagne-Montrachet 1er Cru vineyards of Clos Saint Jean, Les Grandes Ruchottes, Les Caillerets, and La Grande Montagne, and the Saint-Aubin 1er Cru of Les Charmois. Today, the domaine owns 13 hectares of vines (4.5 in red) and is run by Paul’s dynamic son Thierry who began working in 1999, before taking over full time in 2004. Thierry is the 4th generation to take on the reins of this storied property. Under Thierry the focus has been put squarely in the vineyards, with a discreet use of new wood and a non-interventionist approach in the cellar. The result is wines of finesse and palate staining purity that perfectly reflect the finest terroirs of Chassagne-Montrachet.</Text>
          </View>
        </View>
        <ImageBackground style={styles.footerImg} source={require('../../assets/detail_more_bg.png')}>
          <Button
            textStyle={styles.footerBtnTxt}
            style={styles.footerBtn}
            title="More from this winery"
          />
        </ImageBackground>
      </ScrollView>
      <Animated.Image
        source={require('../../assets/detail_bottle.png')}
        style={[styles.bottle, {
          transform: [
            { scale: bottleScale, },
            { translateY: bottleTop, },
            { translateX: bottleLeft, }
          ]
        }]}
        resizeMode="contain"
      />  
    </>
  )
}


function Info({ label, value }: { label: string, value: string }) {
  return (
    <View style={styles.infoContainer}>
      <Text style={TextStyles.smallSecondary}>{label}</Text>
      <Text style={TextStyles.small}>{value}</Text>
    </View>
  )
}

const largeSpacing = 32;
const spacing = 16;
const smallSpacing = 8;

const styles = StyleSheet.create({
  bottle: {
    width: BOTTLE_HEIGHT,
    position: 'absolute',
    left: 0,
    top: 0,
  },
  nameContainer: {
    flexDirection: 'row',
  },
  name: { flex: 1 },
  container: { flex: 1, backgroundColor: 'white' },
  contentContainer: {
    paddingTop: largeSpacing,
    paddingHorizontal: largeSpacing
  },
  scoreContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: largeSpacing,
  },
  drinkingWindow: {
    fontSize: 16,
  },
  pairingDetail: { width: '50%', height: '50%' },
  ctaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: largeSpacing,
  },
  ctaText: {
    color: '#777777',
    paddingHorizontal: smallSpacing,
  },
  ctaWrapper: {
    borderLeftColor: '#777777',
    borderLeftWidth: 1,
  },
  cta: {
    paddingHorizontal: smallSpacing,
    color: colors.brandPrimary,
  },
  largeSpacing: {
    paddingVertical: largeSpacing,
  },
  spacing: {
    paddingVertical: spacing,
  },
  row: { flexDirection: 'row', },
  center: { alignItems: 'center' },
  textLabel: { paddingBottom: smallSpacing },
  scoreLabel: {
    textAlign: 'center',
  },
  buttonSpacing: { marginVertical: spacing },
  rating: {
    marginHorizontal: smallSpacing,
  },
  infoContainer: { flex: 1, paddingVertical: spacing, },
  footerImg: {
    height: 250
  },
  footerBtn: {
    borderColor: colors.primary,
    backgroundColor: '#fff5',
    margin: spacing
  },
  footerBtnTxt: {
    color: colors.primary
  }
})
