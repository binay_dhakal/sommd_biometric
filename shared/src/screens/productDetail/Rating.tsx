import React, { useEffect, useRef, useState } from 'react';
import { Animated, StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import { spring } from '@lib/anim';

export default function Rating({ defaultvalue = 0, count = 5, containerStyle, onComplete, loading }: {
  defaultvalue?: number,
  count?: number,
  containerStyle?: ViewStyle,
  onComplete?: (arg0: number) => Promise<void>,
  loading?: boolean
}) {
  const [val, setVal] = useState(0);

  useEffect(() => {
    // update the current rating value when the defaultValue is updated.
    setVal(defaultvalue);
  }, [defaultvalue])


  return (
    <View style={[styles.container, containerStyle]}>
      {/* @ts-ignore */}
      {Array.from({ length: count }).map((a, idx) => {
        const value = idx + 1;
        const isSelected = val >= value;
        const isLast = val === value;

        return (
          <Star
            loading={loading}
            selected={isSelected}
            onPress={async () => {
              if (!isLast) {
                const res = onComplete && await onComplete(value);
                if (res) setVal(value);
              }
            }}
            key={idx}
          />
        )
      })}
    </View>
  )
}

function Star({ selected, onPress, loading }: {
  selected: boolean, onPress: () => void, loading?: boolean
}) {
  const a = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    if (selected) spring(a, 1.5, { bounciness: 20 }).start();
    else spring(a, 1, { bounciness: 20 }).start();
  }, [selected]);

  return (
    <TouchableOpacity onPress={onPress} disabled={loading}>
      <Animated.Image
        style={[styles.image, { transform: [{ scale: a }], opacity: loading ? 0.5: 1 }]}
        source={selected ? require('../../assets/starSelected.png') : require('../../assets/star.png')}
      />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 16
  },
  image: {
    width: 20,
    height: 20,
    marginHorizontal: 8,
  }
})
