const fs = require('fs');
const path = require('path');


function parseValue(value) {
  try {
    return JSON.parse(value);
  } catch (err) {
    return value;
  }
}

// Retreive the versionCode from the app package.json
const appPkg = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../mobile/package.json')));

const stdConfig = {};
stdConfig['version-name'] = appPkg.version;
stdConfig['timestamp'] = Date.now();
stdConfig['version-code'] = 1;

for (let i = 2; i < process.argv.length; i++) {
  const arg = process.argv[i];
  if (arg.startsWith('--')) {
    const [a, v] = arg.split('=', 2);
    const name = a.substr(2);
    const value = parseValue(v);
    stdConfig[name] = value;

    console.log(`Overriding configuration ${name} = ${value} [${typeof(value)}]`);
  }
}

function fromTemplate(src, target, os = '') {
  const template = fs.readFileSync(path.resolve(__dirname, src), 'utf-8');

  const content = template.replace(/{{(.*?)}}/g, (found, backRef) => {
    const v = stdConfig[backRef];
    if (v === undefined) {
      console.log(`ERROR::Template requires a configuration ${backRef}, but was not found`);
    }
    return v;
  });

  fs.writeFileSync(target, content);
}

// Generate files from template
fromTemplate('Config.xcconfig.template', path.resolve(__dirname, '../mobile/ios/Config.xcconfig'), 'ios');
fromTemplate('Fastfile.template', path.resolve(__dirname, '../mobile/ios/fastlane/Fastfile'), 'ios');
