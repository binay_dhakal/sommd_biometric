const util = require('util');
const exec = util.promisify(require('child_process').exec);


async function job(command) {
  try {
    const { stdout } = await exec(command);
    console.log("\n");
    console.log(stdout);
    return stdout.trim();
  } catch(err) {
    console.log("\n");
    console.error(`Error with command ${command}`)
    return null;
  }
}


async function init() {
  console.log('\t---------Downloading version from aws---------');
  await job('aws s3 cp s3://sommd-iosappsecrets/app.version.txt ../version/version.txt');
 
  console.log('\t---------Incrementing current build version on local file---------');
  await job('awk -F, \'{printf("%d\\n",$1+1)}\' OFS=, ../version/version.txt > tmp && mv tmp ../version/version.txt');
 
  console.log('\t---------Uploading the local file to aws---------');
  await job('aws s3 cp ../version/version.txt s3://sommd-iosappsecrets/app.version.txt');
 
  console.log('\t---------Fetching the new build version---------');
  const version = await job('cat ../version/version.txt');
  console.log(`\t\tThe new build version is ${version}`);

  console.log('\t---------Running setup---------')
  await job(`yarn setup:mobile --version-code=${version}`); 
  
  console.log('\t---------Commit fastlane file with updated version---------')
  await job(`git add mobile/ios/fastlane/Fastfile`); 
  await job(`git commit -m \"Publish new ios version ${version} to testflight\"`); 
  
  const branch = await job('git branch --show-current');
  await job(`git push origin ${branch}`)
}

init();